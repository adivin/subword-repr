#!/bin/bash -e

work=exp_500
model=$1
stage=1

mkdir -p $work/tmp
mkdir -p $work/clusters/iters

if [ $stage -le 1 ]; then
  #rm -rf $work
  #utils/queue.pl --totmem 48G -V -l q_gpu $work/evallog python eval.py -r $work/repr $model data/feats.hdf data/data.pkl

  #utils/queue.pl --totmem 40G -V -l q_gpu $work/splitlog python misc/split_matrix.py $work/repr_nums $work/tmp 6

  utils/queue.pl  -l q_gpu -V $work/init_cluster1_log python km_faiss.py -f $work/tmp/repr_nums_1 ${work}_faiss/clusters_init_a &
  utils/queue.pl  -l q_gpu -V $work/init_cluster2_log python km_faiss.py -f $work/tmp/repr_nums_2 ${work}_faiss/clusters_init_b &
  utils/queue.pl  -l q_gpu -V $work/init_cluster3_log python km_faiss.py -f $work/tmp/repr_nums_3 ${work}_faiss/clusters_init_c &
  utils/queue.pl  -l q_gpu -V $work/init_cluster4_log python km_faiss.py -f $work/tmp/repr_nums_4 ${work}_faiss/clusters_init_d &
  utils/queue.pl  -l q_gpu -V $work/init_cluster5_log python km_faiss.py -f $work/tmp/repr_nums_5 ${work}_faiss/clusters_init_e &
  utils/queue.pl  -l q_gpu -V $work/init_cluster6_log python km_faiss.py -f $work/tmp/repr_nums_6 ${work}_faiss/clusters_init_f &

  wait
  echo "Done initial split"
fi
exit 0
if [ $stage -le 2 ]; then
  num_per_file=$(cat $work/tmp/num_per_file)  # elements

  utils/queue.pl --totmem 40G -V -l q_gpu $work/joinlog python -m misc.join_clusters \"$work/clusters_init*\" $num_per_file $work/clusters/

  utils/queue.pl -l gpumem=20 -l q_gpu  -V $work/clusteringlog python km_faiss.py $work/clusters/cluster_start $work/clusters/

fi
echo "Done"




