import concurrent.futures

from loguru import logger
import torch as to
import psutil
import os
    # import pickle
    # from collections import defaultdict
    # import random
    # import soundfile as sf
    # import h5py as hdf
    #
    #
    # from data_rawaudio import MelSpec
    # from misc.misc import read_word_mapping_f, parse_config
    # from trainer import get_context_size
    # from trainer_short import model_init

NUM_BINS = 60
FRAME_LEN = 400
CHUNKLEN = 0.7
FEATURE_PADDING = 0.09
SR = 16000

# class Noises:
#     def __init__(self):
#         logger.info('initing A')
#         self.chunk_len = int((CHUNKLEN + 0.02) * SR)
#         self.wavpaths = []
#         for line in open('/idiap/temp/rbraun/data/noise/wav_list'):
#             self.wavpaths.append(line.strip())
#         self.wavpaths = random.sample(self.wavpaths, 500)
#         self.noises = []
#         logger.info('initing B')
#         for wavpath in self.wavpaths:
#             x, fs = sf.read(wavpath, always_2d=True)
#             assert fs == SR
#             assert x.shape[1] == 1
#             x = to.as_tensor(x).float().squeeze(1)
#             if x.size(0) < self.chunk_len + 4000:
#                 x = to.nn.functional.pad(x, (0, self.chunk_len - x.size(0),))
#                 self.noises.append(x)
#             else:
#                 self.noises.append(x[:self.chunk_len])
#                 offset = random.randint(4000, x.size(0) - self.chunk_len)
#                 self.noises.append(x[offset: offset + self.chunk_len])
#         logger.info('initing C')
#         self.noises = to.stack(self.noises).cuda()
#
# class RawWordDataholder:
#     def __init__(self, fpath_words, fpath_feats, fpath_words_test, fpath_words_ood_test, word_mapping, batch_size, subsampling_factor, onesided_context,
#                  small_set=False, smooth=False, smooth_exp=1., use_cuda=True, shuffle_grouped=False, pitch_aug=False,
#                  add_aug=False, aug_snr=None):
#         logger.info(f'Loading data, batch size {batch_size} and using fss {subsampling_factor}.')
#         self.shuffle_grouped = shuffle_grouped
#         self.word_mapping = word_mapping
#         self.smooth_exp = smooth_exp
#         self.use_cuda = use_cuda
#         self.smooth = smooth
#         self.pitch_aug = pitch_aug
#         assert aug_snr is not None
#         self.aug_snr = aug_snr
#         assert FEATURE_PADDING > onesided_context / 100
#         self.subsampling_factor = subsampling_factor
#         self.onesided_context = onesided_context
#
#         to.cuda.synchronize()
#         s = to.cuda.memory_summary()
#         logger.info(s)
#
#         with open(fpath_words, 'rb') as fh:
#             self.lst_wordinfo = pickle.load(fh)
#             if small_set:
#                 self.lst_wordinfo = self.lst_wordinfo[:25_000]
#
#         with open(fpath_words_test, 'rb') as fh:
#             self.lst_wordinfo_test = pickle.load(fh)
#
#         self.lst_wordinfo_ood_test = pickle.load(open(fpath_words_ood_test, 'rb'))
#
#         num_words = len(self.lst_wordinfo)
#         logger.info(f'Num words {num_words}')
#         self.fh = hdf.File(fpath_feats, 'r')
#         self.fpath_feats = fpath_feats
#         print_memory_usage()
#         if not small_set:
#             self.feats_dataset = self.fh['raw'][:]
#         else:
#             self.feats_dataset = self.fh['raw'][:400_000_000]
#         print_memory_usage()
#         self.feats_dataset_test = self.fh['test_raw'][:]
#         self.feats_dataset_ood_test = self.fh['ood_test'][:]
#         # fh.close()
#         len_feats = self.feats_dataset.shape[0]
#         logger.info(f'Number of train samples {len_feats} test samples {self.feats_dataset_test.shape[0]}')
#         # assert num_chunks == len_feats // self.chunk_size
#
#         def parse_wordinfo(lst_wordinfo):
#             lst_indcs = []
#             word_to_indcs = defaultdict(list)
#             word_to_cnt = defaultdict(int)
#             idx_to_utt = {}
#             total_cnt = 0
#             total_dur = 0.
#             spk_set = set()
#             for i, tpl in enumerate(lst_wordinfo):
#                 wid, dur, featidx, uttid, spkid, word_pos = tpl
#                 if small_set and featidx + int(CHUNKLEN * SR) > 400_000_000:
#                     continue
#                 total_dur += dur
#                 dur = int(round(dur*SR))
#                 featidx = int(round(featidx))
#                 spk_set.add(spkid)
#                 word_to_cnt[wid] += 1
#                 word_to_indcs[wid].append(total_cnt)
#                 lst_indcs.append((wid, total_cnt, featidx, dur, spkid, word_pos))  # (wid, word index, start feature frame index, input frame count)
#                 idx_to_utt[total_cnt] = uttid
#                 total_cnt += 1
#             num_speakers = len(spk_set)
#             total_dur /= 3600.
#             logger.info(f'Done parsing data, total audio duration is {total_dur:.2f} - num speakers {num_speakers}')
#             return lst_indcs, word_to_indcs, word_to_cnt, idx_to_utt
#
#         self.lst_indcs, self.word_to_indcs, self.word_to_cnt, self.idx_to_utt = parse_wordinfo(self.lst_wordinfo)
#         self.widx_to_gotten_idx = {wid: 0 for wid in self.word_to_indcs.keys()}
#         self.train_speakers = set()
#         for tpl in self.lst_indcs:
#             self.train_speakers.add(tpl[4])
#
#         self.min_word_cnt = min(self.word_to_cnt.values())
#         max_word_cnt = max(self.word_to_cnt.values())
#         total_cnt = sum(self.word_to_cnt.values())
#         self.word_to_prob = {w: c/total_cnt for w, c in self.word_to_cnt.items()}
#         self.min_word_prob = min(self.word_to_prob.values())
#         logger.info(f'Largest word cnt {max_word_cnt}, smallest {self.min_word_cnt} - Total number of words {len(self.lst_indcs)} - Unique words {len(self.word_to_cnt)}')
#
#         self.batch_size = batch_size
#
#         logger.info('Parsing test data.')
#         self.test_lst_indcs, self.test_word_to_indcs, self.test_word_to_cnt, self.test_idx_to_utt = parse_wordinfo(self.lst_wordinfo_test)
#         self.test_size = len(self.test_lst_indcs)
#         self.ood_test_lst_indcs, self.ood_test_word_to_indcs, self.ood_test_word_to_cnt, self.ood_test_idx_to_utt = parse_wordinfo(self.lst_wordinfo_ood_test)
#
#         logger.info(f'Test set size is {self.test_size} (word count)')
#
#         self.class_weights = [1. for _ in range(len(self.word_to_cnt.keys()))]
#         self.class_weights = to.tensor(self.class_weights)
#         self.num_iterations = len(self.lst_indcs) // self.batch_size
#         self.printed = False
#         self.k = 0
#
#         self.word_to_invprob = {w: 1. / prob for w, prob in self.word_to_prob.items()}
#         probsum = sum(self.word_to_invprob.values())
#         self.word_to_invprob = {w: prob / probsum for w, prob in self.word_to_invprob.items()}
#         logger.info('A')
#         self.tpool = concurrent.futures.ThreadPoolExecutor(2)
#         to.cuda.synchronize()
#         s = to.cuda.memory_summary()
#         logger.info(s)
#         logger.info('B')
#         self.melspec = MelSpec()
#         if self.use_cuda:
#             self.melspec = self.melspec.cuda()
#         logger.info('C')
#         self.add_aug = add_aug
#         if add_aug:
#             self.noises = Noises()
#         logger.info('D')
#         dir = os.path.dirname(fpath_feats)
#         spk_mean_f = f'{dir}/spk_stats.pkl'
#         with open(spk_mean_f, 'rb') as fh:
#             self.spk_means = pickle.load(fh)
#         self.mean = to.stack([v for k, v in self.spk_means.items() if k in self.train_speakers]).sum(dim=0) / len(self.train_speakers)
#         if self.use_cuda:
#             self.mean = self.mean.cuda()
#         self.onesided_context_s = self.onesided_context / 100.
#         self.batch_dur = int( (CHUNKLEN - 2 * FEATURE_PADDING + 2 * self.onesided_context_s + 0.02) * SR)
#         self.f_batch = to.zeros((self.batch_size, self.batch_dur,), dtype=to.float32).pin_memory()
#         logger.info('E')
#
def get_memory_usage():
    process = psutil.Process(os.getpid())
    return process.memory_percent()

def main():

    # train_args = parse_config('train_conf')
    word_cnt = 680
    lst = []
    for i in range(8):
        a=to.randn(5_000_000_000 // 4, dtype=to.float32)
        logger.info(i)
        lst.append(a)
    # encoder, word_worker, word_loss, word_contr, reg_worker = \
    #     model_init(train_args, word_cnt, 0)
    # print_memory_usage()
    # onesided_context = get_context_size(encoder.network)
    # print_memory_usage()
    # suffix = '_raw_ood'
    # wordf = f'data_libri{suffix}/words_mapping_600'
    # word_mapping = read_word_mapping_f(wordf)
    # dataholder = RawWordDataholder(f'data_libri{suffix}/word_data_train_600.pkl', f'data_libri{suffix}/960_feats_600.hdf',
    #                                f'data_libri{suffix}/word_data_test_600.pkl', f'data_libri{suffix}/word_ood_test.pkl', word_mapping, 2048,
    #                                subsampling_factor=3, onesided_context=onesided_context, small_set=False,
    #                                smooth=True, smooth_exp=0.6,
    #                                shuffle_grouped=True, pitch_aug=False, add_aug=True,
    #                                aug_snr=15, use_cuda=True)
    # dataholder.tpool.shutdown()
    logger.info('Done')

def main_simple():
    b = to.randn(1000).cuda()
    lst = []
    for i in range(8):
        a=to.randn(5_000_000_000 // 4, dtype=to.float32)
        perc = get_memory_usage()
        logger.info(f'{i} {perc}')
        lst.append(a)
    logger.info('Done')

main_simple()