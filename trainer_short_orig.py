import os
import random
import shutil
import subprocess as sp
import sys

import numpy as np
import regex as re
import torch as to
if to.cuda.is_available(): to.cuda.current_device()
from loguru import logger

from model import Tdnnf, WordWorkerNoOrder, WordContrastiveLoss, WordClassifier, Encoder, SupConLoss, Bottleneck, WordWorkerRnn, RegressionWorker, WordContrastiveLossAlt
from misc.misc import parse_config


def model_init(train_args, word_cnt, num_speakers=0):
    encoder = Encoder(quant_dim=train_args.quant_dim, repr_dim=train_args.repr_dim, input_dim=40,
                      dropout=train_args.drop_r, quantise=train_args.quantise, use_emb=train_args.use_emb,
                      larger_model=train_args.larger_model, use_norm=train_args.use_norm, use_custom=train_args.use_custom)

    word_worker = WordWorkerNoOrder(repr_dim=train_args.repr_dim)


    word_loss = WordClassifier(train_args.repr_dim, word_cnt, 1.0, train_args.bce_loss)

    word_contr = WordContrastiveLossAlt(0.2)

    reg_worker = RegressionWorker(train_args.repr_dim, 60)
    return encoder, word_worker, word_loss, word_contr, reg_worker


def save_checkpoint(model_d, epoch, lr, optimizer, encoder, word_loss, word_contr, word_worker, reg_worker, scheduler):
    fpath = f'{model_d}/{epoch}.tar'
    logger.info(f'Saving {fpath}')
    encoder.cpu(), word_worker.cpu()
    to.save({
        'encoder': encoder.state_dict(),
        'word_worker': word_worker.state_dict(),
        'word_loss': word_loss.state_dict(),
        'word_contr': word_contr.state_dict(),
        'reg_worker': reg_worker.state_dict(),
        'optimizer': optimizer.state_dict(),
        'scheduler': scheduler.state_dict(),
        'lr': lr
    }, fpath)


def load_checkpoint(fpath, encoder, word_loss, word_contr, word_worker, reg_worker):
    if not os.path.exists(fpath):
        logger.info(f'Model {fpath} does not exist!')
    checkpoint = to.load(fpath, map_location='cpu')
    encoder.load_state_dict(checkpoint['encoder'])
    word_loss.load_state_dict(checkpoint['word_loss'])
    word_worker.load_state_dict(checkpoint['word_worker'])
    word_contr.load_state_dict(checkpoint['word_contr'])
    reg_worker.load_state_dict(checkpoint['reg_worker'])
    lr = checkpoint['lr']
    return lr


def train_job(model_d, epochs, epochs_trained, iters_trained, word_cnt, train_args):
    gpumem = 10
    #if train_args.batch_size > 1024:
    #    gpumem = 20

    logf = f'{model_d}/trainlog{epochs_trained}'
    cmd = f"utils/queue.pl -V -l q_short_gpu --totmem 50G -l gpumem={gpumem} --fast 1 {logf} python train_job.py {model_d} {train_args.config_f}" \
    f" -target-epochs {epochs} -epochs-trained {epochs_trained} -iters-trained {iters_trained} " \
    f' -word-cnt {word_cnt} '
    result = sp.run(cmd, shell=True)
    if result.returncode != 0:
        logger.warning('FAILED!')
        sys.exit(0)
    with open(logf) as fh:
        text = fh.read()
        epochs_done = re.search(r'(?<=epochs=)\d+', text).group(0)
        iters_done = re.search(r'(?<=iters=)\d+', text).group(0)
    epochs_trained = int(epochs_done)
    iters_trained = int(iters_done)
    return epochs_trained, iters_trained


def train(model_d, continue_from, train_args):

    suffix = '_short'
    word_cnt = len(open(f'data_libri{suffix}/words_mapping_750').read().splitlines())

    if continue_from == -1:
        num_speakers = 0
        if train_args.use_spk:
            spk_f = f'data_libri{suffix}/spk_map_750'
            num_speakers = len(open(spk_f).read().splitlines())

        encoder, word_worker, word_loss, word_contr, reg_worker = \
            model_init(train_args, word_cnt, num_speakers)

        optimizer = to.optim.AdamW(list(encoder.parameters()) + list(word_worker.parameters()) + list(word_loss.parameters()) + list(word_contr.parameters()),
                                   lr=train_args.lr, weight_decay=0., betas=(0.9, 0.99))

        scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.25, threshold=1e-3,
                                                            patience=train_args.patience, verbose=True, cooldown=5, min_lr=1e-6)
        save_checkpoint(model_d, 0, train_args.lr, optimizer, encoder, word_loss, word_contr, word_worker, reg_worker, scheduler)
        del encoder, word_worker, word_loss, word_contr, reg_worker, optimizer, scheduler
    epochs_trained = continue_from if continue_from >= 0 else 0
    iters_trained = 0
    while epochs_trained < train_args.epochs:
        # logger.info('Submitting job.')
        epochs_trained, iters_trained = train_job(model_d, train_args.epochs, epochs_trained, iters_trained, word_cnt,
                                                  train_args)


def main(model_d, conf_f, continue_from: ('Continue training from last model', 'option', None, int) = -1,
         del_conf: ('', 'flag', None) = False):
    to.manual_seed(0)
    np.random.seed(0)
    to.cuda.manual_seed(0)
    random.seed(0)
    if continue_from == -1:
        train_args = parse_config(conf_f)

    if continue_from == -1:
        if os.path.exists(model_d):
            shutil.rmtree(model_d)
    os.makedirs(model_d, exist_ok=True)
    if continue_from == -1:
        shutil.copyfile(conf_f, f'{model_d}/conf')
        if del_conf:
            os.remove(conf_f)

    if continue_from >= 0:
        train_args = parse_config(f'{model_d}/conf')
    train_args.config_f = f'{model_d}/conf'

    pid = os.getpid()
    with open(f'{model_d}/pid', 'w') as fh:
        fh.write(str(pid))
    logger.info(f'Directory used is {model_d}')
    logger.remove()
    train(model_d, continue_from, train_args)


if __name__ == '__main__':
    import plac; plac.call(main)
