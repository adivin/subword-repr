import math
import time
import wandb
import os
import sys
import pickle
import psutil
import numpy as np
import torch as to
if to.cuda.is_available(): to.cuda.current_device()
import torch.nn as nn
import torch.nn.functional as fu
from loguru import logger
from torch.utils.tensorboard import SummaryWriter
import random
from collections import defaultdict, Counter
import wandb

from data_rawaudio import RawWordDataholder
from trainer import get_context_size, change_lr, spectrogram_as_target, run_eval_wordchunks_constrast, report_grad_norms, change_lr_to
from trainer_short import load_checkpoint, save_checkpoint, model_init
from misc.misc import parse_config, read_word_mapping_f
from layers import grad_normaliser
from torchaudio.transforms import FrequencyMasking, TimeMasking
from pytorch_lightning.metrics.classification import Recall, Precision, Accuracy
from model import JitterLayer


def get_current_lr(optimizer):
    lr = None
    for param_group in optimizer.param_groups:
        lr = param_group['lr']
    return lr


class VarTracker:
    def __init__(self, size):
        self.lst = []
        self.size = size

    def add(self, x):
        self.lst.append(x.detach().clone())
        if len(self.lst) > self.size:
            self.lst = self.lst[1:]

    def get(self):
        if len(self.lst) < 5:
            return 0.
        xs = to.stack(self.lst, dim=0)
        var = to.var(xs, dim=0)
        var = var.mean()
        return var


def train_job(model_d, config_f, target_epochs: ('', 'option', None, int), epochs_trained: ('', 'option', None, int),
              iters_trained: ('', 'option', None, int), word_cnt: ('', 'option', None, int), uid: ('', 'option', None) = ''):
    name = os.path.basename(model_d)
    to.set_printoptions(sci_mode=False, linewidth=100000000000000)
    to.manual_seed(0)
    np.random.seed(0)
    random.seed(1)
    train_args = parse_config(config_f)
    assert not (train_args.repr_drop and train_args.jitter)

    wandb.init(project='phone-models', name=name, id=uid + '4' if uid else name + '4', resume='allow', dir=model_d)
    wandb.config.update(train_args.__dict__)

    suffix = '_raw_1200_ordered'
    num_speakers = 0
    word_cnt_start = 600

    encoder, word_worker, word_loss, word_contr, reg_worker = \
        model_init(train_args, word_cnt, num_speakers)

    onesided_context = get_context_size(encoder.network)

    wordf = f'data_libri{suffix}/words_mapping'
    word_mapping = read_word_mapping_f(wordf)

    dataholder = RawWordDataholder(f'data_libri{suffix}/word_data_train.pkl', f'data_libri{suffix}/960_feats.hdf',
                                f'data_libri{suffix}/word_data_test.pkl', f'data_libri{suffix}/word_ood_test.pkl', word_mapping, train_args.batch_size,
                                subsampling_factor=3, onesided_context=onesided_context, small_set=train_args.small_set,
                                smooth=train_args.smooth, smooth_exp=train_args.smooth_exp, use_cuda=True, shuffle_grouped=train_args.shuffle_grouped,
                                pitch_aug=train_args.pitch_aug, add_aug=train_args.add_aug, aug_snr=train_args.aug_snr,
                                   num_negs=train_args.num_negs)

    if os.path.exists(f'{model_d}/iter.pkl'):
        with open(f'{model_d}/iter.pkl', 'rb') as fh:
            dct = pickle.load(fh)
            dataholder.set_state(dct)

    lr = load_checkpoint(f'{model_d}/{epochs_trained}.tar', encoder, word_loss, word_contr, word_worker, reg_worker)
    accumulate_count = train_args.accumulate_count
    lr *= accumulate_count

    encoder.cuda(),  word_loss.cuda(), word_contr.cuda()
    all_modelparams = list(encoder.parameters())  + list(word_loss.parameters())

    optimizer = to.optim.AdamW(list(encoder.parameters())  + list(word_loss.parameters()) + list(word_contr.parameters()),
                               lr=lr, weight_decay=0., betas=(0.9, 0.99))
    parameters_weight_loss = [v for k, v in list(encoder.named_parameters()) if 'weight' in k and 'quant' not in k]
    parameters_lone_weight_loss = encoder.quantiser.parameters()

    checkpoint = to.load(f'{model_d}/{epochs_trained}.tar')
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.25, patience=train_args.patience, threshold=1e-3, verbose=True, cooldown=5, min_lr=1e-6)
    scheduler.load_state_dict(checkpoint['scheduler'])
    start_time = time.time()
    start_iter_num = iters_trained
    last_iter_save = iters_trained
    param_norms = None
    to.set_printoptions(linewidth=120, threshold=1000000)
    if train_args.do_specaug:
        freq_mask = FrequencyMasking(train_args.max_num_freq_mask, iid_masks=train_args.iid_mask)
        time_mask = TimeMasking(train_args.max_num_time_mask, iid_masks=train_args.iid_mask)

    div_loss_factor = train_args.div_loss
    xent_loss_factor = train_args.xent_loss
    use_xent = True
    if xent_loss_factor == 0.:
        use_xent = False
        xent_loss_factor = 1.
    var_tracker = VarTracker(10)

    while time.time() - start_time < 2.5 * 3600 and epochs_trained < target_epochs:
        encoder.cuda()
        word_loss.cuda(), word_contr.cuda()
        encoder.train(), word_loss.train(), word_contr.train()

        if epochs_trained == train_args.first_lr_drop:
            change_lr(lr, 0.33, optimizer)
        if epochs_trained == train_args.second_lr_drop:
            change_lr(lr, 0.33, optimizer)
        optimizer.zero_grad()
        for j, (f, f_orig, t, neg_indcs, durs, spks) in enumerate(dataholder.iter_train_simple()):
            if train_args.decay_sm_temp:
                lambda_ = -math.log(train_args.decay_sm_temp) / train_args.decay_sm_period
                iters_trained_min = min(iters_trained, train_args.decay_sm_period)
                encoder.quantiser.temp = 1. * math.exp(-lambda_ * iters_trained_min)
            if train_args.decay_div:
                lambda_ = -math.log(0.1) / train_args.decay_div_period
                iters_trained_min = min(iters_trained, train_args.decay_div_period)
                div_loss_factor = train_args.div_loss * math.exp(-lambda_ * iters_trained_min)
            if train_args.decay_gamma:
                lambda_ = -math.log(train_args.decay_gamma) / train_args.decay_gamma_period
                iters_trained_min = min(j, train_args.decay_gamma_period)
                word_contr.gamma = train_args.gamma * math.exp(-lambda_ * iters_trained_min)

            # f_targ = spectrogram_as_target(f_orig, onesided_context)
            # f_targ = f_targ[:, :, ::3].permute(0, 2, 1).contiguous()

            f_joined = to.cat((f, f_orig,), dim=0)
            if train_args.do_specaug:
                f_joined = time_mask(f_joined)

            h_ortho, h_sm = encoder(f_joined)

            bs = t.size(0)
            closs = word_contr(h_ortho[:bs], h_ortho[bs:], t, neg_indcs, durs)

            # h_word = word_worker(h_quant, dur_cnts)
            #
            # if use_xent:
            #     wloss, out = word_loss(h_word, t, return_pred=True)
            # else:
            #     wloss, out = word_loss(h_word.detach(), t, return_pred=True)

            h_soft_summed = h_sm.sum(dim=[0, 1])
            h_soft_summed /= h_soft_summed.sum()
            div_loss = to.sum(h_soft_summed * to.log(h_soft_summed + 1e-10)) + to.log(to.tensor(train_args.quant_dim, device=h_ortho.device, dtype=to.float32))
            div_loss = div_loss.clamp(min=train_args.div_clamp)

            l2_loss = sum(w.norm() for w in parameters_weight_loss)
            l1_loss = sum(w.norm(p=1) for w in parameters_lone_weight_loss)

            totloss = (closs + div_loss_factor * div_loss +
                       train_args.w_decay * l2_loss + train_args.proj_w_loss * l1_loss) / accumulate_count

            totloss.backward()
            if train_args.clip_grad:
                nn.utils.clip_grad_value_(all_modelparams, 3.0)

            wandb.log({'iter': iters_trained, 'closs': closs.item(), 'div_loss': div_loss, 'grad var': var_tracker.get()})

            if (j+1) % accumulate_count == 0:
                if iters_trained % 150 == 0:
                    logger.info(f'Done {iters_trained} iterations - contr loss {closs.item():.4f} '
                                f'div loss {div_loss.item():.4f} - LR {lr:.8f}')
                    report_grad_norms(model_d, encoder, word_worker, word_contr, iter_num=iters_trained, param_norms=param_norms)
                # logger.info(f'grad var {var_tracker.get()}')

                var_tracker.add(encoder.quantiser.linear.weight.grad)
                optimizer.step()
                optimizer.zero_grad()

            if time.time() - start_time >= 2.5 * 3600:
                break
            iters_trained += 1
            if iters_trained > 1500 and div_loss.item() > 5.:
                return
        epochs_trained += 1
        if (iters_trained >= last_iter_save + 1000 or time.time() - start_time >= 2.5 * 3600 or epochs_trained == target_epochs) \
                and not train_args.small_set and not train_args.simple_setisf:
            test_closs = run_eval_wordchunks_constrast(encoder, word_worker, dataholder, word_loss, word_contr,
                                                    train_args, word_cnt, iter_num=iters_trained)
            lr = get_current_lr(optimizer)
            save_checkpoint(model_d, f'{epochs_trained}', lr, optimizer, encoder, word_loss, word_contr, word_worker, reg_worker, scheduler)
            last_iter_save = iters_trained
    logger.info('Ending job.')
    save_checkpoint(model_d, f'{epochs_trained}', lr, optimizer, encoder, word_loss, word_contr, word_worker, reg_worker, scheduler)
    print(f'epochs={epochs_trained} iters={iters_trained}\n', file=sys.stderr)
    with open(f'{model_d}/iter.pkl', 'wb') as fh:
        pickle.dump(dataholder.get_state(), fh)


def train_onebatch(model_d, config_f, target_epochs: ('', 'option', None, int), epochs_trained: ('', 'option', None, int),
              iters_trained: ('', 'option', None, int), word_cnt: ('', 'option', None, int), uid: ('', 'option', None) = ''):
    to.set_printoptions(sci_mode=False, linewidth=100000000000000)
    to.manual_seed(0)
    np.random.seed(0)
    random.seed(1)
    train_args = parse_config(config_f)
    suffix = '_raw_1200_ordered'
    num_speakers = 0
    word_cnt_start = 600

    name = os.path.basename(model_d)
    wandb.init(project='phone-models', name=name, id=name + str(wandb.util.generate_id()), resume='allow', dir=model_d)
    wandb.config.update(train_args.__dict__)

    encoder, word_worker, word_loss, word_contr, reg_worker = \
        model_init(train_args, word_cnt, num_speakers)

    onesided_context = get_context_size(encoder.network)

    wordf = f'data_libri{suffix}/words_mapping'
    word_mapping = read_word_mapping_f(wordf)

    dataholder = RawWordDataholder(f'data_libri{suffix}/word_data_train.pkl', f'data_libri{suffix}/960_feats.hdf',
                                   f'data_libri{suffix}/word_data_test.pkl', f'data_libri{suffix}/word_ood_test.pkl', word_mapping, train_args.batch_size,
                                   subsampling_factor=3, onesided_context=onesided_context, small_set=True,
                                   smooth=train_args.smooth, smooth_exp=train_args.smooth_exp, use_cuda=True, shuffle_grouped=train_args.shuffle_grouped,
                                   pitch_aug=train_args.pitch_aug, add_aug=train_args.add_aug, aug_snr=train_args.aug_snr, num_negs=train_args.num_negs)
    # with open(f'data_libri{suffix}/960_word_data_train_{word_cnt_start}.pkl', 'rb') as fh:
    #     lst = pickle.load(fh)
    #     random.shuffle(lst)
    if os.path.exists(f'{model_d}/iter.pkl'):
        with open(f'{model_d}/iter.pkl', 'rb') as fh:
            dct = pickle.load(fh)
            dataholder.set_state(dct)

    lr = load_checkpoint(f'{model_d}/{epochs_trained}.tar', encoder, word_loss, word_contr, word_worker, reg_worker)

    encoder.cuda(), word_worker.cuda(), word_loss.cuda(), reg_worker.cuda(), word_contr.cuda()
    all_modelparams = list(encoder.parameters()) + list(word_worker.parameters()) + list(word_loss.parameters())

    if not train_args.use_sgd:
        optimizer = to.optim.AdamW(list(encoder.parameters()) + list(word_worker.parameters()) + list(word_loss.parameters()) + list(word_contr.parameters()),
                               lr=train_args.lr, weight_decay=0., betas=(0.9, 0.99))
    else:
        optimizer = to.optim.SGD(list(encoder.parameters()) + list(word_worker.parameters()) + list(word_loss.parameters()) + list(word_contr.parameters()), lr=train_args.lr, momentum=0.9)

    parameters_weight_loss = [v for k, v in list(encoder.named_parameters()) + list(word_worker.named_parameters()) if 'weight' in k and 'quant' not in k]
    parameters_lone_weight_loss = encoder.quantiser.parameters()

    checkpoint = to.load(f'{model_d}/{epochs_trained}.tar')
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.25, patience=train_args.patience, threshold=1e-3, verbose=True, cooldown=5, min_lr=1e-6)
    scheduler.load_state_dict(checkpoint['scheduler'])
    start_time = time.time()
    start_iter_num = iters_trained
    last_iter_save = iters_trained
    param_norms = None
    to.set_printoptions(linewidth=120, threshold=1000000)
    if train_args.do_specaug:
        freq_mask = FrequencyMasking(train_args.max_num_freq_mask, iid_masks=train_args.iid_mask)
        time_mask = TimeMasking(train_args.max_num_time_mask, iid_masks=train_args.iid_mask)

    div_loss_factor = train_args.div_loss
    xent_loss_factor = train_args.xent_loss
    use_xent = True
    if xent_loss_factor == 0.: use_xent = False
    xent_loss_factor = 1.
    encoder.cuda()
    word_worker.cuda()
    word_loss.cuda(), reg_worker.cuda(), word_contr.cuda()
    encoder.train(), word_worker.train(), word_loss.train(), word_contr.cuda()
    var_tracker = VarTracker(10)
    f, f_orig, t, neg_indcs, durs, spks, utts = next(dataholder.iter_train_simple(return_utts=True))
    for j in range(500):
        if train_args.decay_sm_temp:
            lambda_ = -math.log(train_args.decay_sm_temp) / train_args.decay_sm_period
            iters_trained_min = min(j, train_args.decay_sm_period)
            encoder.quantiser.temp = 1. * math.exp(-lambda_ * iters_trained_min)
        if train_args.decay_div:
            lambda_ = -math.log(0.1) / train_args.decay_div_period
            iters_trained_min = min(j, train_args.decay_div_period)
            div_loss_factor = train_args.div_loss * math.exp(-lambda_ * iters_trained_min)
        if train_args.decay_gamma:
            lambda_ = -math.log(train_args.decay_gamma) / train_args.decay_gamma_period
            iters_trained_min = min(j, train_args.decay_gamma_period)
            word_contr.gamma = train_args.gamma * math.exp(-lambda_ * iters_trained_min)

        #if j == 400:
        #    change_lr(lr, 0.1, optimizer)

        optimizer.zero_grad()

        f_joined = to.cat((f, f_orig,), dim=0)
        h_ortho, h_sm = encoder(f_joined)

        bs = t.size(0)
        loss = word_contr(h_ortho[:bs], h_ortho[bs:], t, neg_indcs, durs)

        h_soft_summed = h_sm[:,:,:].sum(dim=[0, 1])
        h_soft_summed /= h_soft_summed.sum()
        div_loss = to.sum(h_soft_summed * to.log(h_soft_summed + 1e-10)) + to.log(to.tensor(train_args.quant_dim, device=h_ortho.device, dtype=to.float32))
        div_loss = div_loss.clamp(min=train_args.div_clamp)

        l2_loss = sum(w.norm() for w in parameters_weight_loss)
        l1_loss = sum(w.norm(p=1) for w in parameters_lone_weight_loss)

        totloss = loss + div_loss_factor * div_loss + train_args.w_decay * l2_loss + train_args.proj_w_loss * l1_loss
        totloss.backward()
        var_tracker.add(encoder.quantiser.linear.weight.grad)
        if train_args.clip_grad:
            nn.utils.clip_grad_value_(all_modelparams, 3.0)

        optimizer.step()

        if train_args.norm_embeds:
            norm = encoder.quantiser.linear.weight.data.norm(p=2, dim=1, keepdim=True)

        wandb.log({'loss': loss.item(), 'dloss': div_loss.item()})
        logger.info(f'vargrad {var_tracker.get()}')
        logger.info(f'Done {iters_trained} iterations - DTW loss {loss.item():.4f} div loss {div_loss.item():.4f}')
        report_grad_norms(model_d, encoder, word_worker, word_loss, reg_worker, iter_num=iters_trained, param_norms=param_norms)

        iters_trained += 1
    save_checkpoint(model_d, 1, lr, optimizer, encoder, word_loss, word_contr, word_worker, reg_worker, scheduler)


if __name__ == '__main__':
    import plac; plac.call(train_onebatch)


