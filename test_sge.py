from dask_jobqueue import SGECluster
from dask.distributed import Client
import time
from loguru import logger
import multiprocessing as mp


def func(arg):
    time.sleep(arg)
    logger.info(f'done {arg}')
    return arg


def main():
    logd = 'testlog'
    cluster = SGECluster(queue='q_1day', processes=1, cores=1, local_directory=logd, log_directory=logd,
                         memory='25GB', resource_spec='q_1day', shebang='/bin/bash', job_extra=['-P parole'])

    cluster.scale(1)

    client = Client(cluster)
    logger.info('submitting')
    futures = []
    for i in range(1):
        futures.append(client.submit(func, i+3))

    for future in futures:
        print(future.result())
    print('done')
    cluster.scale(0)
    client.close()
    cluster.close()
    print('end')


main()
