import plac
import numpy as np


def main(inf, outfa, outfb):
    num_bytes_vec = 256 * 4
    fha = open(outfa, 'wb')
    fhb = open(outfb, 'wb')
    with open(inf, 'rb') as fh:
        text = fh.read(10_000_000 * num_bytes_vec)
        data = np.fromstring(text, dtype=np.float32)
        del text
        data.tofile(fha, format='b')
        del data
        text = fh.read(7_000_000 * num_bytes_vec)
        data = np.fromstring(text, dtype=np.float32)
        del text
        data.tofile(fha, format='b')
        del data

        text = fh.read(10_000_000 * num_bytes_vec)
        data = np.fromstring(text, dtype=np.float32)
        del text
        data.tofile(fhb, format='b')
        del data

        text = fh.read()
        data = np.fromstring(text, dtype=np.float32)
        del text
        data.tofile(fhb, format='b')

    fha.close()
    fhb.close()


plac.call(main)