import numpy as np
import fast
import itertools
from loguru import logger


def convert_condensed_idx(idx, lst):
    return lst[idx]


def sumn(n):
    return n*(n+1) / 2


def convert_to_condensed_idx(i, j, n):
    if i > j:
        i, j = j, i
    idx = sumn(n) - sumn(n-i) + j - i
    return int(idx)


def main(num_f, words_f):
    logger.info('Started.')
    word_info = {}
    total_cnt = 0
    with open(words_f) as fh:
        for line in fh:
            i, word, uttid = line.split()
            i =int(i)
            if i > 100_000:
                continue
            word_info[i] = [word, uttid, -1]
            total_cnt += 1

    word_mapping = {}
    with open('data/words_mapping') as fh:
        for line in fh:
            word, id_ = line.split()
            word_mapping[word] = int(id_)
            word_mapping[int(id_)] = word

    with open(num_f, 'rb') as fh:
        text = fh.read(20_000 * 256 * 4)
        data = np.fromstring(text, dtype=np.float32)
    data = data.reshape(-1, 256)

    lst = list(itertools.combinations(range(data.shape[0]), 2))
    logger.info('Starting to get costs.')
    costs = fast.pdist(data)
    logger.info('Done getting costs.')
    indcs = np.argsort(costs)

    cnt = 0
    testnum = 1000
    for i, idx in enumerate(indcs[:testnum]):
        min = costs[idx]
        real_idx = convert_condensed_idx(idx, lst)
        w1 = word_info[real_idx[0]][0]
        w2 = word_info[real_idx[1]][0]
        if w1 == w2:
            cnt += 1
        if i < 50:
            print(min, word_info[real_idx[0]][0], word_info[real_idx[1]][0])#, word_info[real_idx[0]][1], word_info[real_idx[1]][1])
    print(cnt / testnum)



import plac
plac.call(main)