import openfst_python as fst
import math as m


def calc_lexppl(lines):
    dct_phones = {}
    word2pron = {}
    max_pron_len = 0
    for line in lines:
        linesp = line.split()
        pron = linesp[1:]
        word2pron[linesp[0]] = pron
        if len(pron) > max_pron_len:
            max_pron_len = len(pron)
        for p in pron:
            if '#' not in p:
                if p not in dct_phones:
                    dct_phones[p] = len(dct_phones)

    with open('phones.txt', 'w') as fh:
        for k, v in dct_phones.items():
            fh.write(f'{k} {v}\n')

    f = fst.Fst()
    c = fst.Weight.One(f.weight_type())
    start = f.add_state()
    for w, pron in word2pron.items():
        for i, p in enumerate(pron):
            s = f.add_state()
            if i == 0:
                f.add_arc(start, fst.Arc(dct_phones[p], dct_phones[p], c, s))
            else:
                f.add_arc(s - 1, fst.Arc(dct_phones[p], dct_phones[p], c, s))
            if i == len(pron) - 1:
                f.set_final(s, c)

    f.set_start(start)
    f = fst.determinize(f)
    f.minimize()
    f.verify()
    ppl = 0
    num_phones = len(dct_phones)
    for state in f.states():
        cnt = 0
        for i, _ in enumerate(f.arcs(state)):
            cnt += 1
        if cnt != 0:
            ppl -= (1/num_phones) * m.log2(1 / cnt)
    return ppl


def main(inf):
    lines = open(inf).read().splitlines()
    ppl = calc_lexppl(lines)
    print(ppl)


if __name__ == '__main__':
    import plac
    plac.call(main)