import torch as to
import numpy as np
import torch.nn.functional as F
from soft_dtw_cuda import SoftDTW
from model import DTWLoss
from sklearn import datasets
from layers import ReluBasic
from loguru import logger

def test(use_cuda):
    to.manual_seed(1)
    np.random.seed(0)

    np.set_printoptions(linewidth=500, suppress=True)
    device = to.device('cpu') if not use_cuda else to.device('cuda')
    a = to.randn(2, 3, 3, device=device)
    b = to.randn(2, 5, 3, device=device)
    c = F.pad(a, (0, 0, 0, 2,))
    w = to.randn(3,3, device=device) / 10.
    w.requires_grad = True

    sdtw = SoftDTW(use_cuda=use_cuda)

    x_lens=to.tensor([3, 3], device=device)
    y_lens=to.tensor([5, 5], device=device)

    x = a@w
    y = b@w
    loss = sdtw(x, y, x_lens, y_lens)
    loss = loss.mean()
    loss.backward()
    print(loss.item(), w.grad)
    w.grad = None
    print()

    x_lens=to.tensor([3, 3], device=device)
    y_lens=to.tensor([5, 5], device=device)
    if use_cuda:
        x_lens=x_lens.cuda()
        y_lens=y_lens.cuda()
    x = c@w
    y = b@w
    loss = sdtw(x, y, x_lens, y_lens).mean()
    loss.backward()
    print(loss.item(), w.grad)
    w.grad = None


def test_loss():
    to.set_printoptions(linewidth=1000000000, sci_mode=False)
    np.set_printoptions(linewidth=1000000000, suppress=True)
    # N = 10
    # a = to.randn(N, 16, 64).cuda()
    x, _ = datasets.make_blobs(8, 12, centers=4, shuffle=False)
    # x = np.random.randn(12, 8)
    a = to.from_numpy(x[::2])
    a = F.pad(a, (0, 0, 0, 4,))
    b = to.from_numpy(x)
    b += to.randn_like(b) / 10.

    y, _ = datasets.make_blobs(8, 12, centers=4, shuffle=False)
    c = to.from_numpy(y[::2])
    c = F.pad(c, (0, 0, 0, 4,))
    d = to.from_numpy(y)
    d += to.randn_like(d) / 10.

    a = to.stack((a, b, c, d), dim=0).float()
    # a = to.from_numpy(a).float()
    #a = a.cuda()
    w = ReluBasic(12, 16, residual=False, use_layernorm=True, no_final_ops=True)
    #w.cuda()
    t = to.tensor([0, 0, 1, 1])#.cuda()
    x_lens = to.tensor([4, 8, 4, 8])#.cuda()
    best_loss = None
    params = None
    # for lr in [0.01, 0.1, 1.]:
    #     for gamma in [0.1, 1., 10.]:
    # print(lr, gamma)
    criterion = DTWLoss(False, gamma=0.1, cosine_temp=1., use_orig=False, contr_temp=1.)
    optim = to.optim.SGD(w.parameters(), lr=0.01, momentum=0., weight_decay=0.)
    t_negs = to.tensor([[2, 3], [2, 3], [0, 1], [0, 1]])
    for i in range(10):
        optim.zero_grad()
        # logger.info(f'nans a {to.isnan(a).sum()}')
        out = w(a)
        out = out / out.norm(p=2, keepdim=True, dim=-1)
        # logger.info(f'nans out {to.isnan(out).sum()}')
        out.register_hook(lambda x: logger.info(f'grad {x.shape} {x.norm(p=2, dim=-1)}'))
        loss = criterion(out, out, t, t_negs, x_lens)
        print(f'loss {loss.item()}')
        loss.backward()
        # print(w.fc1.weight.grad)
        optim.step()
    #if best_loss is None or loss.item() < best_loss:
    #    best_loss = loss.item()
    #    params = (lr, gamma)

def test_mag():
    to.set_printoptions(linewidth=1000000000)
    a,_ = datasets.make_blobs(12, 8, centers=4, shuffle=False)
    b,_ = to.randn(1,8,32)
    a /= a.norm(dim=-1, keepdim=True)
    b /= b.norm(dim=-1, keepdim=True)

    sdtw = SoftDTW(False, 1., normalize=False)

    r = to.tensor([8])
    print(sdtw(a, b, to.tensor([2]), r))
    print(sdtw(a, b, to.tensor([4]), r))
    print(sdtw(a, b, to.tensor([8]), r))
    print()
    print(sdtw(a, b, to.tensor([2]), to.tensor([2])))
    print(sdtw(a, b, to.tensor([4]), to.tensor([4])))


# test(False)
# print()
# test(True)
test_loss()
# test_mag()
