import os
import shutil
import subprocess as sp
import pickle
import soundfile as sf
import h5py as hdf
import random
from misc.misc import read_word_mapping_f

def check_ctm(ctm_f, outd, num):
    pass


def check_data(lst_f, feats_f, word_map_f, outd, num: ('', 'positional', None, int)):
    lst = pickle.load(open(lst_f, 'rb'))
    d = hdf.File(feats_f, 'r')['raw']
    random.shuffle(lst)

    word_map = read_word_mapping_f(word_map_f)

    if os.path.exists(outd):
        shutil.rmtree(outd)
    os.makedirs(outd)
    for tpl in lst[:num]:
        wid, dur, featidx, uttid, spkid, word_pos = tpl
        dur = int(dur * 16000)
        f = d[featidx: featidx + dur]
        word = word_map[wid]
        sf.write(f'{outd}/{word}.wav', f, 16000)


def check_ood(lst_f, feats_f, word_map_f, outd, num: ('', 'positional', None, int)):
    lst = pickle.load(open(lst_f, 'rb'))
    d = hdf.File(feats_f, 'r')['ood_test']
    #random.shuffle(lst)

    word_map = read_word_mapping_f(word_map_f)

    if os.path.exists(outd):
        shutil.rmtree(outd)
    os.makedirs(outd)
    for tpl in lst[:num]:
        wid, dur, featidx, uttid, spkid, word_pos = tpl
        dur = int(dur * 16000)
        print(featidx, dur)
        f = d[featidx: featidx + dur]
        word = word_map[wid]
        sf.write(f'{outd}/{word}.wav', f, 16000)


import plac; plac.call(check_ood)
