class TrainArgs:
    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __getattr__(self, attr):
        return 0

    def write(self, outf):
        with open(outf, 'w') as fh:
            for k, v in self.__dict__.items():
                fh.write(f'{k}={v}\n')


def parse_config(config_f):
    args = {'config_f': config_f}
    with open(config_f) as fh:
        for line in fh:
            if not line.strip() or line.startswith('#'):
                continue
            try:
                key, value = line.split('=')
            except ValueError:
                print(line)
                raise
            key = key.strip()
            value = value.strip()
            try:
                value = float(value)
            except ValueError:
                pass
            if isinstance(value, float):
                if value // 1 == value:
                    value = int(value)
            if value == "True":
                value = True
            elif value == "False":
                value = False
            args[key] = value
    train_args = TrainArgs(**args)
    return train_args


def read_word_mapping_f(word_f):
    word_mapping = {}
    with open(word_f) as fh:
        for line in fh:
            word, id = line.split()
            word_mapping[word] = int(id)
            word_mapping[int(id)] = word
    return word_mapping