""" Joining result of running kmeans on 4 different GPUs on all data """
from glob import glob
import pickle
from km import Cluster
from loguru import logger


splits = {'_a_': 0, '_b_': 1, '_c_': 2, '_d_': 3, '_e_': 4, '_f_': 5}


def main(cluster_glob, offset: ('', 'positional', None, int), outf_prefix):
    logger.info('Starting.')
    lstf = glob(cluster_glob)
    lst = []
    logger.info(f'Found following files {lstf}')
    for f in lstf:
        with open(f, 'rb') as fh:
            lstc = pickle.load(fh)
            for split, n in splits.items():
                if split in f:
                    for c in lstc:
                        c.coverage = [idx + n*offset for idx in c.coverage]
            lst.extend(lstc)
    logger.info(f'Found {len(lst)} clusters.')
    fpath = f'{outf_prefix}/cluster_start'

    logger.info(f'Creating new file {fpath}')
    with open(fpath, 'wb') as fh:
        pickle.dump(lst, fh)
    logger.info('Done.')


if __name__ == '__main__':
    import plac
    plac.call(main)