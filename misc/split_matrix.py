import numpy as np
from math import ceil
from loguru import logger

REPRDIM = 256


def main(inf, outd, num: ('', 'positional', None, int)):
    logger.info(f'Reprdim: {REPRDIM}')
    with open(inf, 'rb') as fh:
        bytes = fh.read(8)
        num_elements = int.from_bytes(bytes, 'little')

        logger.info(f'Num elements: {num_elements}')

        num_samples = (num_elements/256.)
        assert int(num_samples) == ceil(num_samples)
        per_file = ceil(num_samples / num)
        logger.info(f'Num samples per file {per_file}')
        with open(f'{outd}/num_per_file', 'w') as fhw:
            fhw.write(str(per_file))
        for i in range(1, 1 + num):
            data = np.frombuffer(fh.read(per_file * 4 * 256), dtype=np.float32)
            data = data.reshape(-1, REPRDIM)
            with open(f'{outd}/repr_nums_{i}', 'wb') as fhn:
                data.tofile(fhn, format='b')
            del data


import plac
plac.call(main)