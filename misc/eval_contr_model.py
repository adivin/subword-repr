import random

import numpy as np
import torch as to
from loguru import logger

from data_rawaudio import RawWordDataholder
from misc.misc import parse_config
from trainer import get_context_size
from trainer_short import model_init, load_checkpoint


def get_dur(durs, cnts, idx):
    tot = sum(cnts)
    i = 0
    counter = 0
    while counter + cnts[i] < idx:
        counter += cnts[i]
        i += 1
    return durs[i]


def main(model_f, conf_f):
    np.random.seed(0)
    random.seed(1)
    data_dir = 'data_libri_raw_1200_b'

    wordf = f'{data_dir}/words_mapping'
    word_mapping = {}
    with open(wordf) as fh:
        for line in fh:
            word, id = line.split()
            word_mapping[word] = int(id)
            word_mapping[int(id)] = word
    word_cnt = len(word_mapping) // 2

    train_args = parse_config(conf_f)
    encoder, word_worker, word_loss, word_contr, reg_worker = \
        model_init(train_args, word_cnt, 0)

    onesided_context = get_context_size(encoder.network)

    lr = load_checkpoint(model_f, encoder, word_loss, word_contr, word_worker, reg_worker)

    dataholder = RawWordDataholder(f'{data_dir}/word_data_train.pkl', f'{data_dir}/960_feats.hdf',
                                   f'{data_dir}/word_data_test.pkl', f'{data_dir}/word_ood_test.pkl', word_mapping, train_args.batch_size,
                                   subsampling_factor=3, onesided_context=onesided_context, small_set=True,
                                   smooth=train_args.smooth, smooth_exp=train_args.smooth_exp, use_cuda=True, shuffle_grouped=train_args.shuffle_grouped,
                                   pitch_aug=train_args.pitch_aug, add_aug=train_args.add_aug, aug_snr=train_args.aug_snr)

    phone_map = {}
    word_pron = {}
    with open(f'{data_dir}/lex_ortho') as fh:
        i = 1
        for line in fh:
            word, *phones = line.split()
            for phone in phones:
                if phone not in phone_map:
                    phone_map[phone] = i
                    phone_map[i] = phone
                    i += 1
            phones = [phone_map[phone] for phone in phones]
            word_pron[word_mapping[word]] = phones
    closest_words = {} # words which have one pron different
    for word, pron in word_pron.items():
        lst = []
        for other_word, other_pron in word_pron.items():
            if word == other_word: continue
            diff = set(pron) ^ set(other_pron)
            diff = len(diff)
            assert diff
            if (diff == 1 and (len(pron) > len(other_pron) or len(other_pron) > len(pron)) or
                diff == 2 and len(pron) == len(other_pron)):
                #print(word, other_word, diff)
                lst.append(other_word)
        #logger.info(f'{word} {lst}')
        closest_words[word] = to.tensor(lst, dtype=to.long).cuda()


    encoder.eval()
    word_worker.eval()
    word_loss.eval()
    word_contr.eval()
    to.set_printoptions(sci_mode=False, linewidth=10000000)
    with to.no_grad():
        total_loss = 0.
        total_closs = 0.
        encoder.cuda()
        word_worker.cuda()
        word_contr.cuda()
        short_words_set = set()
        for j, (f, f_orig, t, dur_cnts, spks, utts) in enumerate(dataholder.iter_ood_test(True)):
            durs, cnts = dur_cnts
            h, h_quant, h_pre = encoder(f)
            h_word = word_worker(h_quant, dur_cnts)

            closs, sim = word_contr(h_word, t, return_sim=True)
            total_closs += closs

            t = t.cpu()

            check_idx = 1
            dur = get_dur(durs, cnts, check_idx)
            indcs = h_quant[check_idx, :dur].argmax(dim=1)
            wid = t[check_idx].item()
            best = sim[check_idx].argmax()
            dur = get_dur(durs, cnts, check_idx)
            indcs_other = h_quant[best, :dur].argmax(dim=1)
            wid_other = t[best].item()
            logger.info(f'{word_mapping[wid]} {indcs} {utts[check_idx]} {best} {word_mapping[wid_other]} {indcs_other} {utts[best]}')

        total_closs /= (j+1)
        logger.info(f'Closs {total_closs.item()}')

import plac; plac.call(main)
