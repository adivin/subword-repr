import numpy as np
import h5py
import subprocess as sp
from glob import glob
import os
import pickle
from loguru import logger


def main(work, alidir):
    os.makedirs(f'{work}/log', exist_ok=True)
    lst_alis = glob(f'{alidir}/ali*gz')
    cnt = len(lst_alis)
    cmd = f'utils/queue.pl -l q1d -V JOB=1:{cnt} {work}/log/ali2pdf.JOB.log ali-to-pdf {alidir}/final.mdl "ark:gunzip -c {alidir}/ali.JOB.gz |" ark,t:{work}/pdf.JOB'
    sp.check_output(cmd, shell=True)

    sp.check_output(f'cat {work}/pdf.* > {work}/all_pdf.ark.txt', shell=True)
    logger.info('Reading in pdf in text form.')
    lst = []
    with open(f'{work}/all_pdf.ark.txt') as fh:
        for line in fh:
            uid, *pdfs = line.split()
            pdfs = [int(pdf) for pdf in pdfs]
            lst.append((uid, pdfs))
    logger.info('Creating pkl file.')
    with open(f'{work}/data_pdf.pkl', 'wb') as fh:
        pickle.dump(lst, fh, protocol=pickle.HIGHEST_PROTOCOL)


import plac; plac.call(main)