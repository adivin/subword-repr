import torch as to
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from sklearn import datasets
from model import DTWLoss
import matplotlib.pyplot as plt
from data_rawaudio import RawWordDataholder
from misc.misc import read_word_mapping_f, parse_config
from trainer_short import model_init, load_checkpoint
from trainer import get_context_size

def main(model_f, config_f):
    to.set_printoptions(sci_mode=False)
    suffix = '_raw_1200_ordered'
    word_cnt = 1082
    train_args = parse_config(config_f)
    wordf = f'data_libri{suffix}/words_mapping'
    word_mapping = read_word_mapping_f(wordf)

    encoder, word_worker, word_loss, word_contr, reg_worker = \
        model_init(train_args, word_cnt, -1)

    onesided_context = get_context_size(encoder.network)

    dataholder = RawWordDataholder(f'data_libri{suffix}/word_data_train.pkl', f'data_libri{suffix}/960_feats.hdf',
                                   f'data_libri{suffix}/word_data_test.pkl', f'data_libri{suffix}/word_ood_test.pkl', word_mapping, train_args.batch_size,
                                   subsampling_factor=3, onesided_context=onesided_context, small_set=True,
                                   smooth=train_args.smooth, smooth_exp=train_args.smooth_exp, use_cuda=False, shuffle_grouped=train_args.shuffle_grouped,
                                   pitch_aug=train_args.pitch_aug, add_aug=False, aug_snr=train_args.aug_snr, num_negs=train_args.num_negs)

    lr = load_checkpoint(model_f, encoder, word_loss, word_contr, word_worker, reg_worker)
    encoder.eval()
    f, f_orig, t, neg_indcs, durs, spks, utts = next(dataholder.iter_train_simple(return_utts=True))

    h_ortho, h_sm = encoder(f_orig)
    loss = word_contr(h_ortho, h_ortho, t, neg_indcs, durs)

    import IPython; IPython.embed()


import plac;
plac.call(main)
