import plac


def main(inf, s, offset: ('', 'positional', None, int)=0):
    indcs = s.split(',')
    indcs = set([int(v)+offset for v in indcs])
    lst = []
    with open(inf) as fh:
        lines = fh.read().splitlines()
    l = []
    for line in lines:
        if line.startswith('-'):
            size = len(l)
            assert size
            for i, onel in enumerate(l):
                idx, w, uid = onel.split()
                if int(idx) in indcs:
                    lst.append((idx, w, uid, f'{i+1}/{size}',))
            l = []
            continue
        l.append(line)

    for entry in lst:
        print(entry)

plac.call(main)
