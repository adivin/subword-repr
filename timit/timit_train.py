import h5py as hdf
import pickle
import random
import numpy as np
from loguru import logger
import concurrent.futures
import os
import shutil
import math

import torchaudio.functional as audiof
import torch as to
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from pytorch_lightning.metrics.classification import Accuracy, Recall, Precision

from model import Tdnnf, LabelSmoothingLoss, ConvGLU
from trainer import get_context_size
from misc.misc import parse_config
from layers import layer_norm

REPRDIM = 256

class PhoneDataholder:
    def __init__(self, feats_hdf, data_pkl, onesided_context_size, batch_size, offset=56):
        self.feats = hdf.File(feats_hdf, 'r')['feats'][:]
        self.lst = pickle.load(open(data_pkl, 'rb'))
        # assert onesided_context_size % 3 == 0, 'Code assumes this, might not work without, see get_batch'
        self.onesided_context_size = onesided_context_size
        self.batch_size = batch_size

        self.indcs = []
        self.output_size = 56
        self.chunk_size = 2 * self.onesided_context_size + self.output_size  # input number of frames

        self.offset = offset  # shift size between chunks used
        if offset is None:
            self.offset = self.output_size
        else:
            self.offset = offset  # shift size between chunks used
        idx = 0
        self.num_frames = 0
        for i, entry in enumerate(self.lst):  # entry is (feat_idx, [labels..])
            dur = len(entry[1])
            num_chunks_for_utterance = (dur - self.chunk_size) // self.offset + 1
            for j in range(num_chunks_for_utterance):
                chunk_offset = j * self.offset
                # index into self.lst, index into self.lst[i][1], feat idx
                idx_tpl = (idx, chunk_offset, entry[0] + chunk_offset, (entry[0], entry[0] + dur,))
                self.indcs.append(idx_tpl)
                self.num_frames += self.chunk_size
            idx += 1

        num_frames_unique = self.feats.shape[0]
        logger.info(f'Chunk size is {self.chunk_size}, num frames is {self.num_frames}, unique frames is {num_frames_unique}')

    def get_batch(self, indcs):
        chunk_size = self.chunk_size
        onesided_context = self.onesided_context_size
        feat = np.zeros((self.batch_size, chunk_size + onesided_context * 2, 40), dtype=np.float32)
        targ = np.zeros((self.batch_size, math.ceil(chunk_size / 3)), dtype=np.int64)
        # targ = np.zeros((self.batch_size, math.ceil(chunk_size)), dtype=np.int64)
        rand_offset = np.random.randint(0, 3, size=(self.batch_size,))
        for i, (utt_idx, offset_idx, feat_idx, tpl) in enumerate(indcs):
            start_idx, end_idx = feat_idx + rand_offset[i], feat_idx + chunk_size + rand_offset[i]
            left_extra, right_extra = 0, 0
            # logger.info(f'{start_idx} {end_idx} {tpl[0]} {tpl[1]} {onesided_context}')
            if (start_idx - tpl[0]) < onesided_context:
                left_extra = onesided_context - (start_idx - tpl[0])
                start_idx -= onesided_context - left_extra
            else:
                start_idx -= onesided_context
            if end_idx > (tpl[1] - onesided_context):
                right_extra = end_idx - (tpl[1] - onesided_context)
                end_idx += onesided_context - right_extra
            else:
                end_idx += onesided_context
            # logger.info(f'{left_extra} {right_extra}')
            f = self.feats[start_idx: end_idx]
            f = np.pad(f, ((left_extra, right_extra), (0, 0,)), mode='constant')
            assert f.shape[0] == feat.shape[1], f'{f.shape[0]} {feat.shape[1]}'
            feat[i] = f
            targ_idx = offset_idx
            labels = self.lst[utt_idx][1][targ_idx + rand_offset[i]: targ_idx + chunk_size + rand_offset[i]]
            if len(labels) < chunk_size:
                labels.append(labels[-1])
            labels = labels[::3]
            assert len(labels) == targ.shape[1], f'{len(labels)} {targ.shape[1]}'
            targ[i] = labels
        return to.as_tensor(feat).cuda(), to.as_tensor(targ, dtype=to.int64).cuda()

    def iter(self):
        bs = self.batch_size
        random.shuffle(self.indcs)
        num_iterations = len(self.indcs) // bs
        idx = 0
        indcs = self.indcs[idx: idx + bs]
        idx += bs
        f, t = self.get_batch(indcs)
        with concurrent.futures.ThreadPoolExecutor(2) as Tpe:
            for i in range(num_iterations - 1):
                indcs = self.indcs[idx: idx + bs]
                idx += bs
                future = Tpe.submit(self.get_batch, indcs)
                yield f, t
                f, t = future.result()


class PhoneClassifier(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.linear = nn.Linear(input_dim, output_dim)
        # self.linear.weight.data.fill_(1 / input_dim)

    def forward(self, x):
        return self.linear(x)


class TwoLayerPhoneClassifier(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.linear = nn.Linear(input_dim, 512)
        self.linear_b = nn.Linear(512, output_dim)
        self.linear.weight.data.fill_(1 / input_dim)
        self.linear_b.weight.data.fill_(1 / input_dim)

    def forward(self, x):
        return self.linear_b(F.relu(self.linear(x)))


def load_encoder(fpath, train_args, num_speakers):
    encoder = ConvGLU(40, 768, train_args.drop_r, stride=3)
    checkpoint = to.load(fpath, map_location='cpu')
    keys = {k for k in checkpoint['encoder'].keys() if 'network' in k}
    logger.info(keys)
    dct = {k.replace('network.', ''): v for k, v in checkpoint['encoder'].items() if k in keys}
    encoder.load_state_dict(dct)
    return encoder

class SM(nn.Module):
    def __init__(self, input_dim, dropout):
        super().__init__()
        self.affine = nn.Linear(80, 96)
        self.conv1 = nn.Conv1d(96, 768, 11)
        self.conv2 = nn.Conv1d(768, 1024, 3, stride=3)
        self.fc1 = nn.Linear(1024, 1024)

    def forward(self, x):
        x = self.affine(x)
        x = x.transpose(1, 2)
        x = F.relu(layer_norm(self.conv1(x)))
        x = F.relu(layer_norm(self.conv2(x))) 
        x = x.transpose(1, 2)
        x = F.relu(self.fc1(x))
        return x

def basic_trainer(dir, tb_writer, epochs, lr, bs, w_decay, basemodel_lr, dropout, encoder_f='', lr_half_step=10, num_speakers=0):

    if encoder_f != '':
        train_args = parse_config(os.path.join(os.path.dirname(encoder_f), 'conf'))
        model = load_encoder(encoder_f, train_args, num_speakers)
    else:
        model = ConvGLU(80, 768, dropout, stride=3)
        #model = SM(80, 0.)

    context_size = get_context_size(model)

    logger.info(f'Model uses padding, setting onesided context size to {context_size} anyways.')
    suffix = '_normed_39'
    train_data = PhoneDataholder(f'/idiap/temp/rbraun/code/subword-repr/timit_data{suffix}/train/data.hdf',
                                 f'/idiap/temp/rbraun/code/subword-repr/timit_data{suffix}/train/targs.pkl', context_size, bs)
    test_data = PhoneDataholder(f'/idiap/temp/rbraun/code/subword-repr/timit_data{suffix}/dev/data.hdf',
                                f'/idiap/temp/rbraun/code/subword-repr/timit_data{suffix}/dev/targs.pkl', context_size, bs, offset=None)

    phone_count = int(open(f'timit_data{suffix}/train/phone_count').read().strip())
    logger.info(f'Output dim is {phone_count}')
    classifier = PhoneClassifier(768, phone_count)

    # criterion = LabelSmoothingLoss(0.1, 40, 0).cuda()
    criterion = nn.CrossEntropyLoss()

    optimizer = to.optim.SGD(model.parameters(), lr=basemodel_lr * lr, weight_decay=w_decay, momentum=0.)
    class_optimizer = to.optim.SGD(classifier.parameters(), lr=lr, weight_decay=0., momentum=0.)
    start_lr = lr
    logger.info('Starting training.')
    running_loss, iternum = 0., 0
    model.cuda()
    classifier.cuda()
    best_loss = None
    best_idx = None
    train_accuracy = Accuracy()
    for i in range(epochs):
        model.train()
        classifier.train()
        for f, t in train_data.iter():
            iternum += 1
            optimizer.zero_grad()
            class_optimizer.zero_grad()
            delta = audiof.compute_deltas(f.transpose(1, 2)).transpose(1, 2)
            f = to.cat((f, delta,), dim=2)
            h = model(f)
            out = classifier(h.reshape(-1, h.size(2)))
            t_flat = t.view(-1)
            loss = criterion(out, t_flat)

            loss.backward()
            optimizer.step()
            class_optimizer.step()
            running_loss = 0.8 * running_loss + 0.2 * loss.item()

            tb_writer.add_scalar('Loss/xent', loss.item(), iternum)

        if (i+1) % lr_half_step == 0:
            for pgroup in optimizer.param_groups:
                pgroup['lr'] = pgroup['lr'] * 0.33
                lr = pgroup['lr']
            logger.info(f'New learning rate {lr:.4f}')
        # if i == epochs - 5:
        #     for pgroup in optimizer.param_groups:
        #         pgroup['lr'] = pgroup['lr'] * 0.1
        #         lr = pgroup['lr']
        #     logger.info(f'New learning rate {lr:.4f}')

        preds = out.reshape(-1, phone_count).argmax(dim=1)
        acc = train_accuracy(preds.cpu(), t_flat.cpu())
        logger.info(f'Train loss / acc - {running_loss:.2f} / {acc:.3f}')
        train_accuracy.reset()

        model.eval()
        classifier.eval()
        test_accuracy = Accuracy()
        test_recall = Recall()
        test_precision = Precision()
        with to.no_grad():
            total_loss, cnt = 0., 0
            correct = 0
            total_cnt = 0
            for f, t in test_data.iter():
                delta = audiof.compute_deltas(f.transpose(1, 2)).transpose(1, 2)
                f = to.cat((f, delta,), dim=2)
                h = model(f)
                out = classifier(h.view(-1, h.size(2)))
                t_flat = t.view(-1)
                total_loss += criterion(out, t_flat).item()
                # if cnt % 4 == 0:
                #     logger.info(to.log_softmax(out_flat, dim=-1)[:5])
                preds = to.argmax(out, dim=1)
                # logger.info(f'{preds[:15]} {t_flat[:15]}')
                preds = preds.cpu()
                t_flat = t_flat.cpu()
                test_accuracy(preds, t_flat)
                test_recall(preds, t_flat)
                test_precision(preds, t_flat)
                total_cnt += t.nelement()
                correct += to.sum(preds == t_flat).item()
                cnt += 1
            total_loss /= cnt
            total_acc = correct / total_cnt
            logger.info(f'Test loss - {total_loss:.2f} - acc - {test_accuracy.compute():.3f} - {total_acc:.3f} - {test_recall.compute():.3f} - {test_precision.compute():.3f} - iterations trained {iternum}')
            if best_loss is None or total_loss < best_loss:
                best_loss = total_loss
                best_idx = i + 1
            tb_writer.add_scalar('Loss/xent_test', total_loss, iternum)
            tb_writer.add_scalar('Acc/test', test_accuracy.compute(), iternum)
            model.eval()
            classifier.eval()
            to.save({'encoder': model.state_dict(),
                     'classifier': classifier.state_dict()}, f'{dir}/model_{i+1}.pt')
    tb_writer.flush()
    tb_writer.close()

    logger.info('Finished.')
    param_str = f'{epochs} {start_lr:.5f} {dropout:.3f} {lr_half_step} {w_decay:.10f}\t'
    open(f'{dir}/best_loss', 'w').write(param_str + str(best_loss))
    os.symlink(f'{dir}/model_{best_idx}.pt', f'{dir}/best_model.pt')


def main(name, epochs: ('', 'option', None, int) = 50,
         lr: ('', 'option', None, float) = 1e-2,
         bs: ('', 'option', None, int) = 128,
         w_decay: ('', 'option', None, float) = 1e-7,
         basemodel_lr: ('', 'option', None, float) = 1.0,
         encoder_f: ('', 'option', None, str) = '',
         dropout: ('', 'option', None, float) = 0.17,
         lr_half_step: ('', 'option', None, int) = 10,
         num_speakers: ('', 'option', None, int) = 0):
    to.manual_seed(0)
    np.random.seed(0)
    to.cuda.manual_seed(0)
    random.seed(0)
    # if os.path.exists(name):
    #     shutil.rmtree(name)
    # os.makedirs(name)
    tb_writer = SummaryWriter(name)
    logger.info(f'Tensorboard log in {name}')
    basic_trainer(name, tb_writer, epochs, lr, bs, w_decay, basemodel_lr, dropout, encoder_f, lr_half_step, num_speakers)


if __name__ == '__main__':
    import plac; plac.call(main)
