import os
import subprocess as sp
import math
from loguru import logger
import h5py as hdf
import numpy as np
import pickle
from glob import glob
import shutil

import kaldi_io


def getbname(fpath):
    return os.path.splitext(os.path.basename(fpath))[0]


MAPFILE = '/remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/conf/phones.60-48-39.map'


def process_timit_set(feats_scp, utt2spk_f, alid, odir, reduce: ('', 'flag', None) = False,
                      do_norm: ('', 'flag', None) = False,
                      use_pdf: ('', 'flag', None) = False):
    if os.path.exists(odir):
        shutil.rmtree(odir)
    os.makedirs(f'{odir}/tmp')

    utt2spk = {}
    for line in open(utt2spk_f):
        line_split = line.split()
        utt2spk[line_split[0]] = line_split[1]

    if use_pdf:
        cmd = f'ali-to-pdf {alid}/final.mdl "ark:gunzip -c {alid}/ali*gz |" ark,t:{odir}/ark.txt'
    else:
        cmd = f'ali-to-phones --per-frame=true {alid}/final.mdl "ark:gunzip -c {alid}/ali*gz |" ark,t:{odir}/ark.txt'
    sp.check_output(cmd, shell=True)

    phone_map = {}
    for line in open('/idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/lang/phones.txt'):
        p, i = line.split()
        i = int(i)
        phone_map[p] = i
        phone_map[i] = p

    map = {}
    for line in open(MAPFILE):
        line_split = line.split()
        if len(line_split) == 3:
            map[line_split[1]] = line_split[2]

    file_to_phns = {}
    new_phone_map = {}
    for line in open('/idiap/temp/rbraun/code/kaldi/egs/timit/s5b/data/lang/phones.txt'):
        w, i = line.split()
        i = int(i)
        new_phone_map[w] = i
        new_phone_map[i] = w
    num_frames_expected = 0
    labels_cnt = set()
    with open(f'{odir}/ark.txt', 'r') as fh:
        for line in fh:
            wavid, *ids = line.split()
            ids = [int(id) for id in ids]
            ps = []
            if reduce:
                for id in ids:
                    p = map.get(phone_map[id], phone_map[id])
                    if p not in new_phone_map:
                        new_phone_map[p] = len(new_phone_map)
                    id = new_phone_map[p]
                    labels_cnt.add(id)
                    ps.append(id)
            else:
                for id in ids:
                    labels_cnt.add(id)
                    ps.append(id)
                ps = ids
            if not use_pdf:
                ps = [p - 1 for p in ps]  # -1 because 0 is eps and therefore not here, and need to use 0 for pytorch.

            num_frames_expected += len(ps)
            file_to_phns[wavid] = ps

    cnt = len(labels_cnt)
    logger.info(f'There are {cnt} unique ids in this dataset.')
    with open(f'{odir}/phone_count', 'w') as fh:
        fh.write(f'{cnt}\n')

    logger.info(f'Num frames expected: {num_frames_expected}')

    spk_to_mean = {}
    spk_count = {}
    for uttid, feats in kaldi_io.read_mat_scp(f'scp:{feats_scp}'):
        spk = utt2spk[uttid]
        if spk not in spk_to_mean:
            spk_to_mean[spk] = np.zeros(feats.shape[-1])
            spk_count[spk] = 0
        spk_to_mean[spk] += feats.sum(axis=0)
        spk_count[spk] += feats.shape[0]
    for spk, mean in spk_to_mean.items():
        spk_to_mean[spk] /= spk_count[spk]

    feat_reader = kaldi_io.read_mat_scp(f'scp:{feats_scp}')
    hdf_handler = hdf.File(f'{odir}/data.hdf', 'w')

    hdf_feats_dataset = hdf_handler.create_dataset('feats', (num_frames_expected, 40,), maxshape=(num_frames_expected, 40,), dtype='f')
    lst = []
    feat_idx = 0
    not_done = True
    skipped, processed = 0, 0
    for wavid, feats in feat_reader:
        if do_norm:
            spk = utt2spk[wavid]
            feats -= spk_to_mean[spk]
        num_frames = feats.shape[0]
        try:
            lst_targs = file_to_phns[wavid]
        except KeyError:
            skipped += 1
            if skipped < 10:
                logger.warning(f'Missing targets for utterance {wavid}')
            continue
        assert len(lst_targs) == num_frames

        lst.append((feat_idx, lst_targs, wavid))

        hdf_feats_dataset[feat_idx: feat_idx + num_frames] = feats
        feat_idx += num_frames
        processed += 1
    logger.warning(f'Skipped {skipped}, processed {processed}')
    hdf_feats_dataset.resize((feat_idx, 40,))
    hdf_handler.close()
    with open(f'{odir}/targs.pkl', 'wb') as fh:
        pickle.dump(lst, fh, protocol=-1)
    logger.info('Finished.')

import plac; plac.call(process_timit_set)