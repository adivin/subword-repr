import os

import plac
import hyperopt
import subprocess as sp
import random
import string
from functools import partial
import math as m
from loguru import logger
import numpy as np
import shutil
from multiprocessing import Process


def sample_uniformlog(low, high):
    return m.exp(np.random.uniform(m.log(low), m.log(high)))


def sample_uniform(low, high):
    return np.random.uniform(low, high)


def run(epochs, lr, dropout, w_decay, lr_half_step, workdir=None):
    name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
    workname = f'{workdir}/timit-model-{name}'
    logger.info(f'{name} {epochs} {lr} {dropout} {lr_half_step} {w_decay}')
    os.makedirs(workname)
    cmd = f'utils/queue.pl -V -l q_short_gpu -P parole {workname}/train.log python -m timit.timit_train -epochs {epochs}' \
    f' -dropout {dropout} -lr {lr} -w-decay {w_decay} -lr-half-step {lr_half_step} {workname}'
    sp.Popen(cmd, shell=True)


def main(workdir):
    if os.path.exists(workdir):
        shutil.rmtree(workdir)
    func = partial(run, workdir=workdir)
    for i in range(100):
        search_space = {
                        'lr': sample_uniformlog(1e-2, 1e-1),
                        'epochs': int(sample_uniform(30, 100)),
                        'dropout': sample_uniform(0.12, 0.2),
                        'w_decay': sample_uniformlog(1e-10, 1e-6),
                        'lr_half_step': int(sample_uniform(15, 30))}
        func(**search_space)


plac.call(main)
