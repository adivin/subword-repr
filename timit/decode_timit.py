import pickle
import h5py as hdf
from model import Tdnnf, ConvGLU
from trainer import get_context_size
from timit.timit_train import PhoneClassifier, TwoLayerPhoneClassifier, load_encoder
import numpy as np
import torch as to
import math
import os
import subprocess as sp
from loguru import logger
import re
import torchaudio.functional as audiof
from misc.misc import parse_config


def run_network(model_f, feats_hdf, data_pkl, likes_f, dropout, use_pdf):
    all_feats = hdf.File(feats_hdf, 'r')['feats'][:]
    lst = pickle.load(open(data_pkl, 'rb'))

    logger.info(model_f)
    encoder = ConvGLU(80, 768, dropout, stride=3)

    onesided_context_size = get_context_size(encoder)
    suffix = '_normed_39'
    if use_pdf: suffix = '_normed_pdf'
    phone_count = int(open(f'timit_data{suffix}/train/phone_count').read())
    classifier = PhoneClassifier(768, phone_count)
    classifier.cpu()

    if use_pdf:
        counts = []
        sum = 0
        with open('/remote/idiap.svm/temp.speech01/rbraun/code/pytorch-kaldi/ali_train_pdf.counts') as fh:
            for line in fh:
                line_split = line.replace('[', '').replace(']','').split()
                for v in line_split[1:]:
                    v = float(v)
                    sum += v
                    counts.append(v)
        logpriors = np.log(np.array([v / sum for v in counts]))

    dct = to.load(model_f, map_location={'cuda:0': 'cpu'})
    classifier.load_state_dict(dct['classifier'])
    encoder.load_state_dict(dct['encoder'])
    encoder.eval()
    classifier.eval()
    logger.info('Running network.')
    fh = open(likes_f, 'w')
    np.set_printoptions(suppress=True)

    total_acc = 0.
    cnt = 0
    with to.no_grad():
        for i, entry in enumerate(lst):
            targs = entry[1]
            dur = len(targs)

            feat_idx = entry[0]
            feats = np.pad(all_feats[feat_idx: feat_idx + dur], ((onesided_context_size, onesided_context_size), (0, 0,)), mode='constant')
            feats = to.as_tensor(feats)
            feats = feats.unsqueeze(0)
            delta = audiof.compute_deltas(feats.transpose(1, 2)).transpose(1, 2)
            feats = to.cat((feats, delta,), dim=2)
            h = encoder(feats)
            # print(h[:5])
            out = to.log_softmax(classifier(h).squeeze(0), dim=-1).numpy()
            # print(out[20:25])
            # logger.info(f'{dur} {out.shape}')
            uttid = entry[2]
            if use_pdf:
                out -= logpriors
            preds = np.argmax(out, axis=1)
            targs = targs[:: 3]
            assert len(targs) == len(preds), f'{len(targs)} {len(preds)}'
            total_acc += np.sum(preds == targs)
            out = np.array2string(out, precision=3, threshold=9e20, max_line_width=10000000).replace('[', '').replace(']', '')
            fh.write(f'{uttid} [\n{out} ]\n')
            cnt += len(targs)
    total_acc /= cnt
    logger.info(f'Frame accuracy: {total_acc:.3f}')

    fh.close()


def run_decoder(likes_f, fst, symf, outf, acwt, use_pdf):
    logger.info('Generating lattices and taking best path.')
    if use_pdf:
        cmd = f'latgen-faster-mapped --allow-partial --acoustic-scale={acwt} --beam=13 --lattice-beam=6.0 $KEG/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali/final.mdl ' \
              f' {fst} ark,t:{likes_f} ark:- 2> /dev/null | lattice-best-path --acoustic-scale={acwt} ark:- ark,t:-     2> /dev/null | ' \
              f' utils/int2sym.pl -f 2- {symf} - > {outf}'
    else:
        cmd = f'latgen-faster --allow-partial --acoustic-scale={acwt} --beam=13 --lattice-beam=6.0 --determinize-lattice=false {fst} ark,t:{likes_f} ark:- 2> /dev/null |' \
              f' lattice-best-path --acoustic-scale={acwt} ark:- ark,t:- ark,t:{outf}.ali 2> /dev/null |' \
            f' utils/int2sym.pl -f 2- {symf} - > {outf}'
    sp.check_output(cmd, shell=True)
    logger.info('Done')
    # sp.check_output('cat /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test/text | $KEG/timit/s5/local/timit_norm_trans.pl -i - -m $KEG/timit/s5/conf/phones.60-48-39.map -from 48 -to 39 | sort > ref', shell=True)
    # sp.check_output(f'cat {outf} | $KEG/timit/s5/local/timit_norm_trans.pl -i - -m $KEG/timit/s5/conf/phones.60-48-39.map -from 48 -to 39 | sort > hyp', shell=True)
    cmd = f'compute-wer --text ark,t:/idiap/temp/rbraun/code/kaldi/egs/timit/s5b/data/test/text ark,t:{outf} 2> /dev/null'
    out = sp.check_output(cmd, shell=True).decode()
    p=re.search(r'WER \d+\.\d+', out)
    per = float(p.group()[4:])
    logger.info(f'PER: {per}')
    with open(os.path.join(os.path.dirname(outf), 'PER'), 'w') as fh:
        fh.write(str(per))


def main(model_f, feats_hdf, data_pkl, likes_f, fst, symf, outf,
         skip_nn: ('', 'flag', 's') = False,
         dropout: ('', 'option', None, float) = 0.,
         use_pdf: ('', 'flag', None) = False,
         acwt: ('', 'option', None, float) = 1.):
    logger.warning('Check if you should be normalising the output or not!')
    if not skip_nn:
        run_network(model_f, feats_hdf, data_pkl, likes_f, dropout, use_pdf)
    run_decoder(likes_f, fst, symf, outf, acwt, use_pdf)


import plac
plac.call(main)
