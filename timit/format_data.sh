#!/bin/bash -e

odir=timit_data_normed
odir_pdf=timit_data_normed_pdf

#python timit/format_timit.py -do-norm /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/train_fbank/feats.scp \
#  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/train_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali $odir/train
#
#python timit/format_timit.py -do-norm /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/dev_fbank/feats.scp \
#  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/dev_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali_dev $odir/dev
#
#python timit/format_timit.py -do-norm /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test_fbank/feats.scp \
#  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali_test $odir/test


#python timit/format_timit.py -do-norm -use-pdf /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/train_fbank/feats.scp \
#  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/train_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali $odir_pdf/train
#
#python timit/format_timit.py -do-norm -use-pdf /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/dev_fbank/feats.scp \
#  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/dev_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali_dev $odir_pdf/dev
#
#python timit/format_timit.py -do-norm -use-pdf /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test_fbank/feats.scp \
#  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali_test $odir_pdf/test

python timit/format_timit.py -do-norm -reduce /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/train_fbank/feats.scp \
  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/train_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali ${odir}_39/train

python timit/format_timit.py -do-norm -reduce /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/dev_fbank/feats.scp \
  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/dev_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali_dev ${odir}_39/dev

python timit/format_timit.py -do-norm -reduce /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test_fbank/feats.scp \
  /idiap/temp/rbraun/code/kaldi/egs/timit/s5/data/test_fbank/utt2spk /remote/idiap.svm/temp.speech01/rbraun/code/kaldi/egs/timit/s5/exp/dnn4_pretrain-dbn_dnn_ali_test ${odir}_39/test