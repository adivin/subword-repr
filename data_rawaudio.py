import concurrent.futures
import math as m
import os
import pickle
import random
from time import time
import subprocess as sp
from collections import defaultdict, Counter
from itertools import groupby

import matplotlib

matplotlib.use('Agg')

import h5pickle as hdf
import numpy as np
import plac
import soundfile as sf
import torch as to
from loguru import logger
from threading import Thread
from queue import Queue

from lhotse.augmentation import SoxEffectTransform
from torchaudio.transforms import MelScale, AmplitudeToDB

from format_data import WordInfo
from model import JitterLayer

NUM_BINS = 60
FRAME_LEN = 400
CHUNKLEN = 0.7
FEATURE_PADDING = 0.09
SR = 16000

# https://dsp.stackexchange.com/questions/26019/sinusoidal-liftering-in-implementations-of-mfcc
# https://stackoverflow.com/questions/60492462/mfcc-python-completely-different-result-from-librosa-vs-python-speech-features

class MelSpecOld(to.nn.Module):
    def __init__(self):
        super().__init__()
        self.melscale = MelScale(n_mels=NUM_BINS, sample_rate=SR, f_min=40, f_max=7600)
        self.window = to.nn.Parameter(to.hann_window(FRAME_LEN), requires_grad=False)
        self.pool = AmplitudeToDB(top_db=80)

    def forward(self, x):
        spec = to.stft(x, n_fft=FRAME_LEN, hop_length=160, window=self.window, center=False, onesided=True, return_complex=True)
        spec = spec.abs().pow(2.)
        spec = self.pool(spec + 1e-6)
        return self.melscale(spec).float()

class MelSpec(to.nn.Module):
    def __init__(self):
        super().__init__()
        # self.melscale = MelScale(n_mels=NUM_BINS, sample_rate=SR, f_min=40, f_max=7600)
        self.window = to.nn.Parameter(to.hann_window(FRAME_LEN), requires_grad=False)
        self.mat = to.nn.Parameter(to.zeros(201, 60), requires_grad=False)
        for i in range(60):
            if i < 25:
                self.mat[i*2: i*2 + 2, i] = 0.5
            elif i < 50:
                self.mat[50 + (i-25)*4: 50 + (i-25)*4 + 4, i] = 0.25
            else:
                self.mat[150+ (i-50)*5: 150+(i-50)*5 + 5, i] = 0.2
        self.mat[200, 59] = 0.2

    def forward(self, x):
        spec = to.stft(x, n_fft=FRAME_LEN, hop_length=160, window=self.window, center=False, onesided=True,     return_complex=True)
        spec = spec.abs().pow(2.)
        spec = to.log(spec + 1e-6)
        spec = spec.transpose(-2, -1)@self.mat
        return spec.transpose(-2, -1).float()


class WavLoader(Thread):
    def __init__(self, wavpaths):
        super().__init__()
        self.wavpaths = wavpaths
        self.queue = Queue(maxsize=1000)
        self.daemon = True
        self.start()

    def run(self):
        for wavpath in self.wavpaths:
            y, fs = sf.read(wavpath)
            assert fs == SR
            self.queue.put(y)
        self.queue.put(None)

    def next(self):
        next_item = self.queue.get()
        return next_item


def format_data(datadir, datadir_hires, langdir, alidir, outd, num_words_max: ('', 'positional', None, int), uttf: ('', 'option', 'u')=''):
    if not os.path.exists(f'{alidir}/ctm') or os.path.getsize(f'{alidir}/ctm') == 0:
        cmd = f'steps/get_train_ctm.sh --cmd "$train_cmd" {datadir} {langdir} {alidir}'
        sp.check_output(cmd, shell=True)
    else:
        logger.info('ctm file already exists.')
    os.makedirs(outd, exist_ok=True)

    maxdur = CHUNKLEN - FEATURE_PADDING*2
    logger.info(f'Excluding words that are longer than {maxdur} seconds.')

    utt2stime = {}
    wavid_to_uids = defaultdict(list)
    uid_to_wavid = {}
    with open(f'{datadir_hires}/segments') as fh:
        for line in fh:
            uttid, wavid, stime, _ = line.split()
            utt2stime[uttid] = float(stime)
            uid_to_wavid[uttid] = wavid
            wavid_to_uids[wavid].append(uttid)

    wavid_to_path = {}
    with open(f'{datadir_hires}/wav.scp') as fh:
        for line in fh:
            wavid, *line_entries = line.split()
            path = None
            for entry in line_entries:
                if entry.endswith('.wav') or entry.endswith('.mp3') or entry.endswith('.flac') or entry.endswith('.sph'):
                    path = entry
            assert path
            wavid_to_path[wavid] = path

    utts = set()
    if uttf:
        with open(uttf) as fh:
            for line in fh:
                utts.add(line.strip())

    wdct = defaultdict(int)
    all_words = []
    with open(f'{alidir}/ctm') as fh:
        for line in fh:
            uttid, _, stime, dur, w = line.split()  # s[tart] time, is based on audio file not utterance
            if uttf:
                if uttid not in utts:
                    continue
            dur = float(dur)
            w = w.lower()
            if w == 'i':
                w = 'I'
            wdct[w] += 1
            stime = float(stime) - utt2stime[uttid]
            wi = WordInfo(w, stime, dur, uttid)
            all_words.append(wi)

    utt2spk = {}
    spk_set = set()
    with open(f'{datadir_hires}/utt2spk') as fh:
        for line in fh:
            uttid, spk = line.split()
            utt2spk[uttid] = spk
            spk_set.add(spk)

    with open(f'{datadir_hires}/utt2dur') as fh:
        for line in fh:
            uttid, dur = line.split()

    spk_map = {}
    speakers = set()
    with open(f'{outd}/spk_map', 'w') as fh:
        for i, spk in enumerate(spk_set):
            spk_map[spk] = i
            spk_map[i] = spk
            speakers.add(i)
            fh.write(f'{spk} {i}\n')
    for uttid in utt2spk.keys():
        spk = utt2spk[uttid]
        utt2spk[uttid] = spk_map[spk]

    top_words = sorted(wdct.items(), key=lambda x: x[1], reverse=True)

    ood_test_set_word_count = 15000
    filter_list = ['us', 'ye']
    logger.info(f'Filtering {filter_list}')
    cnt = 0
    test_words = []
    ood_test_word_cnt = 0
    with open(f'{outd}/words_list', 'w') as fh:
        for w, c in top_words:
            if w == '<unk>' or w == '<UNK>' or w in filter_list:
                continue
            if cnt >= num_words_max + 200:
                break
            fh.write(f'{w}\n')
            if cnt > num_words_max:
                ood_test_word_cnt += wdct[w]
                test_words.append(w)
            cnt += 1
    logger.info(f'The number of candidate oov test words is {len(test_words)}')

    lexf = f'{outd}/lex'
    if not os.path.exists(lexf):
        sp.check_output(f'LC_ALL=en_GB.UTF-8 python $CODE/condutor/prons.py -s {outd}/words_list {lexf}', shell=True)
    # Only going to use those words so that a) each word only has a phone once in its pronunciation and
    # b) no overlap in pronunciations between orders while disregarding phone order and c) at least two phones
    phoneskey_to_words = defaultdict(list)
    lexicon = {}
    with open(lexf) as fh:
        for line in fh:
            word, *phones = line.split()
            if '<' in word:
                continue
            if len(phones) == 2:
                continue
            lexicon[word] = phones
            counter = Counter(phones)
            to_skip = any(count > 1 for count in counter.values())
            if to_skip:
                continue

            phoneskey = ' '.join(phones)
            #phoneskey = ' '.join(sorted(phones))
            phoneskey_to_words[phoneskey].append(word)
    logger.warning('Words with different order are considered now!')

    word_list = []
    with open(f'{outd}/lex_ortho', 'w') as fh:
        for phoneskey, words in phoneskey_to_words.items():
            # selecting word with highest count
            idx = max([(i, wdct[words[i]],) for i in range(len(words))], key=lambda x: x[1])[0]
            word = words[idx]
            phones = ' '.join(lexicon[word])
            fh.write(f'{word} {phones}\n')
            if word not in test_words:
                word_list.append(word)
    logger.info(f'Using {len(word_list)} words.')

    words_to_phones = {v: k for k, vs in phoneskey_to_words.items() for v in vs}
    test_words = [test_word for test_word in test_words if test_word in words_to_phones]
    logger.info(f'Using {len(test_words)} ood test words.')

    cnt = 0
    word_mapping = {}
    with open(f'{outd}/words_mapping', 'w') as fh:
        for w in word_list + test_words:
            word_mapping[w] = cnt
            word_mapping[cnt] = w
            fh.write(f'{w} {cnt}\n')
            cnt += 1
    top_words = set(word_list)

    random.shuffle(all_words)

    words = []
    word_to_cnt = defaultdict(int)
    test_words_set = set(test_words)
    lst_test_words = []
    for wi in all_words:
        if maxdur > wi.dur >= 0.1:
            if wi.word in top_words:
                word_to_cnt[wi.word] += 1
                words.append(wi)
            elif wi.word in test_words_set:
                lst_test_words.append(wi)

    logger.info(f'Number of ood test word samples is set to {ood_test_set_word_count} out of {len(lst_test_words)}')
    lst_test_words = random.sample(lst_test_words, ood_test_set_word_count)
    counter = defaultdict(int)
    for i, wi in enumerate(lst_test_words):
        counter[wi.word] += 1
    to_remove = set()
    for w, cnt in counter.items():
        if cnt == 1:
            to_remove.add(w)
    lst_test_words = [wi for wi in lst_test_words if wi.word not in to_remove]
    del all_words

    word_to_cnt_sorted = sorted(word_to_cnt.items(), key=lambda x: x[1])
    logger.info(f'When using top {num_words_max}, top word count is {word_to_cnt_sorted[-1][1]}, lowest is {word_to_cnt_sorted[0][1]} !')

    # Subsampling
    smoothing_exp = 0.75
    word_to_cnt_gotten = defaultdict(int)
    all_words = []
    for wi in words:
        cnt_to_get = word_to_cnt[wi.word] ** smoothing_exp + 250
        cnt_to_get = min(cnt_to_get, word_to_cnt[wi.word])
        cnt_gotten = word_to_cnt_gotten[wi.word]
        if cnt_gotten < cnt_to_get:
            all_words.append(wi)
            word_to_cnt_gotten[wi.word] += 1
    del words

    uid_to_words = defaultdict(list)
    words_by_wav = defaultdict(list)
    spk_to_dur = defaultdict(float)
    num_words = 0
    audio_duration = 0.
    joined_list = all_words + lst_test_words
    wav_ids_used_set = set()
    word_cnts_train = defaultdict(int)
    for wi in joined_list:
        word_str = wi.word
        wi.word = word_mapping[word_str]
        uid = wi.uid
        uid_to_words[uid].append(wi)
        spk = utt2spk[uid]
        if word_str not in test_words_set:
            num_words += 1
            audio_duration += wi.dur
            spk_to_dur[spk_map[spk]] += wi.dur
            word_cnts_train[word_str] += 1
        wavid = uid_to_wavid[uid]
        wav_ids_used_set.add(wavid)
        words_by_wav[wavid].append(wi)

    tpl = sorted(word_cnts_train.items(), key=lambda x: x[1])[0]
    logger.info(f'Lowest word + count {tpl}')

    for uid, lst in uid_to_words.items():
        lst.sort(key=lambda x: x.stime)

    target_test_dur = 3. * 3600
    current_test_dur = 0.
    speakers = list(speakers)
    random.shuffle(speakers)
    test_speakers = set()
    for speaker in speakers:
        dur = spk_to_dur[spk_map[speaker]]
        if dur == 0.:
            continue
        current_test_dur += dur
        test_speakers.add(speaker)
        if current_test_dur > target_test_dur:
            break
    logger.info(f'Test set has {len(test_speakers)} speakers.')
    lst_words_test = []

    num_utts = len(uid_to_words)
    logger.info(f'Matching features with {num_words} words from {num_utts} utterances, audio duration is {audio_duration/3600.:.2f} hours.')
    melspec = MelSpec()
    expected_size = int(sum(CHUNKLEN for wi in all_words) * SR)
    f = hdf.File(f'{outd}/960_feats.hdf', 'w')
    d = f.create_dataset('raw', (expected_size,), maxshape=(expected_size,), dtype='f')
    d_test = f.create_dataset('test_raw', (3*current_test_dur*SR,), maxshape=(3*current_test_dur*SR,), dtype='f')
    spk_mean_f = f'{outd}/spk_stats.pkl'
    spk_to_dur_f = f'{outd}/spk_durs.pkl'
    spk_mean = {}
    spk_count = {}
    lst_words = []
    feat_idx = 0
    test_feat_idx = 0
    ood_test_joined = np.empty(0, dtype=np.float32)
    lst_ood_test = []
    skipped = 0
    utts_processed = 0
    wavid_to_path_items = list(wavid_to_path.items())
    wavid_to_path_items = [v for v in wavid_to_path_items if v[0] in wav_ids_used_set]
    random.shuffle(wavid_to_path_items)
    wavloader = WavLoader((e[1] for e in wavid_to_path_items))
    for wavid, wavpath in wavid_to_path_items:
        y = wavloader.next()
        assert len(y.shape) == 1
        uttids = wavid_to_uids[wavid]

        # Speaker stuff
        spk = utt2spk[uttids[0]]
        if spk not in spk_mean:
            spk_mean[spk] = to.zeros(NUM_BINS)
            spk_count[spk] = 0
        y_tensor = to.tensor(y, dtype=to.float32)
        m = melspec(y_tensor).T
        spk_mean[spk] += m.sum(axis=0)
        spk_count[spk] += m.shape[0]

        # Feature stuff
        for uttid in uttids:

            if uttid not in uid_to_words:
                continue

            speaker = utt2spk[uttid]
            is_test = False
            if speaker in test_speakers:
                is_test = True
            utts_processed += 1

            words_for_uid = uid_to_words[uttid]

            loop_feat_idx = 0
            joined_feat = None
            last_endidx = -1e9
            for utt_word_pos, wi in enumerate(words_for_uid):
                w, stime, dur = wi.word, wi.stime, wi.dur

                idx = int(round(stime * SR - FEATURE_PADDING * SR))
                durlen = int(round(dur * SR))
                assert durlen < (CHUNKLEN - 2*FEATURE_PADDING) * SR
                endidx = int(round(idx + CHUNKLEN * SR))

                if y.size < idx + durlen:
                    #logger.info(f'{uttid} {w} {stime} {dur}')
                    logger.warning(f'Skipping word because of signal size {y.size} smaller than indices {idx+durlen}.')
                    skipped += 1
                    continue

                if word_mapping[w] in test_words_set:
                    wi.featidx = int(round(ood_test_joined.shape[0] + FEATURE_PADDING * SR))
                    ood_test_joined = np.concatenate((ood_test_joined, y[idx: endidx]), axis=0)
                    tpl = (wi.word, wi.dur, wi.featidx, wi.uid, utt2spk[wi.uid], utt_word_pos)
                    lst_ood_test.append(tpl)
                    continue

                assert endidx - idx == CHUNKLEN * SR, '%s %s' % (feat.shape[0], wi.dur)
                back_offset = 0
                if last_endidx > idx:
                    feat = y[last_endidx: endidx]
                    back_offset = last_endidx - idx
                    loop_feat_idx -= back_offset
                    # print('in ', loop_feat_idx)
                else:
                    if idx < 0:
                        feat = y[0: endidx]
                    else:
                        feat = y[idx: endidx]
                #print(idx, endidx, loop_feat_idx, back_offset)

                # End/Begin padding
                if endidx >= y.shape[0]:
                    num_overhang = endidx - y.shape[0]
                    if last_endidx > y.shape[0]:
                        num_overhang = endidx - last_endidx
                    if feat.shape[0] != 0:
                        feat = np.pad(feat, (0, num_overhang,), mode='constant')
                    else:  # feat could be empty because last frames of utterance already used
                        feat = np.tile(joined_feat[-1], (num_overhang,))
                if idx < 0:
                    if back_offset == 0:
                        num_overhang = -idx
                        feat = np.pad(feat, (num_overhang, 0,), mode='constant')
                    # if back_offset != 0:
                    #     print(idx, endidx, last_endidx, dur)
                    #     raise RuntimeError("WTF?")

                assert feat.shape[0] + back_offset == CHUNKLEN * SR, f'{feat.shape[0]} {back_offset} {CHUNKLEN}'

                if not is_test:
                    wi.featidx = int(round(feat_idx + loop_feat_idx + FEATURE_PADDING * SR))
                    tpl = (wi.word, wi.dur, wi.featidx, wi.uid, utt2spk[wi.uid], utt_word_pos)
                    lst_words.append(tpl)
                else:
                    wi.featidx = test_feat_idx + loop_feat_idx + FEATURE_PADDING * SR
                    tpl = (wi.word, wi.dur, wi.featidx, wi.uid, utt2spk[wi.uid], utt_word_pos)
                    lst_words_test.append(tpl)

                if joined_feat is None:
                    joined_feat = feat
                else:
                    joined_feat = np.concatenate((joined_feat, feat), axis=0)
                loop_feat_idx = joined_feat.shape[0]
                last_endidx = endidx
                # Ending per word loop
            #logger.info(f'{test_feat_idx} {loop_feat_idx} {joined_feat.shape}')
            # Per utterance loop
            if not is_test:
                d[feat_idx: feat_idx + loop_feat_idx] = joined_feat
                feat_idx += loop_feat_idx
            else:
                d_test[test_feat_idx: test_feat_idx + loop_feat_idx] = joined_feat
                test_feat_idx += loop_feat_idx

            if utts_processed % 50000 == 0:
                logger.info('Processed 50000 utts.')

    d_test.resize((test_feat_idx,))
    d.resize((feat_idx,))
    logger.info(f'OOD test set size is {len(lst_ood_test)}')
    f.create_dataset('ood_test', data=ood_test_joined, shape=ood_test_joined.shape, dtype='f')

    f.close()
    for spk, mean in spk_mean.items():
        spk_mean[spk] /= spk_count[spk]

    with open(f'{outd}/train_words', 'w') as fh:
        for w in top_words:
            fh.write(f'{w}\n')
    logger.info('Finished, creating pickled files.')
    with open(spk_mean_f, 'wb') as fh:
        pickle.dump(spk_mean, fh, protocol=pickle.HIGHEST_PROTOCOL)
    with open(spk_to_dur_f, 'wb') as fh:
        pickle.dump(spk_to_dur, fh, protocol=pickle.HIGHEST_PROTOCOL)

    pickle.dump(lst_ood_test, open(f'{outd}/word_ood_test.pkl', 'wb'))
    with open(f'{outd}/word_data_train.pkl', 'wb') as fh:
        pickle.dump(lst_words, fh)
    with open(f'{outd}/word_data_test.pkl', 'wb') as fh:
        pickle.dump(lst_words_test, fh)


class Noises:
    def __init__(self):
        self.chunk_len = int((CHUNKLEN + 0.02) * SR)
        self.wavpaths = []
        for line in open('/idiap/temp/rbraun/data/noise/wav_list'):
            self.wavpaths.append(line.strip())
        self.wavpaths = random.sample(self.wavpaths, 500)
        self.noises = []
        for wavpath in self.wavpaths:
            x, fs = sf.read(wavpath, always_2d=True)
            assert fs == SR
            assert x.shape[1] == 1
            x = to.as_tensor(x).float().squeeze(1)
            if x.size(0) < self.chunk_len + 4000:
                x = to.nn.functional.pad(x, (0, self.chunk_len - x.size(0),))
                self.noises.append(x)
            else:
                self.noises.append(x[:self.chunk_len])
                offset = random.randint(4000, x.size(0) - self.chunk_len)
                self.noises.append(x[offset: offset + self.chunk_len])
        self.noises = to.stack(self.noises)
        if to.cuda.is_available():
            self.noises = self.noises.cuda()

    @to.no_grad()
    def __call__(self, x, snr_db):
        bs = x.size(0)
        x_len = x.size(1)
        assert x_len <= self.chunk_len, f'{x_len} {self.chunk_len}'
        x_powers = (x*x).sum(dim=1) / x_len
        #logger.warning('fucking things up')
        indcs = to.randint(self.noises.size(0), (bs,))
        noises = self.noises[indcs]
        noises = noises[:, :x_len]
        noise_powers = (noises*noises).sum(dim=1)
        alpha = (10 ** (-snr_db / 10) * x_powers / noise_powers) ** 0.5
        #indcs = to.randint(bs, (int(bs*0.1),))
        # alpha[indcs] *= 0.1
        #x[1] = x[0]
        #alpha.fill_(0.)
        #alpha[1] = 0.01
        noises *= alpha.unsqueeze(1)
        x.add_(noises)


class RawWordDataholder:
    def __init__(self, fpath_words, fpath_feats, fpath_words_test, fpath_words_ood_test, word_mapping, batch_size, subsampling_factor, onesided_context,
                 small_set=False, smooth=False, smooth_exp=1., use_cuda=True, shuffle_grouped=False, pitch_aug=False,
                 add_aug=False, aug_snr=None, num_negs=-1):
        logger.info(f'Loading data, batch size {batch_size} and using fss {subsampling_factor}.')
        self.shuffle_grouped = shuffle_grouped
        self.word_mapping = word_mapping
        self.smooth_exp = smooth_exp
        self.use_cuda = use_cuda
        self.smooth = smooth
        self.pitch_aug = pitch_aug
        assert aug_snr is not None
        self.aug_snr = aug_snr
        self.num_negs = num_negs
        assert FEATURE_PADDING > (onesided_context+3) / 100  # +3 because of random offset (1) and extra frames for feat extraction (2)
        self.subsampling_factor = subsampling_factor
        self.onesided_context = onesided_context

        with open(fpath_words, 'rb') as fh:
            self.lst_wordinfo = pickle.load(fh)
            if small_set:
                self.lst_wordinfo = self.lst_wordinfo[:250000]

        with open(fpath_words_test, 'rb') as fh:
            self.lst_wordinfo_test = pickle.load(fh)

        self.lst_wordinfo_ood_test = pickle.load(open(fpath_words_ood_test, 'rb'))

        num_words = len(self.lst_wordinfo)
        logger.info(f'Num words {num_words}')
        self.fh = hdf.File(fpath_feats, 'r')
        self.fpath_feats = fpath_feats
        if not small_set:
            self.feats_dataset = self.fh['raw'][:]
        else:
            self.feats_dataset = self.fh['raw'][:500_000_000]
        self.feats_dataset_test = self.fh['test_raw'][:]
        self.feats_dataset_ood_test = self.fh['ood_test'][:]
        # fh.close()
        len_feats = self.feats_dataset.shape[0]
        logger.info(f'Number of train samples {len_feats} test samples {self.feats_dataset_test.shape[0]}')
        # assert num_chunks == len_feats // self.chunk_size

        def parse_wordinfo(lst_wordinfo):
            lst_indcs = []
            word_to_indcs = defaultdict(list)
            word_to_cnt = defaultdict(int)
            idx_to_utt = {}
            total_cnt = 0
            total_dur = 0.
            spk_set = set()
            for i, tpl in enumerate(lst_wordinfo):
                wid, dur, featidx, uttid, spkid, word_pos = tpl
                if small_set and featidx + int(CHUNKLEN * SR) > 500_000_000:
                   continue
                #if dur < 0.33 or dur > 0.35:
                #    continue
                total_dur += dur
                dur = int(round(dur*SR))
                featidx = int(round(featidx))
                spk_set.add(spkid)
                word_to_cnt[wid] += 1
                word_to_indcs[wid].append(total_cnt)
                lst_indcs.append((wid, total_cnt, featidx, dur, spkid, word_pos))  # (wid, word index, start feature frame index, input frame count)
                idx_to_utt[total_cnt] = uttid
                total_cnt += 1
            num_speakers = len(spk_set)
            total_dur /= 3600.
            logger.info(f'Done parsing data, total audio duration is {total_dur:.2f} - num speakers {num_speakers}')
            return lst_indcs, word_to_indcs, word_to_cnt, idx_to_utt

        self.lst_indcs, self.word_to_indcs, self.word_to_cnt, self.idx_to_utt = parse_wordinfo(self.lst_wordinfo)
        self.widx_to_gotten_idx = {wid: 0 for wid in self.word_to_indcs.keys()}
        self.train_speakers = set()
        for tpl in self.lst_indcs:
            self.train_speakers.add(tpl[4])

        self.min_word_cnt = min(self.word_to_cnt.values())
        max_word_cnt = max(self.word_to_cnt.values())
        total_cnt = sum(self.word_to_cnt.values())
        self.word_to_prob = {w: c/total_cnt for w, c in self.word_to_cnt.items()}
        self.min_word_prob = min(self.word_to_prob.values())
        logger.info(f'Largest word cnt {max_word_cnt}, smallest {self.min_word_cnt} - Total number of words {len(self.lst_indcs)} - Unique words {len(self.word_to_cnt)}')

        self.batch_size = batch_size

        logger.info('Parsing test data.')
        self.test_lst_indcs, self.test_word_to_indcs, self.test_word_to_cnt, self.test_idx_to_utt = parse_wordinfo(self.lst_wordinfo_test)
        self.test_size = len(self.test_lst_indcs)
        self.ood_test_lst_indcs, self.ood_test_word_to_indcs, self.ood_test_word_to_cnt, self.ood_test_idx_to_utt = parse_wordinfo(self.lst_wordinfo_ood_test)

        logger.info(f'Test set size is {self.test_size} (word count)')

        self.class_weights = [1. for _ in range(len(self.word_to_cnt.keys()))]
        self.class_weights = to.tensor(self.class_weights)
        self.num_iterations = len(self) // self.batch_size
        self.printed = False
        self.shuffle()
        self.k = 0

        self.word_to_invprob = {w: 1. / prob for w, prob in self.word_to_prob.items()}
        probsum = sum(self.word_to_invprob.values())
        self.word_to_invprob = {w: prob / probsum for w, prob in self.word_to_invprob.items()}

        self.tpool = concurrent.futures.ThreadPoolExecutor(2)
        self.melspec = MelSpec()
        if self.use_cuda:
            # s = to.cuda.memory_summary()
            # logger.info(s)
            self.melspec = self.melspec.cuda()

        self.augmenter = SoxEffectTransform(effects=[
            ['speed', np.random.uniform(low=0.9, high=1.1)],
            ['rate', SR]
        ])

        self.add_aug = add_aug
        if add_aug:
            self.noises = Noises()

        dir = os.path.dirname(fpath_feats)
        spk_mean_f = f'{dir}/spk_stats.pkl'
        with open(spk_mean_f, 'rb') as fh:
            self.spk_means = pickle.load(fh)
        self.mean = to.stack([v for k, v in self.spk_means.items() if k in self.train_speakers]).sum(dim=0) / len(self.train_speakers)
        if self.use_cuda:
            self.mean = self.mean.cuda()
        self.onesided_context_s = self.onesided_context / 100.
        self.batch_dur = int( (CHUNKLEN - 2 * FEATURE_PADDING + 2 * self.onesided_context_s + 0.02) * SR)

        self.f_batch = to.zeros((self.batch_size, self.batch_dur,), dtype=to.float32)
        self.train_bs = batch_size

    def __len__(self):
        return len(self.lst_indcs)

    def shuffle(self):
        for w, indcs in self.word_to_indcs.items():
            perm = np.random.permutation(len(indcs))
            self.word_to_indcs[w] = np.asarray(indcs)[perm]

    def reset_gotten_indcs(self):
        for k in self.widx_to_gotten_idx.keys():
            self.widx_to_gotten_idx[k] = 0

    def get_simple(self, k, lst_indcs, feats_dataset, is_train=True, return_utts=False):
        bs = self.batch_size
        if k*bs + bs > len(lst_indcs): return None
        device = to.device('cpu')
        if to.cuda.is_available() and self.use_cuda:
            device = to.device('cuda')
        indcs = lst_indcs[k*bs: k*bs + bs]
        indcs = sorted(indcs, key=lambda x: x[3])
        self.current_indcs = indcs
        onesided_context = int(round(self.onesided_context / 100. * SR))
        # batch_dur = int(CHUNKLEN*SR)#indcs[-1][3] + 2 * onesided_context
        # extra_context = int(0.02 * SR)
        t = np.zeros((bs,), np.long)
        spks = np.zeros((bs,), np.long)
        durs = []
        utts = []
        means = to.zeros((bs, NUM_BINS), dtype=to.float32)
        # Need to add +2 context frames because of OTF feature extraction
        # which chops off frames.
        offsets = np.random.randint(-1, 2, (bs,))  # introduce some randomness
        for j, (wid, w_idx, feat_idx, dur, spkid, word_pos) in enumerate(indcs):
            self.f_batch[j] = to.from_numpy(feats_dataset[feat_idx-onesided_context+offsets[j]: feat_idx - onesided_context + self.batch_dur + offsets[j]])
            t[j] = wid
            spks[j] = spkid
            means[j] = self.spk_means[spkid]
            if len(durs) > 0:
                assert dur >= durs[-1]
            durs.append(int(dur / 160))
            if return_utts:
                if is_train:
                    utts.append(self.idx_to_utt[w_idx])
                else:
                    utts.append(self.test_idx_to_utt[w_idx])

        t_uniq = np.unique(t)
        neg_indcs_by_t = np.zeros((bs, self.num_negs), dtype=np.int64)
        for i, target in enumerate(t_uniq):
            mask = target != t
            indcs = np.where(mask)[0]
            t_indcs = np.where(~mask)[0]
            for idx in t_indcs:
                neg_indcs_by_t[idx] = np.random.choice(indcs, (self.num_negs,), replace=False)
        neg_indcs_by_t = to.from_numpy(neg_indcs_by_t).to(device)

        if is_train and self.pitch_aug:
            assert False
            self.f_batch = self.augmenter(self.f_batch, SR)
        f_batch = self.f_batch.to(device)
        means = means.to(device)
        f_batch_orig = f_batch.clone()
        if is_train and self.add_aug:
            self.noises(f_batch, self.aug_snr)

        f_batch = self.melspec(f_batch)
        f_batch -= means.unsqueeze(2)

        f_batch_orig = self.melspec(f_batch_orig)
        f_batch_orig -= means.unsqueeze(2)

        durs = to.tensor([m.ceil(dur/3) for dur in durs], device=device)
        if not return_utts:
            return f_batch, f_batch_orig, to.from_numpy(t).to(device), neg_indcs_by_t, durs, to.as_tensor(spks).to(device)
        else:
            return f_batch, f_batch_orig, to.from_numpy(t).to(device), neg_indcs_by_t, durs, to.as_tensor(spks).to(device), utts

    def subsample(self, lst_indcs):
        word_to_get = {}
        for w, cnt in self.word_to_cnt.items():
            new_cnt = int(cnt**self.smooth_exp + 1000)
            if cnt < new_cnt:
                new_cnt = cnt
            word_to_get[w] = new_cnt
        do_shuffle = False
        new = []
        for wid, idxs in self.word_to_indcs.items():
            cnt_to_get = word_to_get[wid]
            while cnt_to_get % 4 != 0: cnt_to_get -= 1
            start_idx = self.widx_to_gotten_idx[wid]
            end_idx = start_idx + cnt_to_get
            lst = idxs[start_idx: end_idx]
            if end_idx >= len(idxs):
                end_idx = end_idx - len(idxs)
                lst = np.concatenate((lst, idxs[0: end_idx],))
                if wid == 0: do_shuffle = True
            for idx in lst:
                new.append(lst_indcs[idx])
            self.widx_to_gotten_idx[wid] = end_idx
        if not self.shuffle_grouped:
            random.shuffle(new)
        else:
            new = group_n_shuffle(new)
        return new, do_shuffle

    def iter_train_simple(self, return_utts=False):
        self.change_batchsize(self.train_bs)
        if self.smooth:
            lst_indcs, do_shuffle = self.subsample(self.lst_indcs)
            if do_shuffle:
                self.shuffle()
                logger.info('Shuffling data.')
        else:
            random.shuffle(self.lst_indcs)
            lst_indcs = self.lst_indcs
        num_iters = len(lst_indcs) // self.batch_size
        if not self.printed:
            logger.info(f'Num samples in epoch {len(lst_indcs)}')
            self.printed = True
        res = self.get_simple(self.k, lst_indcs, self.feats_dataset, True, return_utts)
        while self.k < num_iters-1:
            self.k += 1
            future = self.tpool.submit(self.get_simple, self.k, lst_indcs, self.feats_dataset, True, return_utts)
            if not return_utts:
                yield res
            else:
                yield res
            res = future.result()
        self.k = 0

    def iter_test_simple(self, return_utts=False):
        self.change_batchsize(256)
        num_iters = len(self.test_lst_indcs) // self.batch_size
        self.test_lst_indcs = sorted(self.test_lst_indcs, key=lambda x: x[0])
        self.test_lst_indcs = group_n_shuffle(self.test_lst_indcs, size=2)
        res = self.get_simple(0, self.test_lst_indcs, self.feats_dataset_test, False, return_utts)
        for k in range(1, num_iters-1):
            future = self.tpool.submit(self.get_simple, k, self.test_lst_indcs, self.feats_dataset_test, False, return_utts)
            if not return_utts:
                yield res
            else:
                yield res
            res = future.result()

    def iter_ood_test(self, return_utts=False):
        self.change_batchsize(256)
        self.ood_test_lst_indcs = sorted(self.ood_test_lst_indcs, key=lambda x: x[0])
        self.ood_test_lst_indcs = group_n_shuffle(self.ood_test_lst_indcs, size=2)
        num_iters = len(self.ood_test_lst_indcs) // self.batch_size
        for k in range(num_iters-1):
            res = self.get_simple(k, self.ood_test_lst_indcs, self.feats_dataset_ood_test, False, return_utts)
            if not return_utts:
                yield res
            else:
                yield res

    def get_state(self):
        dct = {'k': self.k, 'word_to_indcs': self.word_to_indcs, 'widx_to_gotten_idx': self.widx_to_gotten_idx}
        return dct

    def set_state(self, dct):
        self.k = dct['k']
        self.word_to_indcs = dct['word_to_indcs']
        self.widx_to_gotten_idx = dct['widx_to_gotten_idx']
        self.widx_to_gotten_idx = dct['widx_to_gotten_idx']

    def change_batchsize(self, size):
        self.batch_size = size
        self.f_batch = to.zeros((self.batch_size, self.batch_dur,), dtype=to.float32)


def group_n_shuffle(lst, size=4):
    gen = MyGen(size)
    groups = [list(g) for _, g in groupby(lst, gen)]
    random.shuffle(groups)
    new_lst = [v for group in groups for v in group]
    return new_lst

class MyGen:
    def __init__(self, size=4):
        self.i = -1
        self.size = size

    def __call__(self, x):
        self.i += 1
        return self.i // self.size


if __name__ == '__main__':
    plac.call(format_data)
