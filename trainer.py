import math
import wandb
from glob import glob

import numpy as np
import torch as to
import torch.distributions
from loguru import logger

from pytorch_lightning.metrics.classification import Recall, Precision, Accuracy


def lr_scheduler(optim, eta, iter, total_iter, total_decrease=0.1):
    # warm_length = 500
    # if iter < warm_length:
    #     eta = iter * eta / (warm_length*10)
    # else:
    #     iter -= warm_length
    #     total_iter -= warm_length
    final_eta = eta * total_decrease
    eta = iter * (final_eta - eta) / total_iter + eta
    for param_group in optim.param_groups:
        param_group['lr'] = eta
    return eta


# def do_warmup(optim, eta, current_iter, warmup_iter=100, total_decrease=0.001):
#     eta * (current_iter / warmup_iter)


def change_lr_to(new_lr, *optims):
    for optim in optims:
        for param_group in optim.param_groups:
            print(f'old lr {param_group["lr"]}')
            param_group['lr'] = new_lr


def change_lr(lr, factor, *optims):
    new_lr = lr * factor
    for optim in optims:
        for param_group in optim.param_groups:
            param_group['lr'] = new_lr
    return new_lr


def do_warmup(lr, iter, warmup_iterations, *optims):
    if iter >= warmup_iterations:
        return lr
    new_lr = iter * (lr / warmup_iterations)
    change_lr_to(new_lr, *optims)
    return new_lr


def report_grad_norms(model_d, *models, iter_num, param_norms):
    fname = f'{model_d}/gradnorms'
    f = open(fname, 'a')
    print(f'{iter_num}', file=f)
    for model in models:
        modelname = model.__class__.__name__
        print(f'{modelname}', file=f)
        for name, param in model.named_parameters():
            if 'bias' in name or 'batchnorm' in name:
                continue
            if param.grad is None:
                print(f'{name} HAS NO GRAD!', file=f)
                continue
            l2norm = param.grad.norm()
            wnorm = param.data.norm()
            print(f'{name} {wnorm} {l2norm}', file=f, end='')
            if param_norms is not None:
                if modelname not in param_norms:
                    param_norms[modelname] = {}
                if name not in param_norms[modelname]:
                    param_norms[modelname][name] = param.data.clone()
                else:
                    diff = to.norm(param.data - param_norms[modelname][name], p=1)
                    print(f' {diff}', file=f, end='')
                    param_norms[modelname][name] = param.data.clone()
            print('', file=f)
    print('\n', file=f)
    f.close()


def report_grad_signs_evolution(model_d, models, last_signs=None):
    fname = f'{model_d}/gradsigns'
    f = open(fname, 'a')
    i = 0
    newlst = []
    print('\n', file=f)
    for model in models:
        print(f'{model.__class__.__name__}', file=f)
        for name, param in model.named_parameters():
            # logger.info(f'{name}')
            if 'bias' in name or 'batchnorm' in name:
                continue
            sign = param.grad.sign()
            if last_signs is not None:
                corr = torch.sum(last_signs[i] * sign) / (sign.numel()*sign.numel())
                print(f'{name} {corr:.3f}', file=f)
                i += 1
            newlst.append(sign)
    print('\n', file=f)
    f.close()
    return newlst


@to.no_grad()
def splice_frames(x):
    """ x has shape (N, C, T) """
    return to.cat((x[:, :, :-6], x[:, :, 3:-3], x[:, :, 6:]), dim=1)

@to.no_grad()
def spectrogram_as_target(x, onesided_context):
    """ x has shape (N, C, T) """
    assert onesided_context > 3
    endidx = -(onesided_context - 3)
    if endidx == -0: endidx = None
    x_frame = x[:, :, onesided_context - 3: endidx]
    x_frame /= x_frame.norm(dim=1).unsqueeze(1)
    x_frame = splice_frames(x_frame)
    return x_frame


@to.no_grad()
def mask_spectrogram(x, onesided_context):
    N = x.size(0)
    T = x.size(1)
    mask_indcs = to.randint(onesided_context, T - onesided_context - 1, (N, int(T*0.05))).cuda()
    x.scatter_(1, mask_indcs.unsqueeze(2).expand(-1, -1, x.size(2)), x.mean(dim=[1,2]).view(N, 1, 1).expand(-1, T, x.size(2)))
    # logger.info(f'A {mask_indcs[:5]}')
    # mask_indcs = to.randperm(N*T)[:int(N*T*0.1)]
    # x.view(N*T, -1)[mask_indcs] = x.mean()
    return x, mask_indcs


def plot_input(f, oldf):
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(211)
    ax.pcolormesh(np.arange(0, f.shape[1]), np.arange(0, f.shape[2]), oldf.T)
    ax = fig.add_subplot(212)
    newf = f[0].cpu().numpy()
    ax.pcolormesh(np.arange(0, f.shape[1]), np.arange(0, f.shape[2]), newf.T)
    print(np.sum(np.abs((oldf - newf))))
    plt.show()


def load_models(idir, *models):
    epoch = 0
    for m in models:
        mnames = glob(f'{idir}/{m.__class__.__name__}_*')
        if len(mnames) == 0:
            logger.info(f'Not found model {m.__class__.__name__}')
            return epoch
        mname = sorted(mnames, key=lambda x: int(x.split('_')[-1]))[-1]
        epoch = mname.split('_')[-1]
        m.load_state_dict(to.load(mname, map_location=torch.device('cpu')))
        logger.info(mname)
    return epoch


def save_models(odir, iter, *models):
    for m in models:
        m.eval()
        to.save(m.state_dict(), f'{odir}/{m.__class__.__name__}_{iter}')


def get_context_size(model):
    model.eval()
    start_size = 40
    while True:
        test_input = to.randn(1, model.affine.weight.shape[1], start_size)
        try:
            output = model(test_input)
            # print(start_size, output[0].shape)
        except RuntimeError:
            break
        start_size -= 1
    start_size += 1
    context = (start_size - 1) / 2
    assert int(context) == math.ceil(context), f'{context} {start_size}'
    context = int(context)
    logger.info(f'Model onesided context is {context}')
    return context


def run_eval_wordchunks_constrast(model, word_worker, dataholder,
                                  word_loss, word_contr, train_args, word_cnt, collect_results=False, iter_num=None):
    model.eval()
    word_worker.eval()
    word_loss.eval()
    word_contr.eval()
    if collect_results:
        preds = []
        targs = []
    all_utts = []
    with to.no_grad():
        cnt = 0
        total_closs = 0.
        # total_xloss = 0.
        # recall = Recall(num_classes=word_cnt, average='macro')
        # precision = Precision(num_classes=word_cnt, average='macro')
        # accuracy = Accuracy()
        for f, f_orig, t, t_negs, durs, spks in dataholder.iter_test_simple():
            h_quant, h_sm = model(f_orig)

            closs = word_contr(h_quant, h_quant, t, t_negs, durs)

            total_closs += closs.item()
            cnt += 1

        total_closs /= cnt
        logger.info(f'Test closs {total_closs:.5f}')

        cnt = 0
        ood_total_closs = 0.
        for f, f_orig, t, t_negs, durs, spks in dataholder.iter_ood_test():
            h_quant, _ = model(f_orig)

            closs = word_contr(h_quant, h_quant, t, t_negs, durs)
            ood_total_closs += closs.item()
            cnt += 1
        ood_total_closs /= cnt
        logger.info(f'OOD Test closs {ood_total_closs:.5f}')
        if iter_num is not None:
            wandb.log({'iter': iter_num, 'test_closs': total_closs, 'ood_closs': ood_total_closs})

    if collect_results:
        preds = to.cat(preds)
        targs = to.cat(targs)
        return preds.cpu(), targs.cpu(), all_utts
    else:
        return total_closs


def grad_hook(grad):
    norm = grad.norm()
    logger.info(f'gnorm {norm}')
    return grad
