import time
import os
import sys
import pickle
import psutil
import numpy as np
import torch as to
if to.cuda.is_available(): to.cuda.current_device()
import torch.nn as nn
import torch.nn.functional as fu
from loguru import logger
from torch.utils.tensorboard import SummaryWriter
import random
from collections import defaultdict

from format_data import WordDataholder
from trainer import get_context_size, change_lr, spectogram_as_target, run_eval_wordchunks_constrast, report_grad_norms, change_lr_to
from trainer_short import load_checkpoint, save_checkpoint, model_init
from misc.misc import parse_config, read_word_mapping_f
from layers import grad_normaliser
from torchaudio.transforms import FrequencyMasking, TimeMasking
from pytorch_lightning.metrics.classification import Recall, Precision, Accuracy
from model import JitterLayer


def get_current_lr(optimizer):
    lr = None
    for param_group in optimizer.param_groups:
        lr = param_group['lr']
    return lr


def train_job(model_d, config_f, target_epochs: ('', 'option', None, int), epochs_trained: ('', 'option', None, int),
              iters_trained: ('', 'option', None, int), word_cnt: ('', 'option', None, int)):
    to.set_printoptions(sci_mode=False, linewidth=75000000000000)
    to.manual_seed(0)
    np.random.seed(0)
    random.seed(1)
    train_args = parse_config(config_f)
    tb_writer = SummaryWriter(f'{model_d}/log')
    suffix = '_short'
    num_speakers = 0
    if train_args.use_spk:
        spk_f = f'data_libri{suffix}/spk_map_750'
        num_speakers = len(open(spk_f).read().splitlines())

    encoder, word_worker, word_loss, word_contr, reg_worker = \
        model_init(train_args, word_cnt, num_speakers)

    onesided_context = get_context_size(encoder.network)

    wordf = f'data_libri{suffix}/words_mapping_750'
    if train_args.simple_set: wordf = 'data_libri/words_mapping_10'
    word_mapping = read_word_mapping_f(wordf)

    dataholder = WordDataholder(f'data_libri{suffix}/960_word_data_train_750.pkl', f'data_libri{suffix}/960_feats_750.hdf',
                                f'data_libri{suffix}/960_word_data_test_750.pkl', word_mapping, train_args.batch_size,
                                 subsampling_factor=3, onesided_context=onesided_context, small_set=train_args.small_set,
                                smooth=train_args.smooth, smooth_exp=train_args.smooth_exp, ignore_easy=train_args.ignore_easy,
                                shuffle_grouped=train_args.shuffle_grouped)
    with open(f'data_libri{suffix}/960_word_data_train_750.pkl', 'rb') as fh:
        lst = pickle.load(fh)
        random.shuffle(lst)
    if os.path.exists(f'{model_d}/iter.pkl'):
        with open(f'{model_d}/iter.pkl', 'rb') as fh:
            dct = pickle.load(fh)
            dataholder.set_state(dct)

    lr = load_checkpoint(f'{model_d}/{epochs_trained}.tar', encoder, word_loss, word_contr, word_worker, reg_worker)

    encoder.cuda(), word_worker.cuda(), word_loss.cuda(), reg_worker.cuda(), word_contr.cuda()
    all_modelparams = list(encoder.parameters()) + list(word_worker.parameters()) + list(word_loss.parameters())

    optimizer = to.optim.AdamW(list(encoder.parameters()) + list(word_worker.parameters()) + list(word_contr.parameters()) + list(word_loss.parameters()),
                               lr=train_args.lr, weight_decay=0., betas=(0.9, 0.99))
    parameters_weight_loss = [v for k, v in list(encoder.named_parameters()) + list(word_worker.named_parameters()) if 'weight' in k and 'quant' not in k]

    checkpoint = to.load(f'{model_d}/{epochs_trained}.tar')
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.25, patience=train_args.patience, threshold=1e-3, verbose=True, cooldown=5, min_lr=1e-6)
    scheduler.load_state_dict(checkpoint['scheduler'])
    start_time = time.time()
    start_iter_num = iters_trained
    last_iter_save = iters_trained
    param_norms = None
    to.set_printoptions(linewidth=120, threshold=750000)
    if train_args.do_specaug:
        freq_mask = FrequencyMasking(train_args.max_num_freq_mask, iid_masks=train_args.iid_mask)
        time_mask = TimeMasking(train_args.max_num_time_mask, iid_masks=train_args.iid_mask)
    if train_args.do_jitter:
        jitter_layer = JitterLayer(0.1)

    phone_map = {}
    word_pron = {}
    with open('data_libri/lex_ortho') as fh:
        i = 1
        for line in fh:
            word, *phones = line.split()
            for phone in phones:
                if phone not in phone_map:
                    phone_map[phone] = i
                    phone_map[i] = phone
                    i += 1
            phones = [phone_map[phone] for phone in phones]
            word_pron[word_mapping[word]] = phones
    closest_words = {} # words which have one pron different
    for word, pron in word_pron.items():
        lst = []
        for other_word, other_pron in word_pron.items():
            if word == other_word: continue
            diff = set(pron) ^ set(other_pron)
            diff = len(diff)
            assert diff
            if (diff == 1 and (len(pron) > len(other_pron) or len(other_pron) > len(pron)) or
                diff == 2 and len(pron) == len(other_pron)):
                #print(word, other_word, diff)
                lst.append(other_word)
        #logger.info(f'{word} {lst}')
        closest_words[word] = to.tensor(lst, dtype=to.long).cuda()

    div_loss_factor = train_args.div_loss
    while time.time() - start_time < 2.5 * 3600 and epochs_trained < target_epochs:
        encoder.cuda()
        word_worker.cuda()
        word_loss.cuda(), reg_worker.cuda()
        encoder.train(), word_worker.train(), word_loss.train()

        if train_args.decay_exp:
            if epochs_trained >= 1:
                dataholder.smooth_exp = 0.75
            if epochs_trained >= 10:
                dataholder.smooth_exp = 0.5

        if train_args.decay_div and epochs_trained >= 50:
            div_loss_factor = train_args.div_loss * 0.25
        if epochs_trained == train_args.first_lr_drop:
            change_lr(lr, 0.33, optimizer)
        if epochs_trained == train_args.second_lr_drop:
            change_lr(lr, 0.33, optimizer)
        for j, (f, t, dur_cnts, spks) in enumerate(dataholder.iter_train_simple()):
            optimizer.zero_grad()

            if train_args.bce_loss:
                t_onehot = fu.one_hot(t, num_classes=word_cnt).float() * 0.8
                for idx, wid in enumerate(t):
                    other_words = closest_words[wid.item()]
                    if other_words.shape[0] > 0:
                        t_onehot[idx, other_words] = 0.2 / other_words.shape[0]
                    else:
                        t_onehot[idx, wid] = 1.

                t = t_onehot
            # f_targ, f_avg = spectogram_as_target(f, onesided_context)

            if train_args.do_jitter:
                with to.no_grad():
                    f = jitter_layer(f)
            if train_args.do_specaug:
                f = f.transpose(1, 2)
                f = freq_mask(f)
                f = time_mask(f)
                f = f.transpose(1, 2)

            h_ortho, h_sm = encoder(f)
            #h_ortho = to.zeros(f.size(0), 80,)
            #for bidx, wid in enumerate(t):
            #    phones = word_pron[wid.item()]
            #    idx = 0
            #    for p in phones:
            #        for _ in range(np.random.randint(1,6)):
            #            h_ortho[bidx, idx] = p
            #            idx += 1
            #h_ortho = fu.one_hot(h_ortho.long(), num_classes=128)
            #h_ortho = h_ortho.to(f.device, dtype=to.float32)
            #h_ortho = h_ortho.matmul(encoder.quantiser.emb)

            h_word, h_word_sm = word_worker(h_ortho, h_sm, dur_cnts)
            del h_ortho

            wloss, out = word_loss(h_word, t, return_pred=True)

            pred_loss = to.tensor(0., device=t.device)
            if train_args.pred_loss:
                t_uniq = to.unique(t)
                repr_dim = h_word_sm.size(-1)
                for targ in t_uniq:
                    mask = t == targ
                    z_given_w = h_word_sm[mask].view(-1, repr_dim).sum(dim=0)
                    assert z_given_w.dim() == 1
                    z_given_w /= z_given_w.sum()
                    pred_loss += -to.sum(z_given_w * to.log(z_given_w + 1e-10))
                pred_loss /= len(t_uniq)

            if train_args.ignore_easy:
                if train_args.bce_loss:
                    mask = (out.argmax(dim=1) == t.argmax(dim=1))
                else:
                    mask = (out.argmax(dim=1) == t)
                dataholder.update_ignored(mask)

            if div_loss_factor != 0.:
                h_soft_summed = h_sm[:,:,:].sum(dim=[0, 1])
                h_soft_summed /= h_soft_summed.sum()
                div_loss = to.sum(h_soft_summed * to.log(h_soft_summed + 1e-10)) + to.log(to.tensor(train_args.repr_dim, device=h_sm.device, dtype=to.float32))
            else:
                div_loss = to.tensor(0.).to(t.device)

            if train_args.blank_loss:
                h_soft_summed = h_sm.sum(dim=[0, 1])
                h_soft_summed /= h_soft_summed.sum()
                blank_prob = h_soft_summed[0]
                rest_prob = h_soft_summed[1:].sum()
                blank_loss = (blank_prob * to.log(blank_prob + 1e-10) + rest_prob * to.log(rest_prob + 1e-10)) + to.log(to.tensor(2, device=h_sm.device, dtype=to.float32))
            else:
                blank_loss = to.tensor(0.).to(t.device)
            del h_sm
            l2_loss = sum(w.norm() for w in parameters_weight_loss)

            totloss = wloss + div_loss_factor * div_loss + train_args.blank_loss * blank_loss + train_args.w_decay * l2_loss + train_args.pred_loss * pred_loss
            totloss.backward()

            if train_args.clip_grad:
                nn.utils.clip_grad_value_(all_modelparams, 3.0)

            optimizer.step()

            word_loss_val = wloss.item()
            tb_writer.add_scalar('Loss/contr', word_loss_val, iters_trained)

            if iters_trained % 150 == 0:
                if not train_args.pred_loss:
                    logger.info(f'Done {iters_trained} iterations - Contr loss {word_loss_val:.4f} div loss {div_loss.item():.4f} blank loss {blank_loss.item():.4f} - LR {lr:.8f}')
                else:
                    logger.info(f'Done {iters_trained} iterations - Contr loss {word_loss_val:.4f} div loss {div_loss.item():.4f} pred loss {pred_loss.item():.4f} - LR {lr:.8f}')
                #logger.info(f'Done {iters_trained} iterations - Contr loss {contr_loss_val:.4f} regr loss {loss_regress.item():.4f} div loss {div_loss.item():.4f} - LR {lr:.8f}')
                report_grad_norms(model_d, encoder, word_worker, word_loss, reg_worker, iter_num=iters_trained, param_norms=param_norms)

            if time.time() - start_time >= 2.5 * 3600:
                break
            iters_trained += 1
        logger.info('Done epoch')
        epochs_trained += 1
        if (iters_trained >= last_iter_save + 750 or time.time() - start_time >= 2.5 * 3600 or epochs_trained == target_epochs) \
           and not train_args.small_set and not train_args.simple_setisf:
            test_closs = run_eval_wordchunks_constrast(tb_writer, encoder, word_worker, dataholder, word_loss,
                                                       train_args, word_cnt, closest_words, iter_num=iters_trained)
            # scheduler.step(test_closs)
            lr = get_current_lr(optimizer)
            save_checkpoint(model_d, f'{epochs_trained}', lr, optimizer, encoder, word_loss, word_worker, reg_worker, scheduler)
            last_iter_save = iters_trained
    logger.info('Ending job.')
    save_checkpoint(model_d, f'{epochs_trained}', lr, optimizer, encoder, word_loss, word_worker, reg_worker, scheduler)
    print(f'epochs={epochs_trained} iters={iters_trained}\n', file=sys.stderr)
    with open(f'{model_d}/iter.pkl', 'wb') as fh:
        pickle.dump(dataholder.get_state(), fh)


def train_onebatch(model_d, config_f, target_epochs: ('', 'option', None, int), epochs_trained: ('', 'option', None, int),
              iters_trained: ('', 'option', None, int), word_cnt: ('', 'option', None, int)):
    to.set_printoptions(sci_mode=False, linewidth=75000000000000)
    to.manual_seed(0)
    np.random.seed(0)
    random.seed(1)
    train_args = parse_config(config_f)
    tb_writer = SummaryWriter(f'{model_d}/log')
    suffix = '_short'
    num_speakers = 0
    if train_args.use_spk:
        spk_f = f'data_libri{suffix}/spk_map_750'
        num_speakers = len(open(spk_f).read().splitlines())

    encoder, word_worker, word_loss, word_contr, reg_worker = \
        model_init(train_args, word_cnt, num_speakers)

    onesided_context = get_context_size(encoder.network)

    wordf = f'data_libri{suffix}/words_mapping_750'
    if train_args.simple_set: wordf = 'data_libri/words_mapping_10'
    word_mapping = read_word_mapping_f(wordf)

    dataholder = WordDataholder(f'data_libri{suffix}/960_word_data_train_750.pkl', f'data_libri{suffix}/960_feats_750.hdf',
                                f'data_libri{suffix}/960_word_data_test_750.pkl', word_mapping, train_args.batch_size,
                                 subsampling_factor=3, onesided_context=onesided_context, small_set=train_args.small_set,
                                smooth=train_args.smooth, smooth_exp=train_args.smooth_exp, ignore_easy=train_args.ignore_easy,
                                shuffle_grouped=train_args.shuffle_grouped)
    with open(f'data_libri{suffix}/960_word_data_train_750.pkl', 'rb') as fh:
        lst = pickle.load(fh)
        random.shuffle(lst)
    if os.path.exists(f'{model_d}/iter.pkl'):
        with open(f'{model_d}/iter.pkl', 'rb') as fh:
            dct = pickle.load(fh)
            dataholder.set_state(dct)

    lr = load_checkpoint(f'{model_d}/{epochs_trained}.tar', encoder, word_loss, word_contr, word_worker, reg_worker)

    encoder.cuda(), word_worker.cuda(), word_loss.cuda(), reg_worker.cuda(), word_contr.cuda()
    all_modelparams = list(encoder.parameters()) + list(word_worker.parameters()) + list(word_loss.parameters())

    optimizer = to.optim.AdamW(list(encoder.parameters()) + list(word_worker.parameters()) + list(word_contr.parameters()) + list(word_loss.parameters()),
                               lr=train_args.lr, weight_decay=0., betas=(0.9, 0.99))
    parameters_weight_loss = [v for k, v in list(encoder.named_parameters()) + list(word_worker.named_parameters()) if 'weight' in k and 'quant' not in k]

    checkpoint = to.load(f'{model_d}/{epochs_trained}.tar')
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.25, patience=train_args.patience, threshold=1e-3, verbose=True, cooldown=5, min_lr=1e-6)
    scheduler.load_state_dict(checkpoint['scheduler'])
    start_time = time.time()
    start_iter_num = iters_trained
    last_iter_save = iters_trained
    param_norms = None
    to.set_printoptions(linewidth=120, threshold=750000)

    div_loss_factor = train_args.div_loss
    encoder.cuda()
    word_worker.cuda()
    word_loss.cuda(), reg_worker.cuda()
    encoder.train(), word_worker.train(), word_loss.train()

    f, t, dur_cnts, spks, utts = next(dataholder.iter_train_simple(return_utts=True))
    import matplotlib.pyplot as plt
    indcs = [i for i in range(len(utts)) if utts[i] == '5826-53496-0022-1']
    idx = indcs[0]
    plt.imshow(f[idx].cpu())
    logger.info(f[idx].cpu()[:10, :10])
    logger.info(f'{utts[idx]} {t[idx].cpu()}')
    plt.savefig('img.png')
    for j in range(100):
        optimizer.zero_grad()

        h_ortho, h_sm = encoder(f)

        h_word, h_word_sm = word_worker(h_ortho, h_sm, dur_cnts)

        wloss, out = word_loss(h_word, t, return_pred=True)

        pred_loss = to.tensor(0., device=t.device)
        t_uniq = to.unique(t)
        repr_dim = h_word_sm.size(-1)
        for targ in t_uniq:
            mask = t == targ
            z_given_w = h_word_sm[mask].view(-1, repr_dim).sum(dim=0)
            assert z_given_w.dim() == 1
            z_given_w /= z_given_w.sum()
            pred_loss += -to.sum(z_given_w * to.log(z_given_w + 1e-10))
        pred_loss /= len(t_uniq)

        if div_loss_factor != 0.:
            h_soft_summed = h_sm[:,:,:].sum(dim=[0, 1])
            h_soft_summed /= h_soft_summed.sum()
            div_loss = to.sum(h_soft_summed * to.log(h_soft_summed + 1e-10)) + to.log(to.tensor(train_args.repr_dim, device=h_sm.device, dtype=to.float32))
        else:
            div_loss = to.tensor(0.).to(t.device)

        if train_args.blank_loss:
            h_soft_summed = h_sm.sum(dim=[0, 1])
            h_soft_summed /= h_soft_summed.sum()
            blank_prob = h_soft_summed[0]
            rest_prob = h_soft_summed[1:].sum()
            blank_loss = (blank_prob * to.log(blank_prob + 1e-10) + rest_prob * to.log(rest_prob + 1e-10)) + to.log(to.tensor(2, device=h_sm.device, dtype=to.float32))
        else:
            blank_loss = to.tensor(0.).to(t.device)
        del h_sm
        l2_loss = sum(w.norm() for w in parameters_weight_loss)

        totloss = wloss + div_loss_factor * div_loss + train_args.blank_loss * blank_loss + train_args.w_decay * l2_loss + train_args.pred_loss * pred_loss
        totloss.backward()

        optimizer.step()

        word_loss_val = wloss.item()

        logger.info(f'Done {iters_trained} iterations - Contr loss {word_loss_val:.4f} div loss {div_loss.item():.4f} pred loss {pred_loss.item():.4f} - LR {lr:.8f}')
        report_grad_norms(model_d, encoder, word_worker, word_loss, reg_worker, iter_num=iters_trained, param_norms=param_norms)

        iters_trained += 1

import plac; plac.call(train_onebatch)


