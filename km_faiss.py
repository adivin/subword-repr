import numpy as np
import faiss
# from matplotlib import pyplot
from loguru import logger
import pickle

from ahc import Cluster
import os
from glob import glob


REPRDIM = 256


def kmeans(data, targ_clusters, f, offset, lst_coverage=None):
    logger.info(f'Starting kmeans, data shape is {data.shape}, clustering down to {targ_clusters}')
    niters = 20

    kmeans = faiss.Kmeans(REPRDIM, targ_clusters, niter=niters, verbose=True, gpu=True, max_points_per_centroid=1000_000_000)
    kmeans.train(data)
    centroids = kmeans.centroids
    _, assignments = kmeans.index.search(data, 1)

    lst = []
    for i, centroid in enumerate(centroids):
        if np.isnan(centroid).any():
            continue
        indcs_orig = np.where(assignments == i)[0]
        indcs = []
        if lst_coverage is None:
            indcs = [idx+offset for idx in indcs_orig]
        else:
            for idx in indcs_orig:
                indcs.extend([sample_index for sample_index in lst_coverage[idx]])

        c = Cluster(centroid, indcs)
        lst.append(c)

    logger.info(f'Saving {targ_clusters} clusters to {f}.')
    with open(f, 'wb') as fh:
        pickle.dump(lst, fh, protocol=pickle.HIGHEST_PROTOCOL)


def first_split(num_f, prefix):
    with open(num_f, 'rb') as fh:
        text = fh.read()

        data = np.frombuffer(text, dtype=np.float32)
        data = data.reshape(-1, REPRDIM)
        del text
        cluster_num = 500_000

        f = f'{prefix}_0'
        kmeans(data, cluster_num, f, 0)


def get_lst_coverage(lst_clusters):
    lst = []
    for c in lst_clusters:
        lst.append(c.coverage)
    return lst


def cluster_clusters(f, cluster_d):
    lst_clusters = []

    with open(f, 'rb') as fh:
        lst_clusters.extend(pickle.load(fh))

    cluster_num = len(lst_clusters)
    logger.info(f'Num clusters is {cluster_num}')
    # min_idx = 1000_000_000
    data = []
    for c in lst_clusters:
        data.append(c.x)

    data = np.array(data, dtype=np.float32)
    targ_cluster_num = 5000
    newf = f'{cluster_d}/km_cluster_final_{targ_cluster_num}'

    lst_coverage = get_lst_coverage(lst_clusters)
    kmeans(data, targ_cluster_num, newf, None, lst_coverage)
    logger.info('Done.')


def main(arg1, arg2, is_first_run: ('Initial split or not', 'flag', 'f')):
    if is_first_run:
        first_split(arg1, arg2)
    else:
        cluster_clusters(arg1, arg2)


if __name__ == '__main__':
    import plac
    plac.call(main)
