import os
import wandb
import plac
import hyperopt
import subprocess as sp
import random
import string
from functools import partial
import math as m
from loguru import logger
import numpy as np
import time
import shutil
import time
from misc.misc import parse_config
from itertools import product

def sample_uniformlog(low, high):
    return m.exp(np.random.uniform(m.log(low), m.log(high)))


def sample_uniform(low, high):
    return np.random.uniform(low, high)


def iter_uniformlog(low, high, count):
    return list(np.exp(np.linspace(m.log(low), np.log(high), count)))


def iter_uniform(low, high, count):
    return list(np.linspace(low, high, count))


def run(params, workdir=None):
    name = wandb.util.generate_id()
    workname = f'{workdir}/model-{name}'

    lr, drop_r, w_decay, xent_loss, contr_loss, div_loss, first_lr_drop, starting_sm_temp, decay_sm_temp, decay_sm_period, quant_dim, num_layers_contraster, \
        aug_snr, add_aug, repr_drop, jitter, use_layernorm, decay_div, decay_div_period, div_clamp, threshold, contr_dim, use_sgd, gamma, decay_gamma, decay_gamma_period, \
        cosine_temp, use_emb_grad, emb_dim, use_orig, num_negs, proj_w_loss, accumulate_count, contr_temp, add_input, neg_margin, use_gs = params

    train_args = parse_config('train_conf')
    dct = {'lr': lr, 'drop_r': drop_r, 'w_decay': w_decay, 'xent_loss': xent_loss, 'contr_loss': contr_loss,
                                'div_loss': div_loss, 'first_lr_drop': first_lr_drop, 'decay_sm_temp': decay_sm_temp, 'decay_sm_period': decay_sm_period,
          'quant_dim': quant_dim, 'starting_sm_temp': starting_sm_temp,
           'num_layers_contraster': num_layers_contraster, 'aug_snr': aug_snr, 'add_aug': add_aug, 'repr_drop': repr_drop,
           'jitter': jitter, 'use_layernorm': use_layernorm, 'decay_div': decay_div, 'decay_div_period': decay_div_period, 'div_clamp': div_clamp,
           'threshold': threshold, 'contr_dim': contr_dim, 'use_sgd': use_sgd, 'gamma': gamma, 'decay_gamma': decay_gamma, 'decay_gamma_period': decay_gamma_period,
           'cosine_temp': cosine_temp, 'use_emb_grad': use_emb_grad, 'emb_dim': emb_dim, 'use_orig': use_orig, 'num_negs': num_negs, 'proj_w_loss': proj_w_loss,
           'accumulate_count': accumulate_count, 'contr_temp': contr_temp, 'add_input': add_input, 'neg_margin': neg_margin, 'use_gs': use_gs}
    train_args.__dict__.update(dct)
    train_args.write(f'tmpconf_{name}')
    os.makedirs(workname)
    cmd = f'python trainer_short.py -del-conf {workname} tmpconf_{name}'
    sp.Popen(cmd, shell=True)
    # logger.info(f'Submitted {name}.')
    time.sleep(3)


def count_jobs():
    s = sp.check_output('qstat | wc -l', shell=True)
    return int(s.strip())


def main(workdir):
    #if os.path.exists(workdir):
    #    shutil.rmtree(workdir)
    func = partial(run, workdir=workdir)
    lr = [1e-3, 1e-4]
    drop_r = [0.]
    w_decay = [0.]
    xent_loss = [0.]
    contr_loss = [1.]
    div_loss = [ 1., 0.5]
    first_lr_drop = [-1]
    quant_dim = [64]
    num_layers_contraster = [1]
    aug_snr = [15]
    add_aug = [True]
    repr_drop = [0.]
    jitter = [ 0.]
    use_layernorm = [True]
    threshold = [False]
    contr_dim = [128]
    starting_sm_temp = [2., 0.5]
    decay_sm_temp = [0.25]
    decay_sm_period = [ 5000]
    decay_div = [False]
    decay_div_period = [10000]
    div_clamp = [2.5]
    use_sgd = [False]
    gamma = [ 5., 1.]
    decay_gamma = [ 0.2, 0.]
    decay_gamma_period = [ 5000]
    cosine_temp = [1.]
    use_emb_grad = [ True]
    emb_dim = [128]
    use_orig = [True]
    num_negs = [32, 64, 128]
    proj_w_loss = [0.]
    accumulate_count = [1, 4]
    contr_temp = [1.]
    add_input = [ True]
    neg_margin = [ 0.5, 1., 1.5]
    use_gs = [False, True]

    lst_params = list(product(lr, drop_r, w_decay, xent_loss, contr_loss, div_loss, first_lr_drop, starting_sm_temp, decay_sm_temp, decay_sm_period, quant_dim,
                              num_layers_contraster, aug_snr, add_aug, repr_drop, jitter, use_layernorm, decay_div, decay_div_period, div_clamp, threshold, contr_dim,
                              use_sgd, gamma, decay_gamma, decay_gamma_period, cosine_temp, use_emb_grad, emb_dim, use_orig, num_negs, proj_w_loss, accumulate_count,
                              contr_temp, add_input, neg_margin, use_gs))
    logger.info(f'Going to run {len(lst_params)} builds!')
    random.shuffle(lst_params)
    for params in lst_params:
        func(params)
        while count_jobs() > 100:
            time.sleep(600)


plac.call(main)
