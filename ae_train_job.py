import time
import sys

import numpy as np
import torch as to
import torch.nn as nn
from loguru import logger
from torch.utils.tensorboard import SummaryWriter
from pyinstrument import Profiler

from format_data import WordDataholder
from trainer import get_context_size, change_lr, spectogram_as_target, mask_spectrogram
from ae_trainer_short import load_checkpoint, save_checkpoint, model_init


def get_current_lr(optimizer):
    lr = None
    for param_group in optimizer.param_groups:
        lr = param_group['lr']
    return lr


def run_eval_regr(tb_writer, encoder, reg_worker, dataholder, onesided_context,
                  collect_results=False, iter_num=None, do_vq=False):
    encoder.eval()
    reg_worker.eval()
    summed_loss = 0.
    with to.no_grad():
        n = 0
        for f in dataholder.iter_regr_test():
            f_targ, f_avg = spectogram_as_target(f, onesided_context)

            h = encoder(f, do_vq=do_vq)

            loss_regress = reg_worker(h, f_avg, f_targ)

            summed_loss += loss_regress.item()
            n += 1
        summed_loss /= n
        logger.info(f'Test loss - {summed_loss}')
    return summed_loss


def train_job(model_d,
              target_epochs: ('', 'option', None, int), epochs_trained: ('', 'option', None, int), iters_trained: ('', 'option', None, int),
              w_decay: ('', 'option', None, float), drop_r: ('', 'option', None, float), batch_size: ('', 'option', None, int),
              large_model: ('', 'flag', None), use_layernorm:( '', 'flag', None) = False,
              start_vq: ('', 'option', None, int) = 1001, add_noise: ('', 'option', None) = 0.,
              repr_dim: ('', 'option', None, int) = 128,
              sgd: ('', 'option', None, float) = -1., small_set: ('', 'flag', None) = False,
              patience: ('', 'option', None, int) = 1, first_lr_drop: ('', 'option', None, int) = -1,
              second_lr_drop: ('', 'option', None, int) = 6, do_mask: ('', 'flag', None) = False):
    tb_writer = SummaryWriter(f'{model_d}/log')
    do_vq = False
    if epochs_trained >= start_vq:
        do_vq = True
        logger.info('Using VQ.')
    encoder, reg_worker = model_init(repr_dim, w_decay, drop_r, large_model, use_layernorm)

    onesided_context = get_context_size(encoder)

    word_mapping = {}
    wordf = 'data/words_mapping'
    # if small_set: wordf = 'data/words_mapping_100'
    with open(wordf) as fh:
        for line in fh:
            word, id = line.split()
            word_mapping[word] = int(id)
            word_mapping[int(id)] = word
    # if not small_set:
    dataholder = WordDataholder('data/word_data_1500.pkl', 'data/feats_1500.hdf', word_mapping, batch_size,
                                subsampling_factor=3, onesided_context=onesided_context, small_set=small_set)
    # else:
    #     dataholder = WordDataholder('data/word_data_100.pkl', 'data/feats_100.hdf', word_mapping, batch_size,
    #                                 subsampling_factor=3, onesided_context=onesided_context)

    lr = load_checkpoint(f'{model_d}/{epochs_trained}.tar', encoder, reg_worker)

    onesided_context = get_context_size(encoder)

    # encoder.cuda(), reg_worker.cuda()
    all_modelparams = list(encoder.parameters()) + list(reg_worker.parameters())

    if sgd < 0.:
        optimizer = to.optim.AdamW(list(encoder.parameters()) + list(reg_worker.parameters()), lr=lr, weight_decay=w_decay, betas=(0.9, 0.99))
    else:
        logger.info('Using SGD.')
        optimizer = to.optim.SGD(list(encoder.parameters()) + list(reg_worker.parameters()), lr=lr, momentum=sgd)
    checkpoint = to.load(f'{model_d}/{epochs_trained}.tar')
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.25, patience=patience, threshold=1e-3, verbose=True, cooldown=5, min_lr=1e-6)
    scheduler.load_state_dict(checkpoint['scheduler'])

    start_time = time.time()
    start_iter_num = iters_trained
    while time.time() - start_time < 2.4 * 3600 and epochs_trained < target_epochs:
        # encoder.cuda(), reg_worker.cuda()
        encoder.train(), reg_worker.train()
        if epochs_trained == first_lr_drop:
            change_lr(lr, 0.1, optimizer)
        if epochs_trained == second_lr_drop:
            change_lr(lr, 0.25, optimizer)
        profiler = Profiler()
        profiler.start()
        for j, f in enumerate(dataholder.iter_regr()):
            optimizer.zero_grad()

            f_targ, f_avg = spectogram_as_target(f, onesided_context)
            if do_mask:
                f, _ = mask_spectrogram(f, onesided_context)

            h = encoder(f, do_vq=do_vq, add_noise=add_noise)

            loss_regress = reg_worker(h, f_avg, f_targ)

            loss_regress.backward()

            nn.utils.clip_grad_value_(all_modelparams, 3.0)

            optimizer.step()
            if np.random.choice(4) == 0:
                with to.no_grad():
                    encoder.constrain_orthonormal()

            regr_loss_val = loss_regress.item()
            tb_writer.add_scalar('Loss/regr', regr_loss_val, iters_trained)

            if (iters_trained+1) % 150 == 0:
                logger.info(f'Done {iters_trained} iterations - Train loss {regr_loss_val:.4f} - LR {lr:.8f}')
            iters_trained += 1
            if time.time() - start_time > 2.6 * 3600:
                break
            if j == 100:
                break
        profiler.stop()
        print(profiler.output_text(unicode=True, color=True), file=open('profile.txt', 'w'))
        raise RuntimeError
        test_rloss = run_eval_regr(tb_writer, encoder, reg_worker, dataholder, onesided_context, False, None, do_vq=do_vq)
        scheduler.step(test_rloss)
        lr = get_current_lr(optimizer)
        epochs_trained += 1
        save_checkpoint(model_d, f'{epochs_trained}', lr, optimizer, encoder, reg_worker, scheduler)
    logger.info('Ending job.')
    print(f'epochs={epochs_trained} iters={iters_trained}\n', file=sys.stderr)


import plac; plac.call(train_job)