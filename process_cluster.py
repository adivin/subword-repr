import pickle
from ahc import Cluster
from tqdm import tqdm
import numpy as np
from loguru import logger
from model import Tdnnf
import torch as to

REPRDIM = 256


def process_clusters(cluster_f, words_f, outf):
    with open(cluster_f, 'rb') as fh:
        lst = pickle.load(fh)

    idx_to_cluster = {}
    for i, c in enumerate(tqdm(lst)):
        for idx in c.coverage:
            assert idx not in idx_to_cluster
            idx_to_cluster[int(idx)] = str(i)

    lst = []
    total_cnt = 0
    with open(words_f) as fh:
        linegroup = []
        for line in fh:
            if line.startswith('-'):
                cids = [idx_to_cluster[int(idx)] for idx, _, _ in linegroup]
                lst.append((linegroup[0][2], linegroup[0][1], cids))
                linegroup = []
                continue
            idx, word, uttid = line.split()
            linegroup.append((idx, word, uttid))
            total_cnt += 1

    logger.info('Writing out sequences.')

    with open(outf, 'w') as fh:
        for entry in lst:
            fh.write(f'{entry[0]} {entry[1]} {" ".join(entry[2])}\n')


import plac
plac.call(process_clusters)