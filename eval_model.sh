#!/bin/bash -e 

model=$1
name=$2
num=$3
utils/queue.pl -V -l q_short_gpu  $PRON/timitlog_$name python -m timit_train -do-vq -epochs 10 -lr 0.005 -basemodel-lr 0. -encoder-f $model -num-speakers $num $PRON/timitmodel_$name

conf=$(dirname $model)/conf
tmodel=$PRON/timitmodel_$name
python -m misc.decode_timit -num-speakers $num -do-vq $tmodel/encoder.pt $conf $tmodel/classifier.pt timit_data_39/test/data.hdf timit_data_39/test/targs.pkl $tmodel/loglikes /idiap/temp/rbraun/code/kaldi/egs//timit/s5b/o3_test.fst timit_data_39/train/phones.txt $tmodel/best_path.ark.txt
