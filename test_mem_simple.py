import os
import psutil
import torch as to

def get_memory_usage():
    process = psutil.Process(os.getpid())
    return process.memory_percent()

def main_simple():
    b = to.randn(100000).cuda()
    print('starting')
    lst = []
    for i in range(7):
        a=to.randn(5_000_000_000 // 4, dtype=to.float32)
        perc = get_memory_usage()
        print(f'{i} {perc}')
        lst.append(a)
    print('Done')

main_simple()
