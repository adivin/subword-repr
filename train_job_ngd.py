import time
import sys

import numpy as np
import torch as to
import torch.nn as nn
from loguru import logger
from torch.utils.tensorboard import SummaryWriter

from format_data import WordDataholder
from trainer import get_context_size, change_lr, spectogram_as_target, run_eval_wordchunks_constrast
from trainer_short import load_checkpoint, save_checkpoint, model_init
from ngd import NGD


def get_current_lr(optimizer):
    lr = None
    for param_group in optimizer.param_groups:
        lr = param_group['lr']
    return lr


def train_job(model_d, target_epochs: ('', 'option', None, int), epochs_trained: ('', 'option', None, int), iters_trained: ('', 'option', None, int),
              w_decay: ('', 'option', None, float), temp: ('', 'option', None, float), drop_r: ('', 'option', None, float), batch_size: ('', 'option', None, int),
              word_cnt: ('', 'option', None, int), large_model: ('', 'flag', None), contr_loss_factor: ('', 'option', None, float),
              xent_loss_f: ('', 'option', None, float), regr_loss_f: ('', 'option', None, float), stop_xent: ('', 'option', None, int)):
    tb_writer = SummaryWriter(f'{model_d}/log')

    encoder, word_worker, word_classifier, reg_worker, word_contraster = \
        model_init(w_decay, drop_r, word_cnt, temp, large_model)

    onesided_context = get_context_size(encoder)

    word_mapping = {}
    with open('data/words_mapping') as fh:
        for line in fh:
            word, id = line.split()
            word_mapping[word] = int(id)
            word_mapping[int(id)] = word
    dataholder = WordDataholder('data/word_data_v4.pkl', 'data/feats_v4.hdf', word_mapping, batch_size,
                                subsampling_factor=3, onesided_context=onesided_context)

    lr = load_checkpoint(f'{model_d}/{epochs_trained}.tar', encoder, word_worker, word_classifier, reg_worker)

    onesided_context = get_context_size(encoder)

    criterion = nn.CrossEntropyLoss(reduction='mean')

    encoder.cuda(), word_worker.cuda(), word_contraster.cuda(), word_classifier.cuda(), reg_worker.cuda()
    all_modelparams = list(encoder.parameters()) + list(word_worker.parameters()) + list(word_classifier.parameters()) + \
                      list(reg_worker.parameters())

    optimizer = NGD(list(encoder.parameters()) + list(word_worker.parameters()) + list(reg_worker.parameters()) + list(word_classifier.parameters()), lr=lr, weight_decay=w_decay, momentum=0.9)
    checkpoint = to.load(f'{model_d}/{epochs_trained}.tar')
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.5, patience=5, verbose=True, cooldown=10, min_lr=1e-6)
    scheduler.load_state_dict(checkpoint['scheduler'])

    start_time = time.time()
    while time.time() - start_time < 2.4 * 3600 and epochs_trained < target_epochs:
        encoder.cuda(), word_worker.cuda(), word_contraster.cuda(), word_classifier.cuda(), reg_worker.cuda()
        encoder.train(), word_worker.train(), word_contraster.train(), word_classifier.train(), reg_worker.train()

        for j, (f, t, dur_cnts, lst_groups) in enumerate(dataholder.iter_train()):
            optimizer.zero_grad()

            if epochs_trained == 0:
                f_targ, f_avg = spectogram_as_target(f, onesided_context)

            h = encoder(f)
            h_word = word_worker(h, dur_cnts)

            closs = word_contraster(h_word, lst_groups) * contr_loss_factor

            if epochs_trained < stop_xent:
                out = word_classifier(h_word)
                loss_xent = criterion(out, t) * xent_loss_f
            else:
                loss_xent = to.tensor(0.).cuda()

            if epochs_trained == 0:
                loss_regress = reg_worker(h, f_avg, f_targ) * regr_loss_f
            else:
                loss_regress = to.tensor(0.).cuda()

            totloss = closs + loss_xent + loss_regress
            totloss.backward()

            nn.utils.clip_grad_value_(all_modelparams, 3.0)

            optimizer.step()
            if np.random.choice(4) == 0:
                with to.no_grad():
                    encoder.constrain_orthonormal()

            crontr_loss_val, xent_loss_val, regr_loss_val = closs.item(), loss_xent.item(), loss_regress.item()
            tb_writer.add_scalar('Loss/xent', xent_loss_val, iters_trained)
            tb_writer.add_scalar('Loss/contr', contr_loss_factor, iters_trained)
            tb_writer.add_scalar('Loss/regr', regr_loss_val, iters_trained)

            if (iters_trained+1) % 150 == 0:
                logger.info(f'Done {iters_trained} iterations - Train loss {xent_loss_val:.4f} {crontr_loss_val:.4f} {regr_loss_val:.4f} - LR {lr:.8f}')
            iters_trained += 1

        test_closs = run_eval_wordchunks_constrast(tb_writer, encoder, word_worker, word_classifier, reg_worker, dataholder, criterion,
                                                   word_contraster, contr_loss_factor, iter_num=iters_trained)
        scheduler.step(test_closs)
        lr = get_current_lr(optimizer)
        epochs_trained += 1
        save_checkpoint(model_d, f'{epochs_trained}', lr, optimizer, encoder, word_worker, reg_worker, word_classifier, scheduler)
    logger.info('Ending job.')
    print(f'epochs={epochs_trained} iters={iters_trained}\n', file=sys.stderr)


import plac; plac.call(train_job)