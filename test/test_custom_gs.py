import torch as to
import torch.nn as nn
from loguru import logger
from torch_functions import custom_gs
from model import VectorQuantifierGS_Single, WordWorkerNoOrder, WordClassifier, VectorQuantifierGS


class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.vq = VectorQuantifierGS_Single(3, 1., True)
        self.combiner = WordWorkerNoOrder(3, 4)
        self.classifier = WordClassifier(3, 4, 1.)

    def forward(self, x):
        x_q = self.vq(x)
        dur_cnts = ([2], [4])
        x = self.combiner(x_q, dur_cnts)
        out = self.classifier(x)
        return out


def main():
    targs = to.tensor([0, 1, 2, 3])
    data = to.tensor([[[0., 0, 1.], [0, 1, 0.]], [[1., 0, 0], [0, 0, 1.]], [[1.,0, 0], [1., 0, 0]],
                      [[0., 0., 1.], [0., 0., 1.]]])
    logger.info(data.shape)
    criterion = nn.CrossEntropyLoss()

    model = Model()
    optimizer = to.optim.Adam(model.parameters(), lr=1e-1)

    logger.info('Starting')
    model.train()
    for i in range(100):
        optimizer.zero_grad()
        out = model(data)
        loss = criterion(out, targs)
        logger.info(f'LOSS {loss.item()}')
        loss.backward()
        optimizer.step()

main()
