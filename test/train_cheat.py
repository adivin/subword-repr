""" Inputs will be sequences of onehot vectors with phones, we then want to see how different word workers perform
 in combining these sequences representations so as result in a low cross-entropy or contrastive loss """

from collections import defaultdict

import torch as to
import torch.nn as nn
from loguru import logger
import math
from format_data import WordDataholder
from misc.misc import read_word_mapping_f
import numpy as np
import random
from model import WordContrastiveLoss, WordWorker, WordWorkerRnnPlain, WordClassifier, WordWorkerNoOrder, WordPosPairLoss

LEX = 'data_libri/lex_ortho'


def read_lex():
    word_mapping = read_word_mapping_f('data_libri/words_mapping_1000')

    phone_syms = {}
    lexicon = defaultdict(list)
    idx = 0
    with open(LEX) as fh:
        for line in fh:
            word, *phones = line.split()
            for phone in phones:
                if phone not in phone_syms:
                    phone_syms[phone] = idx
                    phone_syms[idx] = phone
                    idx += 1
                phone_idx = phone_syms[phone]
                assert not isinstance(phone_idx, str)
                lexicon[word_mapping[word]].append(phone_idx)
    return word_mapping, lexicon, phone_syms


def create_feats(batch_size, num_timesteps, repr_dim, t, lexicon, dur_cnts):
    feats = to.zeros(batch_size, math.ceil(num_timesteps / 3), repr_dim)
    durs_unrolled = []
    durs, cnts = dur_cnts
    for dur, cnt in zip(durs, cnts):
        for _ in range(cnt):
            durs_unrolled.append(dur)
    for i, widx in enumerate(t):
        phone_indcs = lexicon[widx.item()]
        dur = durs_unrolled[i]
        try:
            assert len(phone_indcs) <= dur, f'{lexicon[widx.item()]} {dur}'
        except:
            import IPython; IPython.embed()
            raise RuntimeError
        indcs = np.array([phone_indcs[idx] for idx in sorted(random.choices(range(len(phone_indcs)), k=dur.item()))])
        while len(np.unique(indcs)) != len(phone_indcs):
            indcs = np.array([phone_indcs[idx] for idx in sorted(random.choices(range(len(phone_indcs)), k=dur.item()))])
        phone_ali = to.as_tensor(indcs)
        time_indcs = to.arange(0, dur)
        try:
            feats[i, time_indcs, phone_ali] = 1
        except:
            import IPython; IPython.embed()
            raise RuntimeError
    # import IPython; IPython.embed()
    return feats


def alter_targs(t, wid_map):
    return to.tensor([wid_map[t_idx.item()] for t_idx in t])


def main():
    to.manual_seed(0)
    np.random.seed(0)
    to.cuda.manual_seed(0)
    random.seed(0)

    word_mapping, lexicon, phone_syms = read_lex()

    # Words with same combination of prons grouped together (order does not matter!)
    # phone_to_words = defaultdict(list)
    # for wid, phones in lexicon.items():
    #     key = ' '.join(str(c) for c in sorted(phones))
    #     phone_to_words[key].append(wid)
    # wid_map = {}
    # for i, wids in enumerate(phone_to_words.values()):
    #     for wid in wids:
    #         wid_map[wid] = i

    repr_dim = len(phone_syms) // 2

    dataholder = WordDataholder('data_libri/960_word_data_1000.pkl', 'data_libri/960_feats_1000.hdf', word_mapping, 512,
                                subsampling_factor=3, onesided_context=0, small_set=True,
                                smooth=True)

    word_worker = WordWorkerNoOrder(repr_dim, repr_dim)

    word_contraster = WordPosPairLoss(0.2)
    word_classifier = WordClassifier(repr_dim, len(lexicon), 1.)
    criterion = nn.CrossEntropyLoss(reduction='mean')

    optimizer = to.optim.AdamW(list(word_worker.parameters()) + list(word_classifier.parameters()),
                               lr=1e-2, weight_decay=0., betas=(0.9, 0.99))
    iters_trained = 0
    word_worker.cuda(), word_classifier.cuda()
    word_worker.train(), word_worker.train()
    for i in range(50):
        for j, (f, t, dur_cnts, lst_groups, spks) in enumerate(dataholder.iter_train()):
            # print('B', t[0])
            optimizer.zero_grad()
            # t = t.cpu()
            feats = create_feats(512, f.size(1), repr_dim, t.cpu(), lexicon, dur_cnts).cuda()
            # new_t = alter_targs(t, wid_map).cuda()

            h_word = word_worker(feats, dur_cnts)

            closs = word_contraster(h_word, t)
            # closs = to.tensor(0.).cuda()
            out = word_classifier(h_word)
            loss_xent = criterion(out, t)
            # import IPython; IPython.embed()
            totloss = closs + loss_xent
            totloss.backward()

            optimizer.step()

            contr_loss_val, xent_loss_val = closs.item(), loss_xent.item()

            if (iters_trained+1) % 10 == 0:
                logger.info(f'Done iter {iters_trained} - Train loss {xent_loss_val:.4f} {contr_loss_val:.6f}')
            # if iters_trained == 100:
            #     import IPython; IPython.embed()
            iters_trained += 1

main()