import os
import random
import shutil
import subprocess as sp
import sys

import numpy as np
import regex as re
import torch as to
from loguru import logger

from model import Tdnnf, WordWorker, WordContrastiveLossMine, WordClassifier, RegressionWorker
from ngd import NGD
from trainer import NUMWORDS

REPRDIM = 128


def model_init(w_decay, drop_r, word_cnt, temp, large_model=False):
    if not large_model:
        encoder = Tdnnf(40, REPRDIM, drop_r, large_size=1280, small_size=128, use_vq=True)
    else:
        conv_stride_list = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1]
        time_stride_list = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0]
        encoder = Tdnnf(40, REPRDIM, drop_r, large_size=1536, small_size=256, conv_stride_list=conv_stride_list,
                        time_stride_list=time_stride_list, use_vq=True)
    word_worker = WordWorker(REPRDIM, 1024)
    word_classifier = WordClassifier(1024, word_cnt, temp=3.)
    reg_worker = RegressionWorker(REPRDIM, 60)
    word_contraster = WordContrastiveLossMine(temp)
    return encoder, word_worker, word_classifier, reg_worker, word_contraster


def save_checkpoint(model_d, epoch, lr, optimizer, encoder, word_worker, reg_worker, word_classifier, scheduler):
    fpath = f'{model_d}/{epoch}.tar'
    encoder.cpu(), word_worker.cpu(), word_classifier.cpu(), reg_worker.cpu()
    to.save({
        'encoder': encoder.state_dict(),
        'word_worker': word_worker.state_dict(),
        'reg_worker': reg_worker.state_dict(),
        'word_classifier': word_classifier.state_dict(),
        'optimizer': optimizer.state_dict(),
        'scheduler': scheduler.state_dict(),
        'lr': lr
    }, fpath)


def load_checkpoint(fpath, encoder, word_worker, word_classifier, reg_worker):
    checkpoint = to.load(fpath)
    encoder.load_state_dict(checkpoint['encoder'])
    word_worker.load_state_dict(checkpoint['word_worker'])
    word_classifier.load_state_dict(checkpoint['word_classifier'])
    reg_worker.load_state_dict(checkpoint['reg_worker'])
    lr = checkpoint['lr']
    return lr


def train_job(model_d, epochs, epochs_trained, iters_trained, w_decay, temp,
              drop_r, batch_size, word_cnt, large_model, contr_loss_factor, xent_loss_f, regr_loss_f, stop_xent):
    gpumem = 20 if batch_size <= 1024 else 30
    large_model_str = '-large-model' if large_model else ''
    logf = f'{model_d}/trainlog{epochs_trained}'
    cmd = f'utils/queue.pl -V -l q_short_gpu -l gpumem={gpumem} {logf} python train_job_ngd.py {model_d} -batch-size {batch_size} -target-epochs {epochs} ' \
          f' -epochs-trained {epochs_trained} -iters-trained {iters_trained} -w-decay {w_decay} -temp {temp} -drop-r {drop_r}' \
          f' -word-cnt {word_cnt} {large_model_str} -contr-loss-factor {contr_loss_factor} -xent-loss-f {xent_loss_f}' \
          f' -regr-loss-f {regr_loss_f} -stop-xent {stop_xent}'
    result = sp.run(cmd, shell=True)
    if result.returncode != 0:
        logger.warning('FAILED!')
        sys.exit(0)
    with open(logf) as fh:
        text = fh.read()
        epochs_done = re.search(r'(?<=epochs=)\d+', text).group(0)
        iters_done = re.search(r'(?<=iters=)\d+', text).group(0)
    epochs_trained = int(epochs_done)
    iters_trained = int(iters_done)
    return epochs_trained, iters_trained


def train(model_d, epochs, lr, batch_size, w_decay, temp,
          contr_loss_factor, xent_loss_f, regr_loss_f, drop_r, first_lr_drop, second_lr_drop, stop_xent, large_model):

    word_cnt = NUMWORDS

    encoder, word_worker, word_classifier, reg_worker, _ = \
        model_init(w_decay, drop_r, word_cnt, temp, large_model)
    optimizer = NGD(list(encoder.parameters()) + list(word_worker.parameters()) + list(reg_worker.parameters()) + list(word_classifier.parameters()), lr=lr, weight_decay=w_decay, momentum=0.9)
    scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.5, patience=5, verbose=True, cooldown=10, min_lr=1e-6)
    save_checkpoint(model_d, 0, lr, optimizer, encoder, word_worker, reg_worker, word_classifier, scheduler)

    epochs_trained = 0
    iters_trained = 0
    while epochs_trained < epochs:
        # logger.info('Submitting job.')
        epochs_trained, iters_trained = train_job(model_d, epochs, epochs_trained, iters_trained, w_decay, temp,
                                                  drop_r, batch_size, word_cnt, large_model, contr_loss_factor, xent_loss_f, regr_loss_f, stop_xent)


def main(model_d, continue_train: ('Continue training from last model', 'flag', 'c'),
         epochs: ('Num epochs', 'option', None, int) = 10,
         lr: ('Learning rate', 'option', None, float) = 1e-2,
         batch_size: ('BS', 'option', None, int) = 1024,
         w_decay: ('Weight decay lambda', 'option', None, float) = 0.,
         temp: ('Word contrast temp', 'option', None, float) = 0.2,
         contr_loss_f: ('Contrastive loss weight factor', 'option', None, float) = 2.,
         xent_loss_f: ('Xent loss weight factor', 'option', None, float) = 0.5,
         regr_loss_f: ('Regr loss weight factor', 'option', None, float) = 1.,
         drop_r: ('Dropout', 'option', None, float) = 0.05,
         first_lr_drop: ('', 'option', None, int) = 1,
         second_lr_drop: ('', 'option', None, int) = 6,
         stop_xent: ('', 'option', None, int) = 1000,
         large_model: ('', 'flag', None) = False):
    to.manual_seed(0)
    np.random.seed(0)
    to.cuda.manual_seed(0)
    random.seed(0)
    loglr = np.log10(lr)
    logwd = np.log10(w_decay)
    model_d += f'_e{epochs}_loglr{loglr:.1f}_bs{batch_size}_logwd{logwd:.1f}'

    if os.path.exists(model_d):
        shutil.rmtree(model_d)
    os.makedirs(model_d)

    logger.info(f'Directory used is {model_d}')
    logger.remove()
    train(model_d, epochs, lr, batch_size, w_decay, temp, contr_loss_f, xent_loss_f,
          regr_loss_f, drop_r, first_lr_drop, second_lr_drop, stop_xent, large_model)


if __name__ == '__main__':
    import plac; plac.call(main)
