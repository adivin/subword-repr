""" Will decide on best pron per word using P(C, I| W), input assumed to be sorted by word """
from collections import defaultdict
from math import floor, ceil

import numpy as np
import plac
from loguru import logger
import math as m
from itertools import product, groupby

PROBMASS = 0.95


def get_word_pron(lst_cids, wid, maxlen, fh, word_syms):
    p_ci = [defaultdict(int) for _ in range(maxlen)]
    Z = [0 for _ in range(maxlen)]
    for cids in lst_cids:
        for i, cid in enumerate(cids):
            step_size = maxlen / len(cids)
            if step_size <= 1.:
                idx = floor(i * step_size)
                # print(idx, maxlen)
                p_ci[idx][cid] += 1
                Z[idx] += 1
            else:
                start_idx = floor(i * step_size)
                end_idx = ceil((i+1) * step_size)
                for idx in range(start_idx, end_idx):
                    if idx != maxlen:
                        # print(idx, maxlen)
                        p_ci[idx][cid] += 1
                        Z[idx] += 1
    for i, p_c in enumerate(p_ci):
        for cid, cnt in p_c.items():
            p_c[cid] /= Z[i]

    possible_cids = []
    for i, p_c in enumerate(p_ci):
        cid_probs = [(cid, prob,) for cid, prob in p_c.items()]
        cid_probs_sorted = sorted(cid_probs, key=lambda x: x[1], reverse=True)
        prob_mass = 0.
        j = 0
        cids = []
        while prob_mass < PROBMASS:
            cid, prob = cid_probs_sorted[j]
            prob_mass += prob
            cids.append(cid)
            j += 1
        possible_cids.append(cids)
    final_cids = []
    for cids in possible_cids:
        if len(cids) > 2:
            continue
        final_cids.append(cids)

    paths = []
    for path in product(*final_cids):
        path = tuple([str(k) for k, g in groupby(path)])
        paths.append(path)
    paths = set(paths)
    for path in paths:
        word = word_syms[wid]
        fh.write(f'{word} {" ".join(path)}\n')


def main(inf, word_syms_f, outf):
    logger.warning(f'{inf} should be sorted by word!')
    word_syms = {}
    with open(word_syms_f) as fh:
        for line in fh:
            word, id_ = line.split()
            word_syms[word] = id_
            word_syms[id_] = word

    num_words = int(len(word_syms) / 2)
    fho = open(outf, 'w')
    with open(inf) as fh:
        current_word = None
        current_maxlen = 0
        lines = []
        for line in fh:
            uid, word, *cids = line.split()
            if len(cids) == 0:
                continue
            cids = [int(cid) for cid in cids]
            wid = word_syms[word]
            num_cids = len(cids)
            if num_cids > current_maxlen: current_maxlen = num_cids
            if current_word is not None and current_word != wid:
                get_word_pron(lines, current_word, current_maxlen, fho, word_syms)
                lines = []
                current_maxlen = 0
            lines.append(cids)
            current_word = wid

    logger.info('Done.')
    fho.close()

plac.call(main)