"""
Calculates lexicon entropy.
With W word and C phoneme like entity, \sum_i - P(W_i, C) log ( P(W, C) )
"""
import plac
from collections import defaultdict
import math as m


def calc_clusterent(dct, cid):
    ent = 0.
    cnt_total = sum(cnt for id_, cnt in dct.items() if id_.split('-')[-1] != str(cid))

    for id_, cnt in dct.items():
        if id_.split('-')[-1] != str(cid):
            continue
        ent -= (cnt/cnt_total) * m.log(cnt/cnt_total)
    return ent


def calc_lexent(lines):
    """ Line format is:
            <word> <cluster-id> [<cluster-d>] ..
    """
    dct = defaultdict(int)
    cnt_total = 0
    for line in lines:
        word, *cids = line.split()
        for cid in cids:
            dct[f'{word}-{cid}'] += 1
            cnt_total += 1

    ent = 0.
    for id_, cnt in dct.items():
        prob = cnt/cnt_total
        ent -= prob * m.log(prob)

    return ent


def main(inf):
    lines = []
    with open(inf) as fh:
        for line in fh:
            lines.append(line)
    ent = calc_lexent(lines)
    print(ent)


if __name__ == '__main__':
    plac.call(main)