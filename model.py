import random

import torch as to
import torch.nn as nn
import torch.nn.functional as fu
from loguru import logger
from collections import Counter
import math
import numpy as np
from pytorch_lightning.metrics.metric import Metric

from layers import FactorizedTDNN, OrderedCombineFused, GLULayer, layer_norm, ReluBasic, ConvReluBasic, sig, ConvLayer
from torch_functions import vq_st, custom_onehot, custom_onehotdropped, repr_drop
from soft_dtw_cuda import SoftDTW


class Tdnnf(nn.Module):
    def __init__(self, input_dim, dropout, conv_stride_list=None, time_stride_list=None, normalise_output=True,
                 large_size=1280, small_size=128, use_layernorm=False, padding_list=None, num_speakers=0):
        super().__init__()
        conv_stride_list = [1, 1, 1, 1, 1, 1, 3, 1, 1, 1] if conv_stride_list is None else conv_stride_list
        time_stride_list = [1, 1, 0, 1, 1, 0, 0, 1, 1, 0] if time_stride_list is None else time_stride_list
        padding_list =     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] if padding_list is None else padding_list
        assert len(conv_stride_list) == len(time_stride_list)

        dim = large_size
        self.dim = dim
        self.bottleneck_dim = small_size
        self.dim = dim
        self.num_layers = len(conv_stride_list)
        self.normalise_output = normalise_output
        self.use_layernorm = use_layernorm

        self.tdnn1_affine = nn.Linear(input_dim, dim)
        if not use_layernorm:
            self.bn1 = nn.BatchNorm1d(dim)
        else:
            self.bn1 = nn.LayerNorm(dim, elementwise_affine=False)

        self.do_speaker = False

        tdnnfs = []
        for i in range(self.num_layers):
            layer = FactorizedTDNN(dim, self.bottleneck_dim, time_stride=time_stride_list[i], conv_stride=conv_stride_list[i],
                                   dropout=dropout, padding=padding_list[i], use_layernorm=use_layernorm)
            tdnnfs.append(layer)
        self.tdnnfs = nn.ModuleList(tdnnfs)

        logger.info('Initialised Tdnnf model.')

    def forward(self, x):
        """ x shape is [N, T, C] """
        bs = x.size(0)
        T = x.size(1)
        if not self.use_layernorm:
            x = fu.relu(self.tdnn1_affine(x))
            x = x.permute(0, 2, 1)
            x = self.bn1(x)
        else:
            x = self.bn1(fu.relu(self.tdnn1_affine(x)))
            x = x.permute(0, 2, 1)
        # now [N, C, T]
        for tdnnf in self.tdnnfs:
            x = tdnnf(x)
        h = x.permute(0, 2, 1).contiguous()
        # now [N, T, C]
        return h

    def constrain_orthonormal(self):
        for i in range(len(self.tdnnfs)):
            self.tdnnfs[i].constrain_orthonormal()


class ConvGLU(nn.Module):
    def __init__(self, input_dim, dim, out_dim, dropout, stride=3, use_layernorm=False, add_input=False):
        super().__init__()
        self.kernels = [3, 3, 3]
        self.strides = [1, stride, 1]
        self.affine = nn.Conv1d(input_dim, dim, kernel_size=1)
        self.add_input = add_input
        if add_input:
            self.in_norm = lambda x: layer_norm(x)
        layers = []
        if not add_input:
            for i, (kernel, stride) in enumerate(zip(self.kernels, self.strides)):
                indim = dim // 2 if i != 0 else dim
                layers.append(GLULayer(indim, dim, kernel, stride, dropout, use_layernorm))
        else:
            for i, (kernel, stride) in enumerate(zip(self.kernels, self.strides)):
                layers.append(ConvLayer(dim, dim, kernel, stride, dropout, use_layernorm))

        self.layers = nn.Sequential(
            *layers
        )
        idim = dim // 2 if not add_input else dim
        self.fc_layers = nn.Sequential(ReluBasic(idim, dim, dropout=dropout, use_layernorm=use_layernorm),
                                       ReluBasic(dim, dim, dropout=dropout, use_layernorm=use_layernorm),
                                       ReluBasic(dim, dim, dropout=dropout, use_layernorm=use_layernorm))
        self.final_fc = nn.Linear(dim, out_dim)

        self.norm = nn.BatchNorm1d(out_dim, affine=False) if not use_layernorm else nn.LayerNorm(out_dim, elementwise_affine=False)
        logger.warning('Assuming os context is 5!')
        self.onesided_context = 5

    def forward(self, x):
        """ x shape is [N, C, T] """
        x = self.affine(x)
        if self.add_input:
            x = self.in_norm(x)
            x_save = x.clone()
        x = self.layers(x)
        if self.add_input:
            x = x + x_save[:, :, self.onesided_context:-self.onesided_context:3]

        x = x.permute(0, 2, 1).contiguous()
        # now [N, T, C]
        N = x.size(0)
        T = x.size(1)
        x = x.reshape(N*T, -1)
        x = self.fc_layers(x)
        h = fu.elu(self.final_fc(x))
        h = self.norm(h)

        h = h.reshape(N, T, -1)
        return h


class Encoder(nn.Module):
    def __init__(self, quant_dim, input_dim, repr_dim, dropout, sm_temp, repr_drop, jitter, use_layernorm, use_emb_grad,
                 emb_dim, add_input, use_gs):
        super().__init__()
        self.network = ConvGLU(input_dim, 768, repr_dim, dropout, use_layernorm=use_layernorm, add_input=add_input)

        if use_gs:
            self.quantiser = VectorQuantifierGS(quant_dim, repr_dim, sm_temp, repr_drop, jitter, use_emb_grad=use_emb_grad,
                                                emb_dim=emb_dim)
        else:
            self.quantiser = VectorQuantifier(quant_dim, repr_dim, sm_temp, repr_drop, jitter)

    def forward(self, x):
        h = self.network(x)
        h_quant, h_sm = self.quantiser(h)
        return h_quant, h_sm


class WordWorkerNoOrder(nn.Module):
    def __init__(self, repr_dim):
        super().__init__()
        self.repr_dim = repr_dim

    def forward(self, x, dur_cnts):
        N = x.size(0)
        T = x.size(1)
        durs, counts = dur_cnts
        mask = to.ones((N, T,), dtype=to.bool, device=x.device)
        idx = 0
        for i, (dur, count) in enumerate(zip(durs, counts)):
            mask[idx: idx + count, :dur] = False
            idx += count
        x[mask] = 0.
        joined = x.sum(dim=1)
        #joined = to.zeros((N, self.repr_dim), dtype=to.float32, device=x.device)
        #for i, (dur, count) in enumerate(zip(durs, counts)):
        #    x_part = x[idx: idx + count, :dur]
        #    joined[idx: idx + count] = x_part.sum(dim=1)
        #    idx += count

        joined = 2*to.sigmoid(joined) - 1.
        return joined


class SpecialAttentionLayer(nn.Module):
    def __init__(self, repr_dim):
        super().__init__()
        self.fc_query = nn.Linear(repr_dim, repr_dim*2, bias=False)
        self.fc_key = nn.Linear(repr_dim, repr_dim*2, bias=False)

    def forward(self, q, k, v, dur_cnts):
        N, T, C = q.shape
        durs, counts = dur_cnts
        mask = to.zeros((N, T,), dtype=to.bool, device=q.device)
        idx = 0
        for i, (dur, count) in enumerate(zip(durs, counts)):
            mask[idx: idx + count, :dur] = True
            idx += count
        q = self.fc_query(q)
        k = self.fc_key(k)
        corrs = to.bmm(q, k.permute(0, 2, 1)) / (C**0.5)
        # logger.info(f'{durs[0]} {corrs[0, :3, :3]} {mask[0, :3]}')
        summed_corrs = -(mask.unsqueeze(1) * corrs).sum(dim=2)
        summed_corrs[~mask] = float('-Inf')
        # logger.info(f'{summed_corrs[0, :4]}')
        # For numerical stability
        corrs_maxs, _ = summed_corrs.detach().max(dim=1)
        summed_corrs -= corrs_maxs.unsqueeze(1)

        frame_weights = to.softmax(summed_corrs, dim=1)
        # logger.info(f'{frame_weights[0, :5]}')
        h_word = sig(to.sum(frame_weights.unsqueeze(2) * v, dim=1))
        # logger.info(f'{v[0, :durs[0]]}\n{h_word[0]}')
        return h_word


class WordClassifier(nn.Module):
    def __init__(self, input_dim, output_dim, temp, bce_loss):
        super().__init__()
        if not bce_loss:
            self.loss = nn.CrossEntropyLoss()
        else:
            self.bce_loss = bce_loss
            self.loss = nn.CrossEntropyLoss()
        self.layer = ReluBasic(input_dim, 1024, residual=False)
        self.fc_final = nn.Linear(1024, output_dim)
        self.fc_final.weight.data.fill_(0.)
        self.temp = temp

    def forward(self, x, t, return_correct=False, return_pred=False):
        x = self.layer(x)
        out = self.fc_final(x)
        if hasattr(self, 'bce_loss'):
            pred = to.log_softmax(out, dim=-1)
            loss = -to.mean(to.sum(t * pred, dim=-1))
        else:
            loss = self.loss(out, t)
        if return_pred:
            return loss, out
        elif return_correct:
            correct = (out.argmax(dim=-1) == t).sum()
            return loss, correct
        else:
            return loss


class WordContrastiveLoss(nn.Module):
    def __init__(self, in_dim, contr_dim, num_layers, temp):
        super().__init__()
        self.temp = temp
        self.num_layers = num_layers
        if num_layers == 0:
            self.fc = lambda x: x
        elif num_layers == 1:
            self.fc = nn.Linear(in_dim, contr_dim)
        elif num_layers == 2:
            self.fc = nn.Sequential(ReluBasic(in_dim, contr_dim, residual=False, use_layernorm=True, no_final_ops=True))

    def forward(self, x, labels, norm_loss=True, return_sim=False):
        x = self.fc(x)
        if self.num_layers:
            x_norm = x.norm(dim=1).unsqueeze(1)
        x = x / x_norm
        N = x.size(0)
        labels = labels.view(-1, 1)
        sim = to.mm(x, x.T.detach()) / self.temp
        # logger.info(f'{labels}\nsim {sim}')
        # sim.retain_grad()
        diag_mask = (to.ones(N) - to.eye(N)).to(x.device)

        # for numerical stability
        sim_maxs, _ = to.max(sim, dim=1, keepdim=True)
        sim = sim - sim_maxs.detach()
        sim = to.exp(sim) * diag_mask

        mask = to.eq(labels, labels.T).float().to(x.device)
        mask = diag_mask * mask

        den = sim.sum(dim=1)
        probs = (sim * mask).sum(dim=1) / den
        #logger.info(f'probs {probs}')
        #probs.retain_grad()
        if norm_loss:
            log_probs = -to.log(probs + 1e-8) / (mask.sum(dim=1) + 1)
        else:
            log_probs = -to.log(probs + 1e-8)
        #log_probs.retain_grad()
        cnt = len(to.unique(labels))
        if not return_sim:
            return log_probs.sum() / cnt
        else:
            return log_probs.sum() / cnt, sim.detach()


class VectorQuantifierGS(nn.Module):
    """ Gumbel Softmax """
    def __init__(self, quant_dim, in_dim, temp=1., repr_drop=0., jitter=0., threshold=False, use_emb_grad=False,
                 emb_dim=128):
        super().__init__()
        self.linear = nn.Linear(in_dim, quant_dim, bias=False)
        self.temp = temp
        self.quant_dim = quant_dim
        self.emb_dim = emb_dim
        self.emb = nn.Parameter(to.randn(quant_dim, emb_dim), requires_grad=use_emb_grad)
        nn.init.orthogonal_(self.emb.data)
        self.emb.data = self.emb.data / self.emb.data.norm(p=2, dim=1, keepdim=True)
        assert not (repr_drop and jitter)
        if repr_drop:
            self.repr_drop = ReprDrop(repr_drop)
        else:
            self.repr_drop = None

        self.jitter = jitter
        self.threshold = threshold
        if jitter:
            self.jitter_layer = JitterLayer(jitter)
        else:
            self.jitter_layer = None
        assert not (repr_drop and jitter)

    def forward(self, x):
        """ x has shape (N, T, C) """
        N = x.size(0)
        T = x.size(1)
        x = self.linear(x)
        if self.training:
            out = fu.gumbel_softmax(x, tau=self.temp, hard=True, dim=-1)
            x = to.softmax(x, dim=-1)
            #indcs = out.argmax(dim=2).view(N*T)
            #counter = Counter(indcs.tolist())
            #logger.info(counter)
            #if self.repr_drop is not None:
            ##    out = self.repr_drop(out)
            if self.jitter:
                out = self.jitter_layer(out)
            out = out.matmul(self.emb)
            out = out / (out.norm(p=2, keepdim=True, dim=-1) + 1e-10)

            return out, x
        else:
            x = to.softmax(x, dim=-1)
            named_tpl = to.max(x, dim=2)
            indcs = named_tpl.indices
            maxs = named_tpl.values

            out = to.index_select(self.emb.data, 0, indcs.view(-1)).reshape(N, T, self.emb_dim)
            # out = fu.one_hot(indcs, self.quant_dim).reshape(N, T, self.quant_dim).float()
            if self.threshold:
                mask = maxs > 0.5
                out *= mask.unsqueeze(2)
            return out, x


class VectorQuantifier(nn.Module):
    """ Gumbel Softmax """
    def __init__(self, quant_dim, in_dim, temp=1., repr_drop=0., jitter=0., threshold=False):
        super().__init__()
        self.linear = nn.Linear(in_dim, quant_dim, bias=False)
        self.temp = temp
        self.quant_dim = quant_dim
        assert not (repr_drop and jitter)
        if repr_drop:
            self.repr_drop = ReprDrop(repr_drop)
        else:
            self.repr_drop = None

        self.jitter = jitter
        self.threshold = threshold
        if jitter:
            self.jitter_layer = JitterLayer(jitter)
        else:
            self.jitter_layer = None
        assert not (repr_drop and jitter)

    def forward(self, x):
        """ x has shape (N, T, C) """
        N = x.size(0)
        T = x.size(1)
        x = self.linear(x)
        if self.training:
            out = to.softmax(x / self.temp, dim=-1)
            #indcs = out.argmax(dim=2).view(N*T)
            #counter = Counter(indcs.tolist())
            #logger.info(counter)
            #if self.repr_drop is not None:
            ##    out = self.repr_drop(out)
            if self.jitter:
                out = self.jitter_layer(out)

            return out, out
        else:
            x = to.softmax(x, dim=-1)
            named_tpl = to.max(x, dim=2)
            indcs = named_tpl.indices
            maxs = named_tpl.values

            out = fu.one_hot(indcs, self.quant_dim).reshape(N, T, self.quant_dim).float()
            if self.threshold:
                mask = maxs > 0.5
                out *= mask.unsqueeze(2)
            return out, out


class JitterLayer(nn.Module):
    def __init__(self, replace_prob):
        super(JitterLayer, self).__init__()
        self.replace_prob = replace_prob

    def forward(self, x):
        """ x shape should be (N, T, C,) """
        if not self.training:
            return x
        time_steps = x.size(1)
        bs = x.size(0)
        channels = x.size(2)
        replace_prob = self.replace_prob
        k = np.random.choice([-1, 0, 1], size=(bs, time_steps-2,), p=[replace_prob/2, 1 - replace_prob, replace_prob/2])
        # k = to.multinomial([])
        # maybe should try linear interpolation
        indices = to.zeros(bs, time_steps)
        # indices[:, -1] = time_steps - 1
        indices[:, 1:time_steps-1] = to.as_tensor(k)

        indices = indices.to(device=x.device, dtype=to.long).view(-1)
        indices += to.arange(bs * time_steps, device=x.device)
        x = to.index_select(x.view(bs*time_steps, channels,), 0, indices)
        x = x.reshape(bs, time_steps, channels).contiguous()
        return x


class ReprDrop(nn.Module):
    def __init__(self, drop_perc):
        super().__init__()
        self.drop_perc = drop_perc

    def forward(self, x):
        if not self.training:
            return x
        return repr_drop(x, self.drop_perc)


class RegressionWorker(nn.Module):
    def __init__(self, dim_in, dim_out):
        super().__init__()
        self.fc_layers = nn.Sequential(ReluBasic(dim_in, dim_out, residual=False))
        self.criterion = nn.MSELoss(reduction='mean')

    def forward(self, x, f_targ):
        x = x.view(-1, x.size(2))
        f_targ = f_targ.view(-1, f_targ.size(2))
        f_hyp = self.fc_layers(x)
        return self.criterion(f_hyp, f_targ)


class DTWLoss(nn.Module):
    def __init__(self, use_cuda, gamma, cosine_temp, use_orig, contr_temp, neg_margin=0.):
        super().__init__()
        self.loss = SoftDTW(use_cuda=use_cuda, normalize=False, gamma=gamma, temp=cosine_temp)
        self.use_orig = use_orig
        self.contr_temp = contr_temp
        self.neg_margin = neg_margin

    def forward(self, x, x_hat, t, t_negs, x_lens):
        to.set_printoptions(sci_mode=False, linewidth=100000000000000)
        N = x.size(0)
        T = x.size(1)
        t = t.unsqueeze(0)
        diag_mask = (to.ones(N) - to.eye(N)).bool().to(x.device)
        mask = to.eq(t, t.T).bool().to(x.device) * diag_mask
        pos_mask = to.tril(mask)

        if self.use_orig:
            indcs_pos_left, indcs_pos_right = to.where(pos_mask)
            num_comparisons = indcs_pos_left.numel()

            t = t.squeeze(0)
            neg_indcs = t_negs[indcs_pos_left]
            indcs_pos_left_expanded = indcs_pos_left.repeat_interleave(t_negs.size(1), dim=0)

            left_indcs = to.cat((indcs_pos_left, indcs_pos_left_expanded), dim=0)
            right_indcs = to.cat((indcs_pos_right, neg_indcs.view(-1)))

            x_l = x[left_indcs]
            x_r = x_hat[right_indcs]
            l_lens = x_lens[left_indcs]
            r_lens = x_lens[right_indcs]

            dists_flat = self.loss(x_l, x_r, l_lens, r_lens)
            pos_dists = dists_flat[:num_comparisons]
            neg_dists = -dists_flat[num_comparisons:].clamp(max=self.neg_margin) + self.neg_margin

            logits = to.cat((-pos_dists.view(num_comparisons, 1), neg_dists.view(num_comparisons, t_negs.size(1))-self.neg_margin), dim=1)
            # sm = fu.softmax(logits.detach(), dim=1)
            # outmaxs = sm.argmax(dim=1)
            # indcs = to.where(outmaxs != 0)[0]
            # negsims = logits[indcs, outmaxs[indcs]]
            # maxsingle = outmaxs[indcs]-1
            # tmpindcs = neg_indcs[indcs, maxsingle]
            # neg_maxs = t[tmpindcs]
            # failed = to.stack((t[indcs_pos_left[indcs]], t[indcs_pos_right[indcs]], neg_maxs, x_lens[indcs_pos_left[indcs]], x_lens[indcs_pos_right[indcs]], x_lens[tmpindcs], -pos_dists[indcs], negsims), dim=1)
            # logger.info(f'failed {indcs.size(0)} {failed} ')


            cost = pos_dists.mean() + neg_dists.mean()
            return cost
        else:

            # dur_mask = (x_lens.unsqueeze(0) - x_lens.unsqueeze(1)).abs()
            # dur_mask = (dur_mask > 3) + (dur_mask == 0)
            indcs_pos_left, indcs_pos_right = to.where(pos_mask)
            num_comparisons = indcs_pos_left.numel()

            t = t.squeeze(0)
            neg_indcs = t_negs[indcs_pos_left]
            indcs_pos_left_expanded = indcs_pos_left.repeat_interleave(t_negs.size(1), dim=0)

            left_indcs = to.cat((indcs_pos_left, indcs_pos_left_expanded), dim=0)
            right_indcs = to.cat((indcs_pos_right, neg_indcs.view(-1)))

            x_l = x[left_indcs]
            x_r = x_hat[right_indcs]
            l_lens = x_lens[left_indcs]
            r_lens = x_lens[right_indcs]
            dists_flat = self.loss(x_l, x_r, l_lens, r_lens)

            pos_logits = -dists_flat[:num_comparisons]
            neg_logits = -dists_flat[num_comparisons:] - self.neg_margin
            logits = to.cat((pos_logits.view(num_comparisons, 1), neg_logits.view(num_comparisons, t_negs.size(1))), dim=1)
            # sm = fu.softmax(logits.detach(), dim=1)
            # outmaxs = sm.argmax(dim=1)
            # indcs = to.where(outmaxs != 0)[0]
            # negsims = logits[indcs, outmaxs[indcs]]
            # maxsingle = outmaxs[indcs]-1
            # tmpindcs = neg_indcs[indcs, maxsingle]
            # neg_maxs = t[tmpindcs]
            # failed = to.stack((indcs_pos_left[indcs], indcs_pos_right[indcs], t[indcs_pos_left[indcs]], t[indcs_pos_right[indcs]], neg_maxs, x_lens[indcs_pos_left[indcs]], x_lens[indcs_pos_right[indcs]], x_lens[tmpindcs], pos_logits[indcs], negsims), dim=1)
            # logger.info(f'failed {indcs.size(0)} {failed} ')
            tmp_targets = to.zeros(num_comparisons, dtype=to.long, device=x.device)
            #return (fu.cross_entropy(logits, tmp_targets, reduction='none') * max_lens[:num_comparisons]**0.5).mean()
            return fu.cross_entropy(logits, tmp_targets)# - pos_logits.mean()


class Accuracies(Metric):
    def __init__(self, num_labels, dist_sync_on_step=False):
        super().__init__(dist_sync_on_step=dist_sync_on_step)
        self.add_state(f'correct', default=to.zeros(num_labels, dtype=to.int32), dist_reduce_fx='sum')
        self.add_state(f'total', default=to.zeros(num_labels, dtype=to.int32), dist_reduce_fx='sum')
        self.num_labels = num_labels

    def update(self, preds, targets):
        assert preds.shape == targets.shape
        for i in range(self.num_labels):
            mask = targets == i
            if mask.sum() > 0:
                self.correct[i] += to.sum(preds[mask] == targets[mask])
                self.total[i] += mask.sum()

    def compute(self):
        recalls = to.zeros(self.num_labels)
        for i in range(self.num_labels):
            if self.total[i] > 0:
                recalls[i] = self.correct[i].float() / self.total[i]
            else:
                recalls[i] = 0.5
        return recalls
