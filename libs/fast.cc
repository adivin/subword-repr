#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <math.h>
#include <vector>
#include <omp.h>
#include <tuple>
#include <unordered_map>
#include <iostream>
#include <queue>


namespace py = pybind11;

typedef unsigned long long longint;

const int D = 256;

float cosine_distance(float A[256], float B[256]) {
	float mul = 0.f, m_a = 0.f, m_b = 0.f;

	for (int i = 0; i < D; i++) {
		float vala = A[i];
		float valb = B[i];
		mul += vala * valb;
		m_a += vala * vala;
		m_b += valb * valb;
	}
	return 1 - mul / sqrt(m_a * m_b);
}

float euclidean_distance(float A[256], float B[256]) {
	float sum = 0.;
	for (int i = 0; i < D; i++) {
		float d = A[i] - B[i];
		sum += d * d;
	}
	return sqrt(sum);
}


double bhatt_distance(const float* A, const float* B, float Am, float Bm, const int N) {
    double sum = 0.;
    #pragma omp simd reduction(+:sum)
    for (int i = 0; i < N; i++) {
        sum += sqrt((A[i]/Am) * (B[i]/Bm));
    }
    return -log((sum + 0.0000000001));
}

float l_distance(const float* A, const float* B, float Am, float Bm, const int N) {
    float sum = 0.;
    for (int i = 0; i < N; i++) {
        sum += abs(A[i]/Am - B[i]/Bm);
    }
    return sum;
}

longint calc_row_idx(longint k, longint n) {
    auto kd = static_cast<double>(k);
    auto nd = static_cast<double>(n);
    return longint(ceil((1 / 2.) * (- pow((-8 * kd + 4 * pow(nd, 2) - 4 * nd - 7), 0.5) + 2 * nd - 1) - 1));
}

longint elem_in_i_rows(longint i, longint n) {
    return i * (n - 1 - i) + (i * (i + 1))/2;
}

longint calc_col_idx(longint k, longint i, longint n) {
    return n - elem_in_i_rows(i + 1, n) + k;
}

std::pair<int, int> condensed_to_square_idx(longint k, longint n) {
    // who knew so complicated???
    // https://stackoverflow.com/questions/13079563/how-does-condensed-distance-matrix-work-pdist
    longint i = (calc_row_idx(k, n));
    longint j = (calc_col_idx(k, i, n));
    int x = (longint) i;
    int y = (longint) j;
    return std::make_pair(x, y);
}

longint sumn(longint n) {
    return n*(n+1)/2;
}

longint convert_to_condensed_idx(longint i, longint j, longint n) {
    if (i > j) std::swap(i, j);
    return sumn(n-1) - sumn(n-i-1) + j - i - 1;
}

class DistMatrix {
public:
    longint size_;
    std::vector<float> dists;
    DistMatrix() {}

    void resize(longint n) {
        size_ = n;
        dists.resize(sumn(n-1), 0.);
    }

    void set(longint i, longint j, float dist) {
        if (i >= j) {
            throw std::runtime_error("Should not happen!");
        }
        longint idx = convert_to_condensed_idx(i, j, size_);
        dists[idx] = dist;
    }

    float get(std::pair<int, int> idx_pair) {
        longint idx = convert_to_condensed_idx(static_cast<longint>(idx_pair.first), static_cast<longint>(idx_pair.second), size_);
        return dists[idx];
    }

    std::vector<std::pair<int, int>> get_smallest_indcs(int k) {
        std::vector<float> k_smallest(k, 100.f);
        std::vector<longint> k_indcs(k);
        for (longint i = 0; i < dists.size(); ++i) {
            float dist = dists[i];
            if (dist < k_smallest.back()) {
                auto it = std::lower_bound(k_smallest.begin(), k_smallest.end(), dist);
                int offset = it - k_smallest.begin();
                k_smallest.insert(it, dist);
                k_indcs[offset] = i;
                k_smallest.pop_back();
            }
        }
        std::vector<std::pair<int, int>> square_indcs;
        for(auto idx: k_indcs) {
            square_indcs.push_back(condensed_to_square_idx(idx, size_));
        }
        return square_indcs;
    }
};

class ClusterAlg {
public:
    std::vector<float> p_wi_c_;
    DistMatrix distmat_;
    std::unordered_map<int, std::vector<int> > cid_groupings_;  // keeps track of merges
    std::unordered_map<int, int> cid_to_int_;
    std::unordered_map<int, int> int_to_cid_;
    const int N_;
    int num_words_;

    ClusterAlg(const py::dict p_wi_c, const int num_words, const int N): N_(N) {
        num_words_ = num_words;
        longint total_size = (longint) p_wi_c.size() * num_words * N;
        p_wi_c_.resize(total_size);
        int i = 0;
        for (auto item: p_wi_c) {
            int cid = py::cast<int>(item.first);
            std::vector<int> v;
            v.push_back(cid);
            cid_groupings_[cid] = v;
            cid_to_int_[cid] = i;
            int_to_cid_[i] = cid;
            py::array_t<float> x = py::cast<py::array_t<float>>(item.second);
            if (x.size() != num_words_ * N) {
                throw std::runtime_error("Sizes do not match! " + std::to_string(x.size()) + " " + std::to_string(num_words*N));
            }
            std::memcpy(p_wi_c_.data() + (longint) i * num_words_ * N, x.data(), x.size()*4);
            i++;
        }

        std::cerr << "Done initialising P(W, I | C)\n";
    }

    void run_comparisons() {
        std::cerr << "Running comparisons.\n";
        longint num_clusts = cid_to_int_.size();
        distmat_.resize(num_clusts);

        longint word_prob_len = num_words_ * N_;
        std::vector<float> magnitudes(num_clusts);
        for (longint i = 0; i < num_clusts; i++) {
            longint offset = i * word_prob_len;
            float magnitude = 0.;
            for (longint j = 0; j < word_prob_len; j++) {
                magnitude += p_wi_c_[offset + j];
            }
            magnitudes[i] = magnitude;
        }
        // https://stackoverflow.com/questions/10850155/whats-the-difference-between-static-and-dynamic-schedule-in-openmp
        #pragma omp parallel for schedule(dynamic)
        for (longint i = 0; i < num_clusts; i++) {
            longint a_offset = i * word_prob_len;
            for (longint j = i+1; j < num_clusts; j++) {
                if (i == j) continue;

                longint b_offset = j * word_prob_len;
                float dist = bhatt_distance(p_wi_c_.data() + a_offset, p_wi_c_.data() + b_offset, magnitudes[i], magnitudes[j], num_words_ * N_);
                distmat_.set(i, j, dist);
            }

        }
        std::cerr << "Done running comparisons\n";
    }

    int merge_clusters(int c1_i, int c2_i) {
        if ((int_to_cid_.find(c1_i) == int_to_cid_.end()) || (int_to_cid_.find(c2_i) == int_to_cid_.end())) {
            return 0;
        }

        longint word_prob_len = num_words_ * N_;
        longint offset_1 = (longint) c1_i * word_prob_len;
        longint offset_2 = (longint) c2_i * word_prob_len;

        for (longint i = 0; i < word_prob_len; i++) {
            p_wi_c_[offset_1 + i] += p_wi_c_[offset_2 + i];
        }

        int cid1 = int_to_cid_[c1_i];
        int cid2 = int_to_cid_[c2_i];

        std::vector<int>& v = cid_groupings_[cid1];
        std::vector<int>& other_v = cid_groupings_[cid2];
        for (size_t i = 0; i < other_v.size(); i++) {
            int cid = other_v[i];
            v.push_back(cid);
        }
        cid_groupings_.erase(cid2);

        int_to_cid_.erase(c2_i);
        cid_to_int_.erase(cid2);
        return 1;
    }

    void shift_and_resize_vector(std::vector<int>& i_to_remove) {
        longint num_removed = 0;
        longint word_prob_len = num_words_ * N_;
        longint startidx = i_to_remove[0] * word_prob_len;
        longint offset = 0;
        longint write_idx;
        longint tot_size = p_wi_c_.size();
        for (write_idx = startidx; write_idx + offset < tot_size; ++write_idx) {

            while (num_removed < i_to_remove.size() && write_idx + offset == (longint) i_to_remove[num_removed] * word_prob_len) {
                num_removed++;
                offset = num_removed * word_prob_len;
            }

            if (write_idx + offset >= tot_size) break;

            if (write_idx % word_prob_len == 0) {
                int original_i = static_cast<int>((write_idx + offset) / word_prob_len);
                int cid = int_to_cid_[original_i];
                int_to_cid_.erase(original_i);
                int new_i = static_cast<int>((write_idx) / word_prob_len);
                int_to_cid_[new_i] = cid;
                cid_to_int_[cid] = new_i;
            }

            p_wi_c_[write_idx] = p_wi_c_[write_idx + offset];
        }
        if (cid_to_int_.size() != (write_idx / word_prob_len) || num_removed != i_to_remove.size()) {
            throw std::runtime_error("Bad sizes " + std::to_string(cid_to_int_.size()) + " " + \
                std::to_string(write_idx) + " " + std::to_string(word_prob_len));
        }
        p_wi_c_.resize(write_idx);
    }

    void run_merging(int to_merge) {
        int k = to_merge;
        auto k_indcs = distmat_.get_smallest_indcs(k);
        std::vector<int> i_to_remove;
        int cnt = 0;
        std::cerr << "Aiming to merge "<< to_merge<<" clusters\n";
        for (size_t i = 0; i < k_indcs.size(); i++) {
            auto idx_pair = k_indcs[i];
            int res = merge_clusters(idx_pair.first, idx_pair.second);
            if (res != 0) {
                i_to_remove.push_back(idx_pair.second);
            }
            cnt += res;
        }

        std::sort(i_to_remove.begin(),i_to_remove.end() );
        float smallest_dist = distmat_.get(k_indcs[0]);
        float largest_dist = distmat_.get(k_indcs.back());
        std::cerr << "Merged "<<cnt<<" clusters, smallest dist was "<<smallest_dist<<" largest "<<largest_dist<<"\n";
        shift_and_resize_vector(i_to_remove);
        std::cerr << "Resized vec\n";

        if (cid_to_int_.size() != int_to_cid_.size()) {
            throw std::runtime_error("Sizes should be the same of maps.");
        }
    }

    std::unordered_map<int, std::vector<int> > run(unsigned int max_cluster_num) {
        int num_iterations = 0;
        while (cid_to_int_.size() > max_cluster_num) {
            run_comparisons();

            int diff = cid_to_int_.size() - max_cluster_num;
            int to_merge = int(pow(float(diff), 0.77));
            if (diff > 30000) {
                to_merge = int(pow(float(diff), 0.93));
            }
            if (to_merge < 50) to_merge = 50;
            run_merging(to_merge);

            num_iterations++;
        }
        std::cerr << "Done, did "<<num_iterations<<" iterations, num clusts: " << cid_groupings_.size() <<"\n";
        return cid_groupings_;
    }
};

py::array_t<float> pdist(py::array_t<float> x) {
	if (x.ndim() != 2) {
		throw std::runtime_error("Input should be 2-D NumPy array");
	}

	longint N = x.shape()[0];
	if (x.shape()[1] != D) {
		throw std::runtime_error("Dimensionality of feature vector not right!");
	}

	auto buf = x.request();
	float* ptr = (float*) buf.ptr;

	longint dist_array_size = sumn(N - 1);
	float* dist_ptr = new float[dist_array_size]();

	int parallelism_enabled = 1;
	if (N < 1000) {
		parallelism_enabled = 0;
	}

	#pragma omp parallel for if (parallelism_enabled)
	for (longint i = 0; i < N; i++) {
		for (longint j = i + 1; j < N; j++) {
			float dist = euclidean_distance(ptr + i*D, ptr + j*D);
			longint idx = convert_to_condensed_idx(i, j, N);
			if (dist < 0.f) dist = 0.f;
			dist_ptr[idx] = dist;
		}
	}

	py::array_t<float> dist_array = py::array_t<float>({dist_array_size}, {sizeof(float)}, dist_ptr);
	return dist_array;
}


py::array_t<int> cross_distance(py::array_t<float> data, py::array_t<float> clust) {
	if (data.ndim() != 2) {
		throw std::runtime_error("Data should be 2D");
	}
	int N = data.shape()[0];
	int D = data.shape()[1];
	if (D != clust.shape()[1]) {
		throw std::runtime_error("Arguments should have same inner dimension.");
	}
	int M = clust.shape()[0];

	auto buf = data.request();
	float* ptr = (float*) buf.ptr;

	auto bufclust = clust.request();
	float* clustptr = (float*) bufclust.ptr;

	int* indcs = new int[N];

	int parallelism_enabled = 1;
	if (N < 100000) {
		parallelism_enabled = 0;
	}

	#pragma omp parallel for if (parallelism_enabled)
	for (int i = 0; i < N; i++) {
		float minval = 1000000.f;
		int idx = -1;
		for (int j = 0; j < M; j++) {
			float sum = 0.;
			for (int k = 0; k < D; k++) {
				sum += abs(ptr[i*D + k] - clustptr[j*D + k]);
			}
			if (sum < minval) {
				minval = sum;
				idx = j;
			}
		}
		indcs[i] = idx;
	}

	py::array_t<int> indcs_array = py::array_t<int>({N}, {sizeof(int)}, indcs);
	return indcs_array;
}


PYBIND11_MODULE(fast, m) {
	m.doc() = "Calculate pdist";
	m.def("pdist", &pdist, py::return_value_policy::reference);

	m.def("cross_distance", &cross_distance);

	py::class_<ClusterAlg>(m, "ClusterAlg")
	    .def(py::init<py::dict, int, int>())
	    .def("run", &ClusterAlg::run);

}