import fst


def create_w(w, size):
    return [w/size for _ in range(size)]


def main():
    f = fst.Fst()
    f.add_linear_sequence([1, 2, 3, 4], create_w(0.2, 4))
    f.add_linear_sequence([1, 2, 5, 4], create_w(0.2, 4))
    f.add_linear_sequence([1, 2, 6, 5, 4], create_w(0.6, 5))
    # f.add_linear_sequence([2, 2, 7, 8], create_w(0.25, 4))
    # f.add_linear_sequence([2, 2, 9, 4], create_w(0.25, 4))
    print(f.get_paths())
    f.determinize()
    a = [1,2,3]
    print(len(a))
    print()
    print(f.get_paths())
    print()
    f.reverse_determinize()
    print(f.get_paths())

main()
