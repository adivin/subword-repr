#include <vector>
#include <string>
#include <unordered_map>
#include <tuple>
#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

namespace py = pybind11;

class State;

class Arc {
public:
  int symbol;
  float weight;
  State *state;

  Arc() {}

  Arc(int sym, float w, State *s) {
    symbol = sym;
    weight = w;
    state = s;
  }
};


class State {
public:
  int id;
  std::vector <Arc> arcs;

  State() {}

  State(int i) { id = i; }

//	~State() {
//		for (int i = 0; i < arcs.size(); i++) {
//			delete arcs[i];
//		}
//	}

  void add_arc(int sym, float w, State *s) {
#if DEBUG == 1
    std::cout <<"adding arc " << sym<<std::endl;
#endif
    Arc arc(sym, w, s);
    arcs.push_back(arc);
  }

  void add_arc(Arc arc) {
    arcs.push_back(arc);
  }
};

int get_previous_state_id(std::vector <std::tuple<int, float, int, int>> paths, int idx, int next_state_id) {
  if (std::get<2>(paths[idx]) != next_state_id) {
    throw std::runtime_error("Begin state does not have right state id!");
  }
  for (size_t i = idx; i >= 0; i--) {
    auto tpl = paths[i];
    if (std::get<3>(tpl) == next_state_id) {
      return std::get<2>(tpl);
    }
  }
  return -1;
}

class Fst {
public:
  std::unordered_map<int, State *> state_map;
  State *start;
  State *end;

  Fst() {
    State *s = new State(0);
    start = s;
    s = new State(1);
    end = s;
    state_map[0] = start;
    state_map[1] = end;
  }

  ~Fst() {
    for (auto kv: state_map) {
      delete kv.second;
    }
  }

  int add_state() {
    int i = 2;
    auto res = state_map.find(i);
    while (res != state_map.end()) {
      i++;
      res = state_map.find(i);
    }
    State *state = new State(i);
    state_map[i] = state;
    return i;
  }

  void add_linear_sequence(std::vector<int> syms, std::vector<float> weights) {
    State *lasts = NULL;
    for (size_t i = 0; i < syms.size(); i++) {
      if (i == 0) {
        const int sid = add_state();
        State *s = state_map.at(sid);
        start->add_arc(syms[i], weights[i], s);
        lasts = s;
      } else if (i != syms.size() - 1) {
        const int sid = add_state();
        State *s = state_map.at(sid);
        lasts->add_arc(syms[i], weights[i], s);
        lasts = s;
      } else {
        lasts->add_arc(syms[i], weights[i], end);
      }
    }
#if DEBUG == 1
    std::cout << "done add"<<std::endl;
#endif
  }

  void stepdown_arc(State *s, std::vector <std::tuple<int, float, int, int>> *path) {
#if DEBUG == 1
    std::cout <<"numarcs "<< s->arcs.size()<<std::endl;
#endif
    for (Arc arc: s->arcs) {
#if DEBUG == 1
      std::cout << "symbol "<<arc.symbol <<std::endl;
#endif
      path->push_back(std::make_tuple(arc.symbol, arc.weight, s->id, arc.state->id));
      if (arc.state != end) {
        stepdown_arc(arc.state, path);
      }
    }
  }

  std::vector <std::tuple<int, float, int, int>> get_paths() {
    std::vector <std::tuple<int, float, int, int>> path;
    stepdown_arc(start, &path);
    return path;
  }

  void determinize_states(State *state) {
    if (state == end) return;
    std::vector <Arc> arcs = state->arcs;
    size_t num_arcs = arcs.size();
    std::vector<int> removed_arcs;
    std::vector <Arc> kept_arcs;
    for (size_t i = 0; i < num_arcs; i++) {
      if (std::find(removed_arcs.begin(), removed_arcs.end(), i) != removed_arcs.end()) {
        continue;
      }
      std::vector<int> indcs_to_collapse;
      for (size_t j = i + 1; j < num_arcs; j++) {
        if (arcs[i].symbol == arcs[j].symbol) {
          indcs_to_collapse.push_back(j);
        }
      }
      Arc &arc = arcs[i];
      for (int idx: indcs_to_collapse) {
        removed_arcs.push_back(idx);
        Arc &arc_to_remove = arcs[idx];
        arc.weight += arc_to_remove.weight;
        for (Arc child_arcs: arc_to_remove.state->arcs) {
          arc.state->add_arc(child_arcs);
        }
        int id = arc_to_remove.state->id;
        delete arc_to_remove.state;
        state_map.erase(id);
      }
      kept_arcs.push_back(arcs[i]);
    }
    state->arcs = kept_arcs;
    for (Arc arc: state->arcs) {
      determinize_states(arc.state);
    }
  }

  std::vector<int> reverse_determinize_stateid(int state_id_to_check) {
    // Should be forward determinized already!
    std::vector <std::tuple<int, float, int, int>> paths = get_paths();
    std::vector <std::tuple<int, float, int, int>> path_finals;
    std::vector<int> indcs;
    std::cout << "ID to check " << state_id_to_check<<std::endl;
    for (size_t i = 0; i < paths.size(); i++) {
      int state_id = std::get<3>(paths[i]);
      if (state_id == state_id_to_check) {  // goes to state id
        path_finals.push_back(paths[i]);
        indcs.push_back(i);
      }
    }
    std::vector<int> removed_ids;
    std::vector<int> states_to_check;
    for (size_t i = 0; i < path_finals.size(); i++) {
      auto tpl = path_finals[i];
      int state_id_keep = std::get<2>(tpl);
      if (std::find(removed_ids.begin(), removed_ids.end(), state_id_keep) != removed_ids.end()) {
        continue;
      }
      State* state_to_keep = state_map[state_id_keep];
      Arc* main_arc = NULL;
      int arc_symbol = std::get<0>(tpl);
      for (size_t k = 0; k < state_to_keep->arcs.size(); k++) {
        if (state_to_keep->arcs[k].symbol == arc_symbol) {
          main_arc = &(state_to_keep->arcs[k]);
          break;
        }
      }
      if (main_arc == NULL) {
        throw std::runtime_error("SOMETHING'S WRONG");
      }

      for (size_t j = i + 1; j < path_finals.size(); j++) {
        auto other_tpl = path_finals[j];
        int other_arc_symbol = std::get<0>(other_tpl);
        std::cout << " keepid " << state_id_keep<<" otherid "<<std::get<2>(other_tpl)<<std::endl;
        if (arc_symbol == other_arc_symbol) {  // same symbol
          int state_id_remove = std::get<2>(other_tpl);
          main_arc->weight += std::get<1>(other_tpl);

          int prev_state_id = get_previous_state_id(paths, indcs[j], state_id_remove);
          std::cout << "prevstateid " << prev_state_id<<" statekeep id " << state_id_keep << " remove "<< state_id_remove<<std::endl;
          State* prev_state = state_map[prev_state_id];
          for (size_t l = 0; l < prev_state->arcs.size(); l++) {
            if (prev_state->arcs[l].state->id == state_id_remove) {
              prev_state->arcs[l].state = state_to_keep;
              std::cout<<"in "<<state_to_keep->id<<" "<<prev_state->arcs[l].state->id<< std::endl;
              break;
            }
          }
          delete state_map[state_id_remove];
          state_map.erase(state_id_remove);
          removed_ids.push_back(state_id_remove);
          if (std::find(states_to_check.begin(), states_to_check.end(), state_id_keep) == states_to_check.end()) {
            std::cout << "in\n";
            states_to_check.push_back(state_id_keep);
          }
        }
      }
    }
    return states_to_check;
  }

  void reverse_determinize() {
    std::vector<int> to_check = reverse_determinize_stateid(1);
    while (to_check.size() > 0) {
      std::vector<int> to_check_next;
      for (int id : to_check) {
        std::vector<int> ids = reverse_determinize_stateid(id);
        to_check_next.insert(to_check_next.begin(), ids.begin(), ids.end());
      }
      to_check = to_check_next;
    }
  }

  void determinize() {
    determinize_states(start);
  }
};

PYBIND11_MODULE(fst, m) {
  m.doc() = "Fst for prons.";

  py::class_<Fst>(m,"Fst")
    .def(py::init<>())
    .def("add_linear_sequence", &Fst::add_linear_sequence)
    .def("get_paths", &Fst::get_paths)
    .def("determinize", &Fst::determinize)
    .def("reverse_determinize", &Fst::reverse_determinize);
}