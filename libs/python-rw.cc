#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include "util/common-utils.h"
#include "matrix/kaldi-matrix.h"
#include "base/kaldi-common.h"
#include <iostream>
#include <tuple>

namespace py=pybind11;
using namespace kaldi;

class Pykread {
public:
  Pykread(const std::string fpath): obj(fpath) {
    ptr = nullptr;
    key = ""; 
  }

  std::tuple<std::string, py::array_t<float>> get() {
    featmat = &(obj.Value());
    int32_t numrows = featmat->NumRows(), numcols = featmat->NumCols();
    ptr = featmat->Data();
    py::array_t<float> m = py::array_t<float>({numrows, numcols}, {numcols*4, 4}, ptr);
    key = obj.Key();
    return std::make_tuple(key, m); 
  }

  bool next() {
    if (!obj.Done()) {
      obj.Next();
      return true;
    }   
    return false;
  }

  bool close() {
    return obj.Close();
  }

  float* ptr;
  std::string key;
  SequentialBaseFloatMatrixReader obj;
  Matrix<BaseFloat>* featmat;
  bool isdone;
};

PYBIND11_MODULE(python_rw, m) {
  m.doc() = "pybind11 stuff";

  py::class_<Pykread>(m, "Pykread")
      .def(py::init<std::string>())
      .def("get", &Pykread::get, py::return_value_policy::reference)
      .def("next", &Pykread::next)
      .def("close", &Pykread::close);
}