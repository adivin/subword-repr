import numpy as np
from libKMCUDA import kmeans_cuda
# from matplotlib import pyplot
from loguru import logger
import pickle

from ahc import Cluster
import os
from glob import glob


REPRDIM = 256

def norm(x):
    return (x.T / (np.linalg.norm(x, axis=1))).T


def kmeans(data, targ_clusters, f, offset, lst_coverage=None, do_yinyang=True):
    logger.info(f'Starting kmeans, data shape is {data.shape}, clustering down to {targ_clusters}')
    tolerance = 0.0001
    if do_yinyang:
        yinyang_f = 0.05
    else:
        yinyang_f = 0.0
    data = norm(data)

    centroids, assignments, avg_distance = kmeans_cuda(data, targ_clusters,
        verbosity = 1, yinyang_t = yinyang_f, seed = 1, tolerance=tolerance, device=0, average_distance = True)

    lst = []
    for i, centroid in enumerate(centroids):
        if np.isnan(centroid).any():
            continue
        indcs_orig = np.where(assignments == i)[0]
        indcs = []
        if lst_coverage is None:
            indcs = [idx+offset for idx in indcs_orig]
        else:
            for idx in indcs_orig:
                indcs.extend([sample_index for sample_index in lst_coverage[idx]])

        c = Cluster(centroid, indcs)
        lst.append(c)

    logger.info(f'Saving {targ_clusters} clusters to {f}.')
    with open(f, 'wb') as fh:
        pickle.dump(lst, fh)


def first_split(num_f, prefix):
    samples_in_batch = 1_200_000
    bytes_in_batch = samples_in_batch * REPRDIM * 4
    with open(num_f, 'rb') as fh:
        iter = 1
        while True:
            text = fh.read(bytes_in_batch)
            if len(text) == 0:
                break

            data = np.frombuffer(text, dtype=np.float32)
            data = data.reshape(-1, REPRDIM)
            cluster_num = data.shape[0] // 10
            if data.shape[0] < samples_in_batch // 2:
                cluster_num = data.shape[0] // 4
            f = f'{prefix}_{iter}'
            if not os.path.exists(f):
                offset = samples_in_batch * (iter - 1)
                kmeans(data, cluster_num, f, offset, do_yinyang=True)
            else:
                logger.info(f'Skipping group because file {f} exists')
            iter += 1


def get_lst_coverage(lst_clusters):
    lst = []
    for c in lst_clusters:
        lst.append(c.coverage)
    return lst


def run_clustering(lst_clusters, cnt, is_final, lst_f_new, cluster_d, iter_):
    cluster_num = len(lst_clusters)
    # min_idx = 1000_000_000
    data = []
    for c in lst_clusters:
        data.append(c.x)
    #     for idx in c.coverage:
    #         if idx < min_idx:
    #             min_idx = idx
    data = np.array(data, dtype=np.float32)
    targ_cluster_num = cluster_num // 7
    if cluster_num < 300_000:
        targ_cluster_num = cluster_num // 3
    elif cluster_num < 700_000:
        targ_cluster_num = cluster_num // 5
    newf = f'{cluster_d}/km_{iter_}_cluster_{cnt}'
    lst_f_new.append(newf)
    if is_final:
        targ_cluster_num = 140_000
    if os.path.exists(newf):
        logger.info(f'File {newf} exists, skipping to next')
        return targ_cluster_num
    lst_coverage = get_lst_coverage(lst_clusters)
    kmeans(data, targ_cluster_num, newf, None, lst_coverage, do_yinyang=True)
    return targ_cluster_num


def cluster_clusters(lst_f, cluster_d, iter_, is_final=False):
    assert iter_ != 0, "Not intended to be possible!"
    lst_clusters = []
    cnt = 1
    logger.info(f'Starting clustering, iteration nr {iter_}.')
    total_num_clusters = 0
    lst_f_new = []
    cluster_num = 1000_000 if not is_final else 900_000
    for f in lst_f:
        with open(f, 'rb') as fh:
            lst_clusters.extend(pickle.load(fh))
        if len(lst_clusters) >= cluster_num and not is_final:
            total_num_clusters += run_clustering(lst_clusters[:cluster_num], cnt, is_final, lst_f_new, cluster_d, iter_)
            lst_clusters = lst_clusters[cluster_num:]
            cnt += 1
    if len(lst_clusters) > 0:
        total_num_clusters += run_clustering(lst_clusters, cnt, is_final, lst_f_new,
                                             cluster_d, iter_)

    logger.info(f'Clustered down to {total_num_clusters} clusters.')
    if total_num_clusters < 250000:
        logger.info(f'Done.')
        return
    else:
        if total_num_clusters < 900_000:
            logger.info('Starting final iteration!')
            is_final = True
        cluster_clusters(lst_f_new, cluster_d, iter_ + 1, is_final)


def main_cluster_clusters(cluster_glob, cluster_d):
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    lst_f = glob(f'{cluster_glob}')
    logger.info(f'Starting, found {len(lst_f)} files.')
    cluster_clusters(lst_f, cluster_d, 1, False)


def kmeans_test():
    arr = np.empty((10000, 2), dtype=np.float32)
    angs = np.random.rand(10000) * 2 * np.pi
    for i in range(10000):
        arr[i] = np.sin(angs[i]), np.cos(angs[i])
    centroids, assignments, avg_distance = kmeans_cuda(
        arr, 4, metric='cos', verbosity=1, seed=3, average_distance=True)
    print("Average distance between centroids and members:", avg_distance)
    print(centroids)
    # pyplot.scatter(arr[:, 0], arr[:, 1], c=assignments)
    # pyplot.scatter(centroids[:, 0], centroids[:, 1], c="white", s=150)
    # pyplot.show()


def main(arg1, arg2, is_first_run: ('Initial split or not', 'flag', 'f')):
    if is_first_run:
        first_split(arg1, arg2)
    else:
        main_cluster_clusters(arg1, arg2)


if __name__ == '__main__':
    import plac
    plac.call(main)
