import torch as to

def main():
    with open('data_libri_raw_ood/lex_ortho') as fh:
        lst = []
        for line in fh:
            word, *prons = line.split()
            lst.append(prons)

    phones = set()
    for pron in lst:
        for p in pron:
            phones.add(p)
    phones_map = {}
    for i, p in enumerate(phones):
        phones_map[p] = i
        phones_map[i] = p
    num = len(phones)

    batch = []
    for pron in lst:
        word = to.zeros(num)
        for p in pron:
            word += to.nn.functional.one_hot(to.tensor(phones_map[p]), num)
        batch.append(word)
    batch = to.stack(batch)
    batch *= 2
    batch += to.rand_like(batch) * 0.1
    batch = 2*to.sigmoid(batch) - 1.
    batch_summed = batch.sum(dim=0)
    batch_summed /= batch_summed.sum()
    ent = to.sum(batch_summed * to.log(batch_summed + 1e-10)) + to.log(to.tensor(num, dtype=to.float32))
    print(ent)

main()