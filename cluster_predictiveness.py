"""
P(W, I| C) is stored as a dictionary from integer id C -> integer id W -> array of size N
"""

from math import floor, ceil, log
import numpy as np
from joblib import dump, load
from loguru import logger
from collections import defaultdict
import os
from fast import ClusterAlg
import pickle
N = 15  # number of positions possible


def get_predictivness(repr_file, word_syms):

    logger.info('Creating P(W, I|C), getting counts.')
    p_wi_c = {}  # P(W,I|C)
    Z = defaultdict(int)  # i.e. total count per cid
    num_words = int(len(word_syms) / 2)
    assert num_words == 3000, num_words
    with open(repr_file) as fh:
        for line in fh:
            uid, word, *cids = line.split()
            wid = word_syms[word]
            num_cids = len(cids)
            step_size = N / num_cids
            for i, cid in enumerate(cids):
                cid = int(cid)
                if cid not in p_wi_c:
                    p_wi_c[cid] = np.zeros((N*num_words,), dtype=np.float32)
                wid_offset = int(wid) * N
                if step_size <= 1.:
                    idx = floor(i * step_size)
                    p_wi_c[cid][wid_offset+idx] += 1
                    Z[cid] += 1
                else:
                    start_idx = floor(i * step_size)
                    end_idx = ceil((i+1) * step_size)
                    p_wi_c[cid][wid_offset + start_idx: wid_offset + end_idx] += 1
                    Z[cid] += (end_idx) - start_idx
    logger.info('Normalizing.')
    num_small = 0
    to_remove = []
    for cid in p_wi_c.keys():
        p_wi = p_wi_c[cid]
        z = Z[cid]
        if z < 100:
            num_small += 1
        if z < 25:
            to_remove.append(cid)
            continue
        p_wi /= z
    logger.info(f'Number of clusters with less than 100 counts {num_small}, less than 25 {len(to_remove)}')
    for cid in to_remove:
        del p_wi_c[cid]
    logger.info('Finished creating P(W, I|C).')
    # logger.info('Starting filtering.')
    # ents = {}
    # for cid, p_wi in p_wi_c.items():
    #     ent = - np.sum(p_wi*np.log(p_wi+1e-15))
    #     ents[cid] = ent
    #
    # lst_ents_sorted = sorted(ents.items(), key=lambda x: x[1])
    # num_original = len(lst_ents_sorted)
    # num_kept = int(num_original * 0.9)
    # cids_to_remove = [v[0] for v in lst_ents_sorted[num_kept:]]  # removing highest valued
    # for cid in cids_to_remove:
    #     del p_wi_c[cid]
    # num_kept = len(p_wi_c)
    # logger.info(f'Finished filtering, kept {num_kept} from {num_original}')
    return p_wi_c


def write_new_repr_file(repr_unfolded_f, new_repr_unfolded_f, cid_map):
    cid_map_f = f'cid_map_{len(cid_map)}'
    with open(cid_map_f, 'w') as fh:
        pickle.dump(cid_map, fh, protocol=pickle.HIGHEST_PROTOCOL)
    cid_map_inv = {}
    for main_cid, cids in cid_map.items():
        for cid in cids:
            cid_map_inv[cid] = main_cid

    cluster_cnt = len(cid_map)
    new_repr_unfolded_f = f'{new_repr_unfolded_f}_{cluster_cnt}'
    cnt_skipped = 0
    with open(repr_unfolded_f) as fhi, open(new_repr_unfolded_f, 'w') as fho:
        for line in fhi:
            if line.startswith('-'):
                fho.write(line)
            split_line = line.split()
            fho.write(f'{split_line[0]} {split_line[1]} ')
            cids = []
            for cid in split_line[2:]:
                cid = int(cid)
                if cid in cid_map_inv:
                    cids.append(cid_map_inv[cid])
                else:
                    cnt_skipped
            fho.write(f'{" ".join(str(cid) for cid in cids)}\n')
    logger.info(f'Number of cluster instances skipped: {cnt_skipped}')
    logger.info(f'Wrote out to {new_repr_unfolded_f}')


def run_clustering(repr_unfolded_f, word_ids_f, new_repr_unfolded_f):
    logger.info('Starting.')
    word_syms = {}
    with open(word_ids_f) as fh:
        for line in fh:
            word, id_ = line.split()
            word_syms[word] = id_
            word_syms[id_] = word

    p_wi_c_f = 'p_wi_c.pkl'
    if not os.path.exists(p_wi_c_f):
        p_wi_c = get_predictivness(repr_unfolded_f, word_syms)
        # p_wi_c = filter_pred(p_wi_c, list(word_syms.values()))

        dump(p_wi_c, p_wi_c_f)
    else:
        logger.info('Loading p_wi_c file.')
        p_wi_c = load(p_wi_c_f)

    logger.info(f'Starting clustering step with {len(p_wi_c.keys())} clusters')

    cluster_alg = ClusterAlg(p_wi_c, int(len(word_syms)/2), N)
    del p_wi_c
    max_cluster_num = 5000
    logger.info(f'Running clustering, max cluster num is {max_cluster_num}')
    cid_map = cluster_alg.run(max_cluster_num)
    logger.info('Done clustering.')
    write_new_repr_file(repr_unfolded_f, new_repr_unfolded_f, cid_map)

    max_cluster_num = 1000
    logger.info(f'Running clustering, max cluster num is {max_cluster_num}')
    cid_map = cluster_alg.run(max_cluster_num)
    logger.info('Done clustering.')
    write_new_repr_file(repr_unfolded_f, new_repr_unfolded_f, cid_map)

    max_cluster_num = 250
    logger.info(f'Running clustering, max cluster num is {max_cluster_num}')
    cid_map = cluster_alg.run(max_cluster_num)
    logger.info('Done clustering.')
    write_new_repr_file(repr_unfolded_f, new_repr_unfolded_f, cid_map)


if __name__ == '__main__':
    import plac; plac.call(run_clustering)