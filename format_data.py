import subprocess as sp
from collections import defaultdict, Counter
from itertools import groupby
from loguru import logger
import os
import pickle
import numpy as np
from tqdm import tqdm
import h5py as hdf
import torch as to
import math as m
import random
import concurrent.futures
import kaldi_io


def argsort(lst):
    return sorted(range(len(lst)), key=lst.__getitem__)


CHUNKLEN = 72
FEATURE_PADDING = 11  # Should be larger than model onesided-context size
NUMWORDS = 1500



class WordInfo:
    def __init__(self, word, stime, dur, uid):
        self.word = word
        self.stime = stime
        self.dur = dur
        self.uid = uid
        self.featidx = -1

    def __repr__(self):
        return f'{self.uid} {self.word} {self.stime} {self.dur}'


def format_data(datadir, datadir_hires, langdir, alidir, outd, num_words_max: ('', 'positional', None, int), uttf: ('', 'option', 'u')=None):
    if not os.path.exists(f'{alidir}/ctm') or os.path.getsize(f'{alidir}/ctm') == 0:
        cmd = f'steps/get_train_ctm.sh --cmd "$train_cmd" {datadir} {langdir} {alidir}'
        sp.check_output(cmd, shell=True)
    else:
        logger.info('ctm file already exists.')
    os.makedirs(outd, exist_ok=True)

    maxdur = CHUNKLEN - FEATURE_PADDING*2
    logger.info(f'Excluding words that are longer than {maxdur} frames.')

    utt2stime = {}
    with open(f'{datadir_hires}/segments') as fh:
        for line in fh:
            uttid, wavid, stime, _ = line.split()
            utt2stime[uttid] = float(stime)

    utts = set()
    if uttf is not None:
        with open(uttf) as fh:
            for line in fh:
                utts.add(line.strip())

    wdct = defaultdict(int)
    all_words = []
    with open(f'{alidir}/ctm') as fh:
        for line in fh:
            uttid, _, stime, dur, w = line.split()  # s[tart] time, is based on audio file not utterance
            if uttf is not None:
                if uttid not in utts:
                    continue
            dur = float(dur)
            w = w.lower()
            if w == 'i':
                w = 'I'
            wdct[w] += 1
            stime = float(stime) - utt2stime[uttid]
            wi = WordInfo(w, stime, dur, uttid)
            all_words.append(wi)

    utt2spk = {}
    spk_set = set()
    with open(f'{datadir_hires}/utt2spk') as fh:
        for line in fh:
            uttid, spk = line.split()
            utt2spk[uttid] = spk
            spk_set.add(spk)
    spk_map = {}
    with open(f'{outd}/spk_map_{num_words_max}', 'w') as fh:
        for i, spk in enumerate(spk_set):
            spk_map[spk] = i
            fh.write(f'{spk} {i}\n')
    for uttid in utt2spk.keys():
        spk = utt2spk[uttid]
        utt2spk[uttid] = spk_map[spk]

    top_words = sorted(wdct.items(), key=lambda x: x[1], reverse=True)

    filter_list = ['us', 'ye']
    logger.info(f'Filtering {filter_list}')
    cnt = 0
    with open(f'{outd}/words_{num_words_max}', 'w') as fh:
        for w, c in top_words:
            if w == '<unk>' or w == '<UNK>' or w in filter_list:
                continue
            if cnt == num_words_max:
                break
            fh.write(f'{w}\n')
            cnt += 1

    lexf = f'{outd}/lex_{num_words_max}'
    if not os.path.exists(lexf):
        sp.check_output(f'LC_ALL=en_GB.UTF-8 python $CODE/condutor/prons.py -s {outd}/words_{num_words_max} {lexf}', shell=True)
    # Only going to use those words so that a) each word only has a phone once in its pronunciation and
    # b) no overlap in pronunciations between orders while disregarding phone order and c) at least two phones
    phoneskey_to_words = defaultdict(list)
    lexicon = {}
    with open(lexf) as fh:
        for line in fh:
            word, *phones = line.split()
            if '<' in word:
                continue
            if len(phones) == 1:
                continue
            lexicon[word] = phones
            counter = Counter(phones)
            to_skip = False
            for _, count in counter.items():
                if count > 1:
                    to_skip = True
            if to_skip:
                continue
            phoneskey = ' '.join(sorted(phones))
            phoneskey_to_words[phoneskey].append(word)

    word_list = []
    with open(f'{outd}/lex_ortho', 'w') as fh:
        for phoneskey, words in phoneskey_to_words.items():
            idx = max([(i, wdct[words[i]],) for i in range(len(words))], key=lambda x: x[1])[0]
            word = words[idx]
            phones = ' '.join(lexicon[word])
            fh.write(f'{word} {phones}\n')
            word_list.append(word)
    logger.info(f'Using {len(word_list)} words.')

    cnt = 0
    word_mapping = {}
    with open(f'{outd}/words_mapping_{num_words_max}', 'w') as fh:
        for w in word_list:
            if cnt == num_words_max:
                break
            word_mapping[w] = cnt
            fh.write(f'{w} {cnt}\n')
            cnt += 1
    top_words = set(word_list)
    lowest_count = c
    if lowest_count < 100:
        logger.warning('Lowest count is smaller than 100! Probably better to use more data or lower word count.')

    # Creating word-word pronunciation distance matrix
    # pron_dist = np.zeros((len(word_list), len(word_list)))
    # for i, worda in enumerate(word_list):
    #     for j, wordb in enumerate(word_list):
    #         if i == j:
    #             continue
    #         diff = len(set(lexicon[worda]) ^ set(lexicon[wordb]))
    #         assert diff > 0, print(worda, wordb)
    #         pron_dist[i, j] = diff
    # with open('data_libri/pron_dist.pkl', 'wb') as fh:
    #     pickle.dump(pron_dist, fh)

    logger.info(f'Lowest count when using top {num_words_max} words is {lowest_count} !')

    uid_to_words = defaultdict(list)
    num_words = 0
    for wi in all_words:
        if wi.word in top_words and int(wi.dur*100) < maxdur and wi.dur > 0.03:
            wi.word = word_mapping[wi.word]
            uid_to_words[wi.uid].append(wi)
            num_words += 1
    # for uid, lst in uid_to_words.items():
    #     lst.sort(key=lambda x: x.stime)

    # Get spk stats
    spk_mean_f = f'{outd}/spk_stats.pkl'
    spk_to_dur_f = f'{outd}/spk_durs.pkl'
    spk_to_dur = defaultdict(float)
    speakers = set()
    if not os.path.exists(spk_mean_f) or not os.path.exists(spk_to_dur_f):
        spk_mean = {}
        spk_count = {}
        for uttid, m in kaldi_io.read_mat_scp(f'scp:{datadir_hires}/feats.scp'):
            spk = utt2spk[uttid]
            speakers.add(spk)
            spk_to_dur[spk] += m.shape[0] * 0.01
            if spk not in spk_mean:
                spk_mean[spk] = np.zeros(m.shape[-1])
                spk_count[spk] = 0
            spk_mean[spk] += m.sum(axis=0)
            spk_count[spk] += m.shape[0]
        for spk, mean in spk_mean.items():
            spk_mean[spk] /= spk_count[spk]
        with open(spk_mean_f, 'wb') as fh:
            pickle.dump(spk_mean, fh, protocol=pickle.HIGHEST_PROTOCOL)
        with open(spk_to_dur_f, 'wb') as fh:
            pickle.dump(spk_to_dur, fh, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        with open(spk_mean_f, 'rb') as fh:
            spk_mean = pickle.load(fh)
        with open(spk_to_dur_f, 'rb') as fh:
            spk_to_dur = pickle.load(fh)
        speakers.update(list(spk_to_dur.keys()))

    logger.info('Finished getting speaker means.')
    target_test_dur = 6. * 3600  # large portion will be silence and therefore thrown away
    current_test_dur = 0.
    speakers = list(speakers)
    random.shuffle(speakers)
    test_speakers = set()
    for speaker in speakers:
        current_test_dur += spk_to_dur[speaker]
        test_speakers.add(speaker)
        if current_test_dur > target_test_dur:
            break
    logger.info(f'Test set has {len(test_speakers)} speakers.')
    lst_words_test = []

    num_utts = len(uid_to_words)
    logger.info(f'Matching features with {num_words} words from {num_utts} utterances.')
    expected_size = sum(CHUNKLEN for wi in all_words)
    f = hdf.File(f'{outd}/960_feats_{num_words_max}.hdf', 'w')
    d = f.create_dataset('raw', (expected_size, 40,), maxshape=(expected_size, 40,), dtype='f')
    d_test = f.create_dataset('test_raw', (current_test_dur*100, 40,), maxshape=(current_test_dur*100, 40,), dtype='f')
    lst_words = []
    feat_idx = 0
    test_feat_idx = 0
    skipped = 0
    utts_processed = 0
    for uttid, m in kaldi_io.read_mat_scp(f'scp:{datadir_hires}/feats.scp'):
        speaker = utt2spk[uttid]
        is_test = False
        if speaker in test_speakers:
            is_test = True

        mean = spk_mean[speaker]
        m -= mean
        utts_processed += 1
        if uttid not in uid_to_words:
            continue

        words_for_uid = uid_to_words[uttid]

        loop_feat_idx = 0
        joined_feat = None
        last_endidx = -100
        last_stime = -1.
        for utt_word_pos, wi in enumerate(words_for_uid):
            w, stime, dur = wi.word, wi.stime, wi.dur
            assert last_stime < stime, f'{last_stime} {stime}'
            last_stime = stime
            idx = round(stime * 100) - FEATURE_PADDING
            durlen = round(dur * 100)
            assert durlen < CHUNKLEN - 2*FEATURE_PADDING
            endidx = idx + CHUNKLEN

            if m.shape[0] < idx + int(wi.dur*100):
                logger.warning(f'Skipping word because of bad feature matrix size.')
                skipped += 1
                continue

            assert endidx - idx == CHUNKLEN, '%s %s' % (feat.shape[0], wi.dur)

            back_offset = 0
            if last_endidx > idx:
                feat = m[last_endidx: endidx]
                back_offset = last_endidx - idx
                loop_feat_idx -= back_offset
                # print('in ', loop_feat_idx)
            else:
                if idx < 0:
                    feat = m[0: endidx]
                else:
                    feat = m[idx: endidx]

            # End/Begin padding
            if endidx >= m.shape[0]:
                num_overhang = endidx - m.shape[0]
                if last_endidx > m.shape[0]:
                    num_overhang = endidx - last_endidx
                if feat.shape[0] != 0:
                    feat = np.pad(feat, ((0, num_overhang), (0, 0)), mode='constant')
                else:  # feat could be empty because last frames of utterance already used
                    feat = np.tile(joined_feat[-1], (num_overhang, 1))
            if idx < 0:
                if back_offset == 0:
                    num_overhang = -idx
                    feat = np.pad(feat, ((num_overhang, 0), (0, 0)), mode='constant')
                # if back_offset != 0:
                #     print(idx, endidx, last_endidx, dur)
                #     raise RuntimeError("WTF?")

            assert feat.shape[0] + back_offset == CHUNKLEN, f'{feat.shape[0]} {back_offset} {CHUNKLEN}'

            if not is_test:
                wi.featidx = feat_idx + loop_feat_idx + FEATURE_PADDING
                tpl = (wi.word, wi.dur, wi.featidx, wi.uid, utt2spk[wi.uid], utt_word_pos)
                lst_words.append(tpl)

                if joined_feat is None:
                    joined_feat = feat
                else:
                    joined_feat = np.concatenate((joined_feat, feat), axis=0)
                loop_feat_idx = joined_feat.shape[0]
                last_endidx = endidx

            else:
                wi.featidx = test_feat_idx + loop_feat_idx + FEATURE_PADDING
                tpl = (wi.word, wi.dur, wi.featidx, wi.uid, utt2spk[wi.uid], utt_word_pos)
                lst_words_test.append(tpl)

                if joined_feat is None:
                    joined_feat = feat
                else:
                    joined_feat = np.concatenate((joined_feat, feat), axis=0)
                loop_feat_idx = joined_feat.shape[0]
                last_endidx = endidx
            # Ending per word loop

        # Per utterance loop
        if not is_test:
            d[feat_idx: feat_idx + loop_feat_idx] = joined_feat
            feat_idx += loop_feat_idx
        else:
            d_test[test_feat_idx: test_feat_idx + loop_feat_idx] = joined_feat
            test_feat_idx += loop_feat_idx

        if utts_processed % 50000 == 0:
            logger.info('Processed 50000 utts.')

    d_test.resize((test_feat_idx, 40,))
    d.resize((feat_idx, 40,))
    f.close()

    logger.info('Finished, creating pickled file.')
    with open(f'{outd}/960_word_data_train_{num_words_max}.pkl', 'wb') as fh:
        pickle.dump(lst_words, fh)
    with open(f'{outd}/960_word_data_test_{num_words_max}.pkl', 'wb') as fh:
        pickle.dump(lst_words_test, fh)


class WordDataholder:
    def __init__(self, fpath_words, fpath_feats, fpath_words_test, word_mapping, batch_size, subsampling_factor, onesided_context,
                 small_set=False, smooth=False, smooth_exp=1., use_cuda=True,
                 ignore_easy=False, shuffle_grouped=False):
        logger.info(f'Loading data, batch size {batch_size} and using fss {subsampling_factor}.')
        self.shuffle_grouped = shuffle_grouped
        self.word_mapping = word_mapping
        self.smooth_exp = smooth_exp
        self.use_cuda = use_cuda
        self.smooth = smooth
        assert FEATURE_PADDING > onesided_context
        self.subsampling_factor = subsampling_factor
        self.onesided_context = onesided_context

        with open(fpath_words, 'rb') as fh:
            self.lst_wordinfo = pickle.load(fh)
            if small_set:
                self.lst_wordinfo = self.lst_wordinfo[:25_000]

        with open(fpath_words_test, 'rb') as fh:
            self.lst_wordinfo_test = pickle.load(fh)

        num_words = len(self.lst_wordinfo)
        logger.info(f'Num words {num_words}')
        fh = hdf.File(fpath_feats, 'r')
        if not small_set:
            self.feats_dataset = fh['raw'][:]
        else:
            self.feats_dataset = fh['raw'][:12_000_000]
        self.feats_dataset_test = fh['test_raw'][:]
        fh.close()
        len_feats = self.feats_dataset.shape[0]
        logger.info(f'Number of train frames {len_feats} test frames {self.feats_dataset_test.shape[0]}')
        # assert num_chunks == len_feats // self.chunk_size
        utts_subset = set()
        with open('data_libri_short/train_utt_subset') as fh:
            for line in fh:
                utts_subset.add(line.strip())

        def parse_wordinfo(lst_wordinfo, u_subset=None):
            lst_indcs = []
            word_to_indcs = defaultdict(list)
            word_to_cnt = defaultdict(int)
            idx_to_utt = {}
            total_cnt = 0
            total_dur = 0.
            spk_set = set()
            for i, tpl in enumerate(lst_wordinfo):
                wid, dur, featidx, uttid, spkid, word_pos = tpl
                if (u_subset is not None and uttid not in u_subset) or word_pos == 0:
                    continue
                total_dur += dur
                dur = m.ceil(dur*100)
                spk_set.add(spkid)
                word_to_cnt[wid] += 1
                word_to_indcs[wid].append(total_cnt)
                lst_indcs.append((wid, total_cnt, featidx, dur, spkid, word_pos))  # (wid, word index, start feature frame index, input frame count)
                idx_to_utt[total_cnt] = uttid
                if uttid == '5826-53496-0022-1':
                    logger.info((wid, total_cnt, featidx, dur, spkid, word_pos,))
                total_cnt += 1
            num_speakers = len(spk_set)
            total_dur /= 3600.
            logger.info(f'Done parsing data, total audio duration is {total_dur:.2f} - num speakers {num_speakers}')
            return lst_indcs, word_to_indcs, word_to_cnt, idx_to_utt

        self.lst_indcs, self.word_to_indcs, self.word_to_cnt, self.idx_to_utt = parse_wordinfo(self.lst_wordinfo, utts_subset)
        self.lst_indcs = sorted(self.lst_indcs, key=lambda x: x[5])
        self.widx_to_gotten_idx = {wid: 0 for wid in self.word_to_indcs.keys()}

        self.min_word_cnt = min(self.word_to_cnt.values())
        max_word_cnt = max(self.word_to_cnt.values())
        total_cnt = sum(self.word_to_cnt.values())
        self.word_to_prob = {w: c/total_cnt for w, c in self.word_to_cnt.items()}
        self.min_word_prob = min(self.word_to_prob.values())
        logger.info(f'Largest word cnt {max_word_cnt}, smallest {self.min_word_cnt} - Total number of words {len(self.lst_indcs)} - Unique words {len(self.word_to_cnt)}')

        self.batch_size = batch_size

        logger.info('Parsing test data.')
        self.test_lst_indcs, self.test_word_to_indcs, self.test_word_to_cnt, self.test_idx_to_utt = parse_wordinfo(self.lst_wordinfo_test)
        self.test_size = len(self.test_lst_indcs)
        logger.info(f'Test set size is {self.test_size} (word count)')

        self.class_weights = [1. for _ in range(len(self.word_to_cnt.keys()))]
        self.class_weights = to.tensor(self.class_weights)
        self.num_iterations = len(self) // self.batch_size
        self.printed = False
        self.shuffle()
        self.k = 0

        self.ignored_samples = set()
        self.ignore_easy = ignore_easy
        self.word_to_invprob = {w: 1. / prob for w, prob in self.word_to_prob.items()}
        probsum = sum(self.word_to_invprob.values())
        self.word_to_invprob = {w: prob / probsum for w, prob in self.word_to_invprob.items()}

        self.tpool = concurrent.futures.ThreadPoolExecutor(2)

    def __len__(self):
        return len(self.lst_indcs)

    def shuffle(self):
        for w, indcs in self.word_to_indcs.items():
            perm = np.random.permutation(len(indcs))
            self.word_to_indcs[w] = np.asarray(indcs)[perm]

    def get_simple(self, k, lst_indcs, is_train=True, return_utts=False):
        bs = self.batch_size
        indcs = lst_indcs[k*bs: k*bs + bs]
        indcs = sorted(indcs, key=lambda x: x[3])
        self.current_indcs = indcs
        batch_dur = indcs[-1][3] + 2*self.onesided_context
        logger.info(f'batch_dur {batch_dur}')
        f_batch = np.zeros((bs, batch_dur, 40,), dtype=np.float32)
        t = np.zeros((bs,), np.long)
        spks = np.zeros((bs,), np.long)
        durs = []
        utts = []
        fh = open(f'batch_{k}', 'w')
        for j, (wid, w_idx, feat_idx, dur, spkid, word_pos) in enumerate(indcs):
            fh.write(f'{self.idx_to_utt[w_idx]} {word_pos}\n')
            if self.idx_to_utt[w_idx] == '1473-135657-0018-1':
                logger.info(f'{j} {wid} {feat_idx} {word_pos}')
            if is_train:
                f_batch[j] = self.feats_dataset[feat_idx-self.onesided_context: feat_idx - self.onesided_context + batch_dur]
            else:
                f_batch[j] = self.feats_dataset_test[feat_idx-self.onesided_context: feat_idx - self.onesided_context + batch_dur]
            t[j] = wid
            spks[j] = spkid
            if len(durs) > 0:
                assert dur >= durs[-1]
            durs.append(dur)
            if return_utts:
                if is_train:
                    utts.append(self.idx_to_utt[w_idx])
                else:
                    utts.append(self.test_idx_to_utt[w_idx])
        fh.close()
        durs = to.tensor([m.ceil(dur/3) for dur in durs])
        durs_counts = to.unique_consecutive(durs, return_counts=True)
        device = to.device('cpu')
        if to.cuda.is_available() and self.use_cuda:
            device = to.device('cuda')
        f_batch = to.from_numpy(f_batch).to(device).transpose(1, 2)
        if k == 0:
            logger.info(f_batch.shape)
        if not return_utts:
            return f_batch, to.from_numpy(t).to(device), durs_counts, to.as_tensor(spks).to(device)
        else:
            return f_batch, to.from_numpy(t).to(device), durs_counts, to.as_tensor(spks).to(device), utts

    def subsample(self, lst_indcs):
        word_to_get = {}
        for w, cnt in self.word_to_cnt.items():
            new_cnt = int(cnt**self.smooth_exp + 1000)
            if cnt < new_cnt:
                new_cnt = cnt
            word_to_get[w] = new_cnt
        do_shuffle = False
        new = []
        for wid, idxs in self.word_to_indcs.items():
            cnt_to_get = word_to_get[wid]
            while cnt_to_get % 4 != 0: cnt_to_get -= 1
            start_idx = self.widx_to_gotten_idx[wid]
            end_idx = start_idx + cnt_to_get
            lst = idxs[start_idx: end_idx]
            if end_idx >= len(idxs):
                end_idx = end_idx - len(idxs)
                lst = np.concatenate((lst, idxs[0: end_idx],))
                if wid == 0: do_shuffle = True
            for idx in lst:
                new.append(lst_indcs[idx])
            self.widx_to_gotten_idx[wid] = end_idx
        if not self.shuffle_grouped:
            random.shuffle(new)
        else:
            gen = MyGen()
            groups = [list(g) for _, g in groupby(new, gen)]
            random.shuffle(groups)
            new = [v for group in groups for v in group]
        return new, do_shuffle

    def update_ignored(self, mask):
        """ True if was predicted """
        for i, idx in enumerate(self.current_indcs):
            if mask[i]:
                self.ignored_samples.add(idx[1])
            else:
                if idx[1] in self.ignored_samples:
                    self.ignored_samples.remove(idx[1])

    def iter_train_simple(self, return_utts=False):
        if self.ignore_easy:
            lst_indcs = []
            rand = np.random.random(len(self.ignored_samples))
            randidx = -1
            for i, idx in enumerate(self.lst_indcs):
                if idx[1] in self.ignored_samples:
                    randidx += 1
                    if rand[randidx] > self.word_to_invprob[idx[0]] ** 0.5:
                        continue
                lst_indcs.append(idx)
        elif self.smooth:
            lst_indcs, do_shuffle = self.subsample(self.lst_indcs)
            if do_shuffle:
                self.shuffle()
                logger.info('Shuffling data.')
        else:
            #random.shuffle(self.lst_indcs)
            lst_indcs = self.lst_indcs
        num_iters = len(lst_indcs) // self.batch_size
        if not self.printed:
            logger.info(f'Num samples in epoch {len(lst_indcs)}')
            #self.printed = True
        f, t, durs_counts, *spks_utts = self.get_simple(self.k, lst_indcs, True, return_utts)
        while self.k < num_iters-1:
            self.k += 1
            future = self.tpool.submit(self.get_simple, self.k, lst_indcs, True, return_utts)
            if not return_utts:
                yield f, t, durs_counts, spks_utts
            else:
                yield f, t, durs_counts, spks_utts[0], spks_utts[1]
            f, t, durs_counts, *spks_utts = future.result()
        self.k = 0

    def iter_test_simple(self, return_utts=False):
        num_iters = len(self.test_lst_indcs) // self.batch_size
        f, t, durs_counts, *spks_utts = self.get_simple(0, self.test_lst_indcs, False, return_utts)
        for k in range(1, num_iters-1):
            future = self.tpool.submit(self.get_simple, k, self.test_lst_indcs, False, return_utts)
            if not return_utts:
                yield f, t, durs_counts, spks_utts
            else:
                yield f, t, durs_counts, spks_utts[0], spks_utts[1]
            f, t, durs_counts, *spks_utts = future.result()

    def get_state(self):
        dct = {'k': self.k, 'word_to_indcs': self.word_to_indcs, 'widx_to_gotten_idx': self.widx_to_gotten_idx, 'ignored_samples': self.ignored_samples}
        return dct

    def set_state(self, dct):
        self.k = dct['k']
        self.word_to_indcs = dct['word_to_indcs']
        self.widx_to_gotten_idx = dct['widx_to_gotten_idx']
        self.ignored_samples = dct['ignored_samples']


class MyGen:
    def __init__(self):
        self.i = -1

    def __call__(self, x):
        self.i += 1
        return self.i // 4

if __name__ == '__main__':
    import plac; plac.call(format_data)
