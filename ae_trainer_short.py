import os
import random
import shutil
import subprocess as sp
import sys

import numpy as np
import regex as re
import torch as to
from loguru import logger

from model import Tdnnf, RegressionWorker


def model_init(repr_dim, w_decay, drop_r, large_model=False, use_layernorm=False):
    if not large_model:
        encoder = Tdnnf(40, repr_dim, drop_r, large_size=1280, small_size=128, use_vq=True, use_layernorm=use_layernorm)
    else:
        conv_stride_list = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1]
        time_stride_list = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0]
        encoder = Tdnnf(40, repr_dim, drop_r, large_size=1280, small_size=192, conv_stride_list=conv_stride_list,
                        time_stride_list=time_stride_list, use_vq=True, use_layernorm=use_layernorm)

    reg_worker = RegressionWorker(repr_dim, 60)
    return encoder, reg_worker


def save_checkpoint(model_d, epoch, lr, optimizer, encoder, reg_worker, scheduler):
    fpath = f'{model_d}/{epoch}.tar'
    encoder.cpu(), reg_worker.cpu()
    to.save({
        'encoder': encoder.state_dict(),
        'reg_worker': reg_worker.state_dict(),
        'optimizer': optimizer.state_dict(),
        'scheduler': scheduler.state_dict(),
        'lr': lr
    }, fpath)


def load_checkpoint(fpath, encoder, reg_worker):
    checkpoint = to.load(fpath, map_location='cpu')
    encoder.load_state_dict(checkpoint['encoder'])
    reg_worker.load_state_dict(checkpoint['reg_worker'])
    lr = checkpoint['lr']
    return lr


def train_job(model_d, epochs, epochs_trained, iters_trained, batch_size, w_decay, drop_r, first_lr_drop, second_lr_drop, large_model,
              start_vq, add_noise, repr_dim, sgd, small_set, patience, use_layernorm, do_mask):
    gpumem = 10
    if batch_size == 1024:
        gpumem = 20
    elif batch_size >= 2048:
        gpumem = 30

    large_model_str = '-large-model' if large_model else ''
    small_set_str = '-small-set' if small_set else ''
    use_layernorm_str = '-use-layernorm' if use_layernorm else ''
    do_mask_str = '-do-mask' if do_mask else ''
    logf = f'{model_d}/trainlog{epochs_trained}'
    cmd = f'utils/queue.pl -V -l q1dm --totmem 20G {logf} python ae_train_job.py {model_d} -batch-size {batch_size} -target-epochs {epochs} ' \
          f' -epochs-trained {epochs_trained} -iters-trained {iters_trained} -w-decay {w_decay}  -drop-r {drop_r}' \
          f'  {large_model_str} {do_mask_str} ' \
          f'  -start-vq {start_vq} -add-noise {add_noise} -repr-dim {repr_dim}' \
          f'  -sgd {sgd} {small_set_str} -patience {patience}' \
          f'  {use_layernorm_str} -first-lr-drop {first_lr_drop} -second-lr-drop {second_lr_drop}'
    result = sp.run(cmd, shell=True)
    if result.returncode != 0:
        logger.warning('FAILED!')
        sys.exit(0)
    with open(logf) as fh:
        text = fh.read()
        epochs_done = re.search(r'(?<=epochs=)\d+', text).group(0)
        iters_done = re.search(r'(?<=iters=)\d+', text).group(0)
    epochs_trained = int(epochs_done)
    iters_trained = int(iters_done)
    return epochs_trained, iters_trained


def train(model_d, continue_from, epochs, lr, batch_size, w_decay, drop_r, first_lr_drop, second_lr_drop, large_model,
          start_vq, add_noise, repr_dim, sgd, small_set, patience, use_layernorm, do_mask):

    if continue_from == 0:
        encoder, reg_worker = model_init(repr_dim, w_decay, drop_r, large_model, use_layernorm)
        if sgd < 0.:
            optimizer = to.optim.AdamW(list(encoder.parameters()) + list(reg_worker.parameters()), lr=lr, weight_decay=w_decay, betas=(0.9, 0.99))
        else:
            optimizer = to.optim.SGD(list(encoder.parameters()) + list(reg_worker.parameters()), lr=lr, momentum=sgd)
        scheduler = to.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.25, threshold=1e-3, patience=patience, verbose=True, cooldown=5, min_lr=1e-6)
        save_checkpoint(model_d, 0, lr, optimizer, encoder, reg_worker, scheduler)

    epochs_trained = continue_from
    iters_trained = 0
    while epochs_trained < epochs:
        # logger.info('Submitting job.')
        epochs_trained, iters_trained = train_job(model_d, epochs, epochs_trained, iters_trained, batch_size, w_decay,
                                                  drop_r, first_lr_drop, second_lr_drop, large_model,
                                                  start_vq, add_noise, repr_dim, sgd, small_set, patience, use_layernorm,
                                                  do_mask)


def main(model_d, continue_from: ('Continue training from last model', 'option', None, int) = 0,
         epochs: ('Num epochs', 'option', None, int) = 10,
         lr: ('Learning rate', 'option', None, float) = 1e-2,
         batch_size: ('BS', 'option', None, int) = 1024,
         w_decay: ('Weight decay lambda', 'option', None, float) = 0.,
         drop_r: ('Dropout', 'option', None, float) = 0.05,
         first_lr_drop: ('', 'option', None, int) = -1,
         second_lr_drop: ('', 'option', None, int) = 6,
         large_model: ('', 'flag', None) = False,
         start_vq: ('', 'option', None, int) = 1001,
         add_noise: ('', 'option', None, float) = 0.,
         repr_dim: ('', 'option', None, int) = 128,
         sgd: ('', 'option', None, float) = -1.,
         small_set: ('', 'flag', None) = False,
         patience: ('', 'option', None, int) = 0,
         use_layernorm:( '', 'flag', None) = False,
         do_mask: ('', 'flag', None) = False):
    to.manual_seed(0)
    np.random.seed(0)
    to.cuda.manual_seed(0)
    random.seed(0)
    loglr = np.log10(lr)
    logwd = np.log10(w_decay)
    model_d += f'_e{epochs}_loglr{loglr:.1f}_bs{batch_size}_dp{drop_r:.2f}_logwd{logwd:.1f}'

    if os.path.exists(model_d) and not continue_from:
        shutil.rmtree(model_d)
    os.makedirs(model_d, exist_ok=True)
    pid = os.getpid()
    with open(f'{model_d}/pid', 'w') as fh:
        fh.write(str(pid))
    logger.info(f'Directory used is {model_d}')
    logger.remove()
    train(model_d, continue_from, epochs, lr, batch_size, w_decay, drop_r, first_lr_drop, second_lr_drop, large_model,
          start_vq, add_noise, repr_dim, sgd, small_set, patience, use_layernorm, do_mask)


if __name__ == '__main__':
    import plac; plac.call(main)
