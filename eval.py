import torch as to
from loguru import logger
import os
import math
from collections import defaultdict

import format_data
from model import Encoder, WordWorkerNoOrder, WordClassifier
from misc.misc import parse_config
from trainer_short import model_init, load_checkpoint
import trainer
REPRDIM = 256


def get_representations_variable_chunklen(model, dataholder, get_repr, spk2utt):
    logger.info('Getting representations.')
    fh = open(f'{get_repr}_words', 'w')
    fhb = open(f'{get_repr}_nums_tmp', 'wb')
    cnt = 0
    num_elements_total = 0
    with to.no_grad():
        for f, t, durs, extra_infos in dataholder.iter_data(spk2utt):
            # logger.info(f'{f.shape}')
            h = model(f)
            h = h.cpu()
            durs, counts = to.unique_consecutive(durs, return_counts=True)
            idx = 0
            for i, (dur, count) in enumerate(zip(durs, counts)):
                h_current = h[idx: idx + count, :dur]
                h_current = h_current.reshape(-1, REPRDIM)
                num_elements_total += h_current.shape[0] * REPRDIM
                h_current.numpy().tofile(fhb, format='b')
                current_cnt = 0
                for j in range(idx, idx + count):
                    uid, w = extra_infos[j]
                    for _ in range(dur):
                        fh.write(f'{cnt} {w} {uid}\n')
                        cnt += 1
                        current_cnt += 1
                    fh.write('-\n')
                assert current_cnt == h_current.shape[0]
                idx += count

    fhb.close()
    fh.close()
    logger.info(f'Done. Wrote out {cnt} words.')
    logger.info('Adding 8 bytes infront to save element count information.')
    with open(f'{get_repr}_nums_tmp', 'rb') as old, open(f'{get_repr}_nums', 'wb') as new:
        new.write(num_elements_total.to_bytes(8, 'little'))
        for chunk in iter(lambda: old.read(4096), b''):
            new.write(chunk)
    os.remove(f'{get_repr}_nums_tmp')


def main(checkpoint_f):

    word_mapping = {}
    with open('data_libri/words_mapping_1000') as fh:
        for line in fh:
            w, i = line.split()
            i = int(i)
            word_mapping[w] = i
            word_mapping[i] = w

    conf_f = os.path.dirname(checkpoint_f) + '/conf'
    word_cnt = len(word_mapping) // 2

    train_args = parse_config(conf_f)

    encoder, word_worker, word_loss, reg_worker = \
        model_init(train_args, word_cnt, None)

    load_checkpoint(checkpoint_f, encoder, word_loss, word_worker, reg_worker)

    for m in (encoder, word_worker, word_loss):
        m.eval()
        m.to('cpu')

    onesided_context = trainer.get_context_size(encoder)

    dataholder = format_data.WordDataholder('data_libri/960_word_data_1000.pkl', 'data_libri/960_feats_1000.hdf', word_mapping, train_args.batch_size,
                                subsampling_factor=3, onesided_context=onesided_context, small_set=train_args.small_set,
                                smooth=train_args.smooth, train_args=train_args, use_cuda=False)
    import IPython; IPython.embed()
    trainer.run_eval_wordchunks_constrast(None, encoder, word_worker, dataholder, word_loss, train_args)


import plac
plac.call(main)
