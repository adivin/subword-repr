import plac
from tqdm import tqdm

def main(segments, outf, *ctms):
    uid_times = {}
    with open(segments) as fh:
        for line in fh:
            uid, wavid, stime, etime = line.split()
            uid_times[uid] = (stime, etime,)

    lines = []
    for ctm in tqdm(ctms):
        with open(ctm) as fh:
            for line in fh:
                uid, _, stime, dur, w = line.split()
                stime = float(uid_times[uid][0]) + float(stime)
                dur = float(dur)
                lines.append(f'{uid} _ {stime:.2f} {dur:.2f} {w}\n')

    with open(outf, 'w') as fh:
        for line in lines:
            fh.write(line)
            

plac.call(main)
