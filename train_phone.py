import subprocess as sp
from loguru import logger
import numpy as np
import h5py
import os
import python_rw


CHUNK_LEN = 1.5


def format_data(feats_f, ali, phones_f, uttlist_f=None):

    if not os.path.exists('data/ali.txt'):
        model = os.path.dirname(ali) + '/final.mdl'
        cmd = f'ali-to-phones --per-frame {model} "ark:gunzip -c {ali} |" ark,t:data/ali.txt'
        sp.check_output(cmd, shell=True)
    else:
        logger.info('ali.txt already exists.')

    uttlist = None
    if uttlist_f is not None:
        logger.info(f'Will filter down data to given uttlist {uttlist_f}')
        with open(f'{uttlist_f}') as fh:
            uttlist = set(fh.read().splitlines())

    dct_phones = {}
    phones_map = {}
    with open(f'{phones_f}') as fh:
        for line in fh:
            ph, i = line.split()
            i = int(i)
            dct_phones[ph] = i
            dct_phones[i] = ph
            phnew = ph.split('_')[0]
            if phnew not in phones_map:
                phones_map[phnew] = len(phones_map)

    with open('data/phones_mapping', 'w') as fh:
        for k, v in phones_map.items():
            fh.write(f'{k} {v}\n')

    utt2targs = {}
    num_targs = 0
    with open('data/ali.txt') as fh:
        for line in fh:
            linesp = line.split()
            if uttlist is not None:
                if linesp[0] not in uttlist:
                    continue
            targs = [phones_map[dct_phones[int(v)].split('_')[0]] for v in linesp[1:]]
            utt2targs[linesp[0]] = targs
            num_targs += len(linesp) - 1
    logger.info('Read through ali.txt file.')
    sp.check_output(f'utils/filter_scp.pl {uttlist_f} {feats_f} > data/feats.scp', shell=True)
    feats_f = 'data/feats.scp'

    num_utts = len(uttlist)
    logger.info(f'Total number of utterances is {num_utts}')
    fh = h5py.File('data/data_phone.hdf', 'w')
    df = fh.create_dataset('feats', (num_targs, 40,), dtype='f', maxshape=(num_targs, 40,))
    dt = fh.create_dataset('targs', (num_targs,), dtype='i', maxshape=(num_targs,))

    chunk_size = int(CHUNK_LEN * 100)
    logger.info('Processing feats.')
    feat_reader = python_rw.Pykread(f'scp:{feats_f}')
    utts_processed = 0
    not_done = True
    idx = 0
    while not_done:
        try:
            uttid, m = feat_reader.get()
        except RuntimeError:
            print('Done')
            break
        m = m - np.mean(m, axis=0)
        utt_len = m.shape[0]
        if utt_len < chunk_size:
            not_done = feat_reader.next()
            continue
        targs = utt2targs[uttid]
        if len(targs) != utt_len:
            print(len(targs), utt_len)
            logger.info('Skipping because number of targs does not match number of feats !')
            not_done = feat_reader.next()
            continue

        for i in range(utt_len // chunk_size):
            f = m[i*chunk_size: i*chunk_size + chunk_size]
            df[idx: idx + chunk_size] = f

            dt[idx: idx + chunk_size] = targs[i*chunk_size: i*chunk_size + chunk_size]

            idx += chunk_size

        utts_processed += 1
        if utts_processed % 10000 == 0:
            logger.info('Processed 10000 utts.')
        not_done = feat_reader.next()
    df.resize((idx, 40,))
    dt.resize((idx,))
    fh.close()

    logger.info('Finished data formatting.')


def main():
    format_data('/work/data-3600h/train_cleaned_hires/feats.scp', '/work/exp-3600h/tri3_ali_cleaned/ali.*.gz', \
                '/work/data-3600h/lang/phones.txt', '/work/fun/subword-repr/utts')

import plac
plac.call(main)