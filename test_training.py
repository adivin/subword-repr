import torch.nn as nn
import torch as to

def contr_loss(sim, labels):
    sim_maxs, _ = sim.max(dim=1, keepdim=True)
    sim -= sim_maxs.detach()
    sim = to.exp(sim)

    to.diagonal(sim).fill_(0.)

    mask = to.eq(labels, labels.T).float()
    to.diagonal(mask).fill_(0.)

    den = sim.sum(dim=1)

    probs = (sim * mask).sum(dim=1) / den
    log_probs = -to.log(probs) / (mask.sum(dim=1) + 1)

    cnt = len(to.unique(labels))
    return log_probs.sum() / cnt


def main():
    labels = to.tensor([[0, 1, 0, 1]])
    sim = to.tensor([[5., 0., 5., 0.],
                     [0., 5., 0., 5.],
                     [5., 0., 5., 0.],
                     [0., 5., 0., 5.]])

    print(contr_loss(sim, labels))

    sim = to.tensor([[5.0000, 5.0000, 4.7434, 5.0000],
                     [5.0000, 5.0000, 4.7434, 5.0000],
                     [4.7434, 4.7434, 5.0000, 4.7434],
                     [5.0000, 5.0000, 4.7434, 5.0000]])
    print(contr_loss(sim, labels))

    sim = to.tensor([[5.0000, 4.4721, 4.7434, 4.7434],
                     [4.4721, 5.0000, 3.5355, 3.5355],
                     [4.7434, 3.5355, 5.0000, 5.0000],
                     [4.7434, 3.5355, 5.0000, 5.0000]])
    print(contr_loss(sim, labels))

    sim = to.ones(4,4) * 5.
    print(contr_loss(sim, labels))

main()