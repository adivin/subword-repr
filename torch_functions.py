import math

import torch as to
import random
from torch.autograd import Function, Variable
import torch.nn.functional as F
import numpy as np
from loguru import logger


class VectorQuantization(Function):
    @staticmethod
    def forward(ctx, inputs, codebook):
        with to.no_grad():
            embedding_size = codebook.size(1)
            inputs_size = inputs.size()
            inputs_flatten = inputs.view(-1, embedding_size)

            codebook_sqr = to.sum(codebook ** 2, dim=1)
            inputs_sqr = to.sum(inputs_flatten ** 2, dim=1, keepdim=True)

            # Compute the distances to the codebook
            distances = to.addmm(codebook_sqr + inputs_sqr,
                inputs_flatten, codebook.t(), alpha=-2.0, beta=1.0)

            _, indices_flatten = to.min(distances, dim=1)
            indices = indices_flatten.view(*inputs_size[:-1])
            ctx.mark_non_differentiable(indices)

            return indices

    @staticmethod
    def backward(ctx, grad_output):
        raise RuntimeError('Trying to call `.grad()` on graph containing '
            '`VectorQuantization`. The function `VectorQuantization` '
            'is not differentiable. Use `VectorQuantizationStraightThrough` '
            'if you want a straight-through estimator of the gradient.')


class VectorQuantizationStraightThrough(Function):
    @staticmethod
    def forward(ctx, inputs, codebook):
        indices = vq(inputs, codebook)
        indices_flatten = indices.view(-1)

        codes_flat = to.index_select(codebook, dim=0,
            index=indices_flatten)
        ctx.save_for_backward(indices_flatten, inputs, codes_flat, codebook)
        codes = codes_flat.view_as(inputs)
        return codes

    @staticmethod
    def backward(ctx, grad_output):
        grad_inputs, grad_codebook = None, None

        if ctx.needs_input_grad[0]:
            # Straight-through estimator
            indices_flat, inputs, codes_flat, codebook = ctx.saved_tensors
            inputs_flat = inputs.view_as(codes_flat)
            diff = 2 * (inputs_flat - codes_flat)
            diffval = to.abs(diff).sum()
            grad_output_flat = grad_output.contiguous().view(-1, inputs.size(-1))
            gradval = to.abs(grad_output_flat).sum()
            # print(gradval, diffval)
            beta = gradval / diffval
            diff *= beta * 0.1
            # diffval = diff.sum(dim=-1).mean().item()
            # print('backdiffgrad', diffval)
            grad_inputs = grad_output.clone() + diff.view_as(inputs)
        if ctx.needs_input_grad[1]:
            # Gradient wrt. the codebook
            indices_flat, inputs, codes_flat, codebook = ctx.saved_tensors

            emb_dim = inputs.size(-1)
            inputs_flat = inputs.view_as(codes_flat)
            diff = 2 * (codes_flat - inputs_flat)
            # print('diffgrad', diff.sum(dim=-1).mean().item())
            grad_codebook = to.zeros_like(codebook)
            grad_codebook.index_add_(0, indices_flat, diff)

            # grad_output_flat = grad_output.contiguous().view(-1, emb_dim)
            # grad_codebook.index_add_(0, indices_flat, grad_output_flat)
            # print('gradout', grad_output_flat.sum(dim=-1).mean().item())
            # avg = (diff - grad_output_flat).sum(dim=-1).mean().item()
            # print('avg', avg)
        return grad_inputs, grad_codebook


def _sample_gumbel(shape, eps=1e-10, out=None):
    """
    Sample from Gumbel(0, 1)
    based on
    https://github.com/ericjang/gumbel-softmax/blob/3c8584924603869e90ca74ac20a6a03d99a91ef9/Categorical%20VAE.ipynb ,
    (MIT license)
    """
    U = out.resize_(shape).uniform_() if out is not None else to.rand(shape)
    return - to.log(eps - to.log(U + eps))


def _gumbel_softmax_sample(logits, tau=1., eps=1e-10):
    """
    Draw a sample from the Gumbel-Softmax distribution
    based on
    https://github.com/ericjang/gumbel-softmax/blob/3c8584924603869e90ca74ac20a6a03d99a91ef9/Categorical%20VAE.ipynb
    (MIT license)
    """
    dims = logits.dim()
    gumbel_noise = _sample_gumbel(logits.size(), eps=eps, out=logits.data.new())
    y = logits + gumbel_noise
    return to.softmax(y / tau, dims - 1)


class GumbelSoftmax(Function):
    @staticmethod
    def forward(ctx, inputs, tau=1.):
        shape = inputs.size()
        # y_soft = _gumbel_softmax_sample(inputs, tau=tau).view(shape[0] * shape[1], -1)
        y_soft = to.softmax(inputs / tau, dim=-1).view(shape[0] * shape[1], -1)
        indcs = y_soft.argmax(-1)

        ctx.tau = tau
        y_hard = inputs.data.new(*shape).zero_().view(shape[0] * shape[1], -1).scatter_(-1, indcs.unsqueeze(1), 1.0).view(*shape)
        ctx.save_for_backward(y_soft, y_hard, indcs)
        ctx.mark_dirty(inputs)
        return y_hard

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        if ctx.needs_input_grad[0]:
            shape = grad_output.size()
            grad_output = grad_output.view(-1, shape[2])
            logger.info(f'gradoutput {grad_output.shape} {grad_output.norm()} {grad_output[1]}')
            y_soft, y_hard, indcs = ctx.saved_tensors
            tau = ctx.tau

            i = to.arange(y_soft.size(0)).to(y_soft.device)
            y_soft_targs = y_soft[i, indcs]
            logger.info(f'{y_soft_targs[1]} {indcs[1]} {y_soft[1].sum()}')
            logger.info(f'{y_soft[1]}')
            grad_softmax = -y_soft_targs.unsqueeze(1) * y_soft
            logger.info(f'BEF {grad_softmax[1]}')
            grad_softmax[i, indcs] += y_soft_targs
            logger.info(f'AFT {grad_softmax[1]}')
            logger.info(f'grad sm {grad_softmax.norm()}')
            delta = (grad_output * y_hard.view(-1, shape[2])).sum(dim=-1)
            logger.info(f'delta {to.sign(delta).sum()} {delta.norm()} {delta[1]}')
            grad_input = delta.unsqueeze(1) * grad_softmax / tau
            # grad_input = grad_output * grad_softmax
            logger.info(f'grad_input {grad_input.norm()} {grad_input[1]}')
            grad_input = grad_input.view(*shape)
        return grad_input, None


class OneHotDropped(Function):
    @staticmethod
    def forward(ctx, y_soft):
        shape = y_soft.size()
        named_tuple = to.max(y_soft, dim=1)
        indcs = named_tuple.indices
        maxs = named_tuple.values

        y_hard = to.zeros(*shape, device=y_soft.device).scatter_(1, indcs.unsqueeze(1), 1.)
        mask = to.rand(shape[0], shape[2]) < maxs
        y_hard *= mask.unsqueeze(1)

        ctx.save_for_backward(y_hard.bool(), indcs)
        return y_hard

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        if ctx.needs_input_grad[0]:
            shape = grad_output.shape
            y_hard, indcs = ctx.saved_tensors
            # grad_input = grad_output * y_hard
            return grad_output


class ReprDrop(Function):
    @staticmethod
    def forward(ctx, y_hard, drop_perc):
        """ Input is (N, T, C) """
        shape = y_hard.size()
        N = shape[0]
        T = shape[1]
        offset = 0 if random.random() < 0.5 else 1
        fraction_off = int(T * drop_perc)
        time_indcs = to.randint(0, T // 2, (N * fraction_off,), device=y_hard.device) * 2 + offset
        batch_indcs = to.arange(N, device=y_hard.device).repeat_interleave(fraction_off)
        indcs = (batch_indcs, time_indcs)
        y_hard[indcs] = 0.
        mask = to.ones(N, T, dtype=to.bool, device=y_hard.device)
        mask[indcs] = False
        ctx.save_for_backward(mask)
        return y_hard

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        if ctx.needs_input_grad[0]:
            mask = ctx.saved_tensors[0]
            grad_input = grad_output * mask.unsqueeze(2)
        return grad_input, None


class JitterF(Function):
    @staticmethod
    def forward(ctx, x, replace_prob):
        """ Input is (N, T, C) """
        bs = x.size(0)
        time_steps = x.size(1)
        channels = x.size(2)
        k = np.random.choice([-1, 0, 1], size=(bs, time_steps-2,), p=[replace_prob/2, 1 - replace_prob, replace_prob/2])
        # k = to.multinomial([])
        # maybe should try linear interpolation
        indices = to.zeros(bs, time_steps)
        # indices[:, -1] = time_steps - 1
        indices[:, 1:time_steps-1] = to.as_tensor(k)

        indices = indices.to(device=x.device, dtype=to.long).view(-1)
        indices += to.arange(bs * time_steps, device=x.device)
        x = to.index_select(x.view(bs*time_steps, channels,), 0, indices)
        x = x.reshape(bs, time_steps, channels).contiguous()
        return x

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        if ctx.needs_input_grad[0]:
            grad_input = grad_output
        return grad_input, None


class OneHot(Function):
    @staticmethod
    def forward(ctx, y_soft):
        shape = y_soft.size()
        indcs = y_soft.argmax(dim=1)
        # logger.info(f'{indcs.shape}')

        y_hard = to.zeros(*shape, device=y_soft.device).scatter_(1, indcs.unsqueeze(1), 1.)
        # logger.info(f'{y_hard.shape}')
        ctx.save_for_backward(y_hard.bool(), indcs)
        return y_hard

    @staticmethod
    def backward(ctx, grad_output):
        grad_input = None
        if ctx.needs_input_grad[0]:
            shape = grad_output.shape
            y_hard, indcs = ctx.saved_tensors
            # grad_input = grad_output * y_hard
            return grad_output


class STClamp(to.autograd.Function):
    """
    In the forward pass this operation behaves like to.clamp.
    But in the backward pass its gradient is 1 everywhere, as if instead of clamp one had used the identity function.
    """

    @staticmethod
    def forward(ctx, input, min, max):
        return input.clamp(min=min, max=max)

    @staticmethod
    def backward(ctx, grad_output):
        return grad_output.clone(), None, None


vq = VectorQuantization.apply
vq_st = VectorQuantizationStraightThrough.apply
custom_gs = GumbelSoftmax.apply
custom_onehot = OneHot.apply
custom_onehotdropped = OneHotDropped.apply
st_clamp = STClamp.apply
repr_drop = ReprDrop.apply
jitter = JitterF.apply