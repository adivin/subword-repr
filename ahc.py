from scipy.spatial.distance import cosine
import numpy as np
from loguru import logger
from functools import total_ordering
import pickle
import math
from glob import glob


class Cluster:
    def __init__(self, x, coverage):
        self.x = x
        self.coverage = coverage


def bhatt_dist(x, y):
    return -np.log(np.sum((x*y)**0.5))


def convertf_to_posidx(f, round_down=True):
    """ convert fraction """
    if round_down:
        return int(math.floor(f * 10))
    else:
        return int(math.ceil(f * 10))


def get_pos_overlap(dct1, cid1, dct2, cid2):
    """
    dct1 has all words, count and corresponding sequence of cids, one of which is cid1
    """
    # hist is implicitly split into bins of size 0.1, therefore len(hist) == 10
    hist = [0 for _ in range(10)]
    for word, entry in dct1.items():
        cnt, *cids = entry
        cnt_cids = len(cids)
        for i, cid in enumerate(cids):
            if cid == cid1:
                start = i / cnt_cids
                end = (i+1) / cnt_cids
                for i in range(convertf_to_posidx(start), convertf_to_posidx(end, False)+1):
                    hist[i] += 1


def dist(c1, c2):
    return cosine(c1.average_x, c2.average_x)


def convert_condensed_idx(idx, lst):
    return lst[idx]


def sumn(n):
    return n*(n+1) / 2


def convert_to_condensed_idx(i, j, n):
    if i > j:
        i, j = j, i
    idx = sumn(n) - sumn(n-i) + j - i
    return int(idx)


def ahc(data, cluster_down_to_num, f):
    import fast
    data = data[:16000]
    logger.info(f'Data has shape {data.shape}')

    num_clusts = data.shape[0]
    logger.info(f'Setting {num_clusts} initial clusters.')

    active_clusters = fast.ActiveClusters(data)
    logger.info('Creating cluster pairs queue.')

    pq = fast.List(data, 0.3)
    num_pairs = pq.size(False)
    logger.info(f'Considering {num_pairs} pairs.')

    logger.info('Running AHC.')
    while num_clusts > cluster_down_to_num:
        fast.run_ahc_step(pq, active_clusters)
        num_clusts -= 1

    logger.info('Done.')

    lst = active_clusters.get_keys()
    logger.info(f'Saving {len(lst)} clusters')
    lst_clusters = []
    for i in lst:
        c = active_clusters.get(i)
        cluster = Cluster(c.get_x(), c.get_coverage())
        lst_clusters.append(cluster)
    with open(f, 'wb') as fh:
        pickle.dump(lst_clusters, fh)


# def run_clustering(lst_f):
#     lst_clusters = []
#     for f in lst_f:
#         with open(f, 'rb') as fh:
#             lst_clusters.extend(pickle.load(fh))
#         if len(lst_clusters)


def main(word_f, cluster_d):
    logger.info('Starting.')

    words = {}
    with open(word_f) as fh:
        for i, line in enumerate(fh):
            idx, w, uid = line.split()
            words[idx] = (w, uid,)
    logger.info(f'Number of words {len(words)}')
    lst_f = glob(f'{cluster_d}/km_cluster*')

    # for f in lst_f
    num_samples = len(words)
    logger.info(f'Number of samples {num_samples}')
    n = 320_000
    num_iters = num_samples // n
    # with open(num_f, 'rb') as fh:
    #     for i in range(1, num_iters + 2):
    #         logger.info(f'Processing chunk nr {i}')
    #         bytes_to_read = n * 256 * 4
    #         text = None
    #         if i != num_iters + 1:
    #             text = fh.read(bytes_to_read)
    #         else:
    #             text = fh.read()
    #         data = np.fromstring(text, dtype=np.float32)
    #         data = data.reshape(-1, 256)
    #
    #         if data.shape[0] < clusttarg:
    #             clusttarg = data.shape[0] // 4
    #         ahc(data, clusttarg, i)


def test_ahc():
    import fast
    data = [[1.0, 1.1, 1.5],
         [1.4, 1.3, 1.45],
         [0.2, 0.15, 0.25],
         [0.1, 0.9, 0.15],
         [4.1, 4.3, 3.9],
         [4.05, 4.4, 4.1]]
    data = np.array(data)
    num_clusts = data.shape[0]
    data = np.array(data)

    active_clusters = fast.ActiveClusters(data)
    pq = fast.List(data, 0.75)

    while num_clusts > 3:

        fast.run_ahc_step(pq, active_clusters)

        num_clusts -= 1
    lst = active_clusters.get_keys()
    for i in lst:
        c = active_clusters.get(i)
        print(i, c.get_coverage())
        print(c.get_x())
    # if cost < 0.5:
    #     c = active_clusters.get(i)


if __name__ == '__main__':
    import plac
    plac.call(main)
