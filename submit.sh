#!/bin/bash -e

utils/queue.pl --totmem 20G -l q_short_gpu -V $PRON/trainlog_contr_a python trainer.py $PRON/model_contr_a -epochs 5 -first-lr-drop 1 -second-lr-drop 4 -batch-size 512 -drop-r 0. -temp 1.0 &
utils/queue.pl --totmem 20G -l q_short_gpu -V $PRON/trainlog_contr_b python trainer.py $PRON/model_contr_b -epochs 5 -first-lr-drop 1 -second-lr-drop 4 -batch-size 512 -drop-r 0. -temp 4. &
