import torch
import torch.nn as nn
from loguru import logger
import numpy as np
import torch.nn.functional as F



def _constraint_orthonormal_internal(M):
    '''
    Refer to
        void ConstrainOrthonormalInternal(BaseFloat scale, CuMatrixBase<BaseFloat> *M)
    from
        https://github.com/kaldi-asr/kaldi/blob/master/src/nnet3/nnet-utils.cc#L982
    Note that we always use the **floating** case.
    '''
    assert M.ndim == 2

    num_rows = M.size(0)
    num_cols = M.size(1)

    assert num_rows <= num_cols

    # P = M * M^T
    P = torch.mm(M, M.t())
    P_PT = torch.mm(P, P.t())

    trace_P = torch.trace(P)
    trace_P_P = torch.trace(P_PT)

    scale = torch.sqrt(trace_P_P / trace_P)

    ratio = trace_P_P * num_rows / (trace_P * trace_P)
    assert ratio > 0.98

    update_speed = 0.125

    if ratio > 1.02:
        update_speed *= 0.5
        if ratio > 1.1:
            update_speed *= 0.5

    identity = torch.eye(num_rows, dtype=P.dtype, device=P.device)
    P = P - scale * scale * identity

    alpha = update_speed / (scale * scale)
    M = M - 4 * alpha * torch.mm(P, M)
    return M


class OrthonormalLinear(nn.Module):

    def __init__(self, dim, bottleneck_dim, time_stride=0, padding=0):
        super().__init__()
        assert time_stride in [0, 1]

        if time_stride == 0:
            kernel_size = 1
        else:
            kernel_size = 3

        self.kernel_size = kernel_size

        # conv requires [N, C, T]
        self.conv = nn.Conv1d(in_channels=dim,
                              out_channels=bottleneck_dim,
                              kernel_size=kernel_size,
                              bias=False,
                              padding=padding)
        self.conv.weight.data *= 20 ** (-0.5)

    def forward(self, x):
        # input x is of shape: [batch_size, feat_dim, seq_len] = [N, C, T]
        assert x.ndim == 3
        x = self.conv(x)
        return x

    def constraint_orthonormal(self):
        state_dict = self.conv.state_dict()
        w = state_dict['weight']
        # w is of shape [out_channels, in_channels, kernel_size]
        out_channels = w.size(0)
        in_channels = w.size(1)
        kernel_size = w.size(2)

        w = w.reshape(out_channels, -1)

        num_rows = w.size(0)
        num_cols = w.size(1)

        need_transpose = False
        if num_rows > num_cols:
            w = w.t()
            need_transpose = True

        w = _constraint_orthonormal_internal(w)

        if need_transpose:
            w = w.t()

        w = w.reshape(out_channels, in_channels, kernel_size)

        state_dict['weight'] = w
        self.conv.load_state_dict(state_dict)


class OrthonormalActualLinear(nn.Module):

    def __init__(self, dim_in, dim_out):
        super().__init__()
        self.proj = nn.Linear(dim_in, dim_out, bias=False)

    def forward(self, x):
        # input x is of shape: [batch_size, feat_dim, seq_len] = [N, C, T]
        x = self.proj(x)
        return x

    def constraint_orthonormal(self):
        state_dict = self.proj.state_dict()
        w = state_dict['weight']
        # w is of shape [out_channels, in_channels]
        out_channels = w.size(0)
        in_channels = w.size(1)

        num_rows = w.size(0)
        num_cols = w.size(1)

        need_transpose = False
        if num_rows > num_cols:
            w = w.t()
            need_transpose = True

        w = _constraint_orthonormal_internal(w)

        if need_transpose:
            w = w.t()

        state_dict['weight'] = w
        self.proj.load_state_dict(state_dict)


def layer_norm(x, time_last=True):
    dim = -1
    if time_last:
        dim = 1
    m = x.mean(dim=dim, keepdim=True)
    var = torch.mean((x - m)**2, dim=dim, keepdim=True)
    return (x - m) / torch.sqrt(var+1e-10)


class FactorizedTDNN(nn.Module):
    '''
    This class implements the following topology in kaldi:
      tdnnf-layer name=tdnnf2 $tdnnf_opts dim=1024 bottleneck-dim=128 time-stride=1
    References:
        - http://danielpovey.com/files/2018_interspeech_tdnnf.pdf
        - ConstrainOrthonormalInternal() from
          https://github.com/kaldi-asr/kaldi/blob/master/src/nnet3/nnet-utils.cc#L982
    '''

    def __init__(self,
                 dim,
                 bottleneck_dim,
                 time_stride,
                 conv_stride,
                 dropout,
                 padding,
                 bypass_scale=1.,
                 use_layernorm=False):
        super().__init__()

        assert conv_stride in [1, 3]
        assert abs(bypass_scale) <= 1

        self.bypass_scale = torch.nn.Parameter(torch.tensor(bypass_scale), requires_grad=False)
        self.conv_stride = conv_stride
        self.padding = padding

        self.linear = OrthonormalLinear(dim=dim,
                                        bottleneck_dim=bottleneck_dim,
                                        time_stride=time_stride,
                                        padding=padding)
        self.affine = nn.Conv1d(in_channels=bottleneck_dim,
                                out_channels=dim,
                                kernel_size=1,
                                stride=conv_stride)

        # batchnorm requires [N, C, T]
        if not use_layernorm:
            self.batchnorm = nn.BatchNorm1d(num_features=dim)
        else:
            self.batchnorm = lambda x: layer_norm(x, time_last=True)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        # input x is of shape: [batch_size, feat_dim, seq_len] = [N, C, T]
        assert x.ndim == 3
        # logger.debug(x.shape)
        input_x = x

        x = self.linear(x)
        x = self.affine(x)
        # at this point, x is [N, C, T]
        x = self.batchnorm(F.relu(x))
        x = self.dropout(x)

        # TODO(fangjun): implement GeneralDropoutComponent in PyTorch
        # logger.debug(f'{x.shape} {input_x.shape}')
        # at this point, x is [N, C, T]
        if self.linear.kernel_size > 1 and self.padding == 0:
            x = self.bypass_scale * input_x[:, :, self.conv_stride:-self.conv_stride:self.conv_stride] + x
        else:
            x = self.bypass_scale * input_x[:, :, ::self.conv_stride] + x
        return x

    def constrain_orthonormal(self):
        self.linear.constraint_orthonormal()


class MovingAverage(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.factor = 0.1
        self.initial_state = nn.Parameter(torch.randn(1, dim*3))

    def forward(self, x):
        """ x has shape [N, T, C] """
        T = x.size(1)
        h = self.initial_state.expand(x.size(0), -1)
        before_x = F.pad(x.clone()[:, :-1], [0,0,1,0], mode='constant')
        after_x = F.pad(x.clone()[:, 1:], [0,0,0,1], mode='constant')
        x = torch.cat((before_x, x, after_x), dim=2)
        outs = []
        for i in range(T):
            h = (1-self.factor) * h + self.factor * x[:, i]
            outs.append(h)
        outs = torch.stack(outs, dim=1)
        return outs


class OrderedCombine(nn.Module):
    def __init__(self, dim_in, dim_out):
        super().__init__()
        self.pos_layer = PositionSymmetricLayer(128)
        self.proj = nn.Linear(dim_in + 128, dim_out, bias=False)

    def forward(self, x, durs):
        """ x has shape [N, T, C] """
        x = self.pos_layer(x, durs)
        x = F.relu(self.proj(x))
        x = x.sum(dim=1) / durs.unsqueeze(1)
        return x


class OrderedCombineFused(nn.Module):
    def __init__(self, dim_in, dim_out):
        super().__init__()
        assert dim_out - dim_in > 64
        self.emb_dim = 128
        self.embedding = nn.Parameter(torch.linspace(0., 1., self.emb_dim//2), requires_grad=False)
        self.proj_pos = nn.Linear(dim_in + self.emb_dim, dim_out - dim_in, bias=False)
        self.proj_pos.weight.data.fill_(1. / self.proj_pos.weight.size(0))
        self.dim_out = dim_out

    def forward(self, x, dur_cnts):
        """ x has shape [N, T, C]
            x is assumed to be sorted by durations
        """
        N = x.size(0)
        T = x.size(1)
        durs, counts = dur_cnts
        idx = 0
        output = torch.zeros((N, self.dim_out), dtype=torch.float32, device=x.device)
        for i, (dur, count) in enumerate(zip(durs, counts)):
            factor = torch.linspace(0, 1, dur).unsqueeze(1).to(x.device)
            pos_emb = (torch.cat((self.embedding - factor, -self.embedding + factor,), dim=-1) >= 0).float()
            pos_emb = pos_emb.unsqueeze(0).expand(count, -1, -1)
            x_part = x[idx: idx + count, :dur]
            x_part_pos = torch.cat((x[idx: idx + count, :dur], pos_emb), dim=-1)
            x_part_proj = torch.cat((self.proj_pos(x_part_pos), x_part), dim=-1)
            output[idx: idx + count] = x_part_proj.mean(dim=1)
            idx += count
        return output


class OrderedCombineFusedRnn(nn.Module):
    def __init__(self, dim_in, dim_out, rnn_dim=32, do_sum=False, simple=False):
        super().__init__()
        self.rnn_dim = rnn_dim
        self.gru = nn.GRU(dim_in, self.rnn_dim, batch_first=True)
        self.dim_in = dim_in
        self.dim_out = dim_out
        self.simple = simple
        if not simple:
            self.fc1 = nn.Linear(self.rnn_dim + dim_in, 2048)
            self.fc2 = nn.Linear(2048, dim_out)
        else:
            self.fc = nn.Linear(self.rnn_dim + dim_in, dim_out, bias=False)
        self.do_sum = do_sum

    def forward(self, x, dur_cnts):
        """ x has shape [N, T, C]
            x is assumed to be sorted by durations
        """
        N = x.size(0)
        T = x.size(1)
        durs, counts = dur_cnts
        idx = 0
        output = torch.zeros((N, self.dim_in + self.rnn_dim), dtype=torch.float32).to(x.device)
        for i, (dur, count) in enumerate(zip(durs, counts)):
            x_part = x[idx: idx + count, :dur]
            if not self.do_sum:
                output[idx: idx + count, :self.dim_in] = x_part.mean(dim=1)
            else:
                output[idx: idx + count, :self.dim_in] = x_part.sum(dim=1)
            _, h_gru = self.gru(x_part)
            output[idx: idx + count, self.dim_in:] = h_gru.squeeze(1)
            idx += count
        if not self.simple:
            output = self.fc2(torch.tanh(self.fc1(output)))
        else:
            output = self.fc(output)
        return output


class OrderedCombineFusedSinusoid(nn.Module):
    def __init__(self, dim_in, dim_out):
        super().__init__()
        # self.proj_pos.weight.data.fill_(1. / self.proj_pos.weight.size(0))
        self.dim_in = dim_in
        self.dim_out = dim_out
        self.fc = nn.Linear(dim_in, dim_out)

    def forward(self, x, dur_cnts):
        """ x has shape [N, T, C]
            x is assumed to be sorted by durations
        """
        N = x.size(0)
        T = x.size(1)
        durs, counts = dur_cnts
        idx = 0
        output = torch.zeros((N, self.dim_in), dtype=torch.float32).to(x.device)
        for i, (dur, count) in enumerate(zip(durs, counts)):
            x_part = x[idx: idx + count, :dur]
            output[idx: idx + count] = add_positionsinusoid(x_part).mean(dim=1)
            idx += count
        output = self.fc(output)
        return output


class ReluBasic(nn.Module):
    def __init__(self, dim_in, dim_out, dropout=0., residual=True, use_layernorm=False, no_final_ops=False):
        super().__init__()
        inner_dim = 768
        if dim_out > inner_dim:
            inner_dim = dim_out
        self.residual = residual
        self.fc1 = nn.Linear(dim_in, inner_dim)
        self.fc2 = nn.Linear(inner_dim, dim_out)
        self.bn1 = nn.BatchNorm1d(inner_dim, affine=False) if not use_layernorm else nn.LayerNorm(inner_dim, elementwise_affine=False)
        self.bn2 = nn.BatchNorm1d(dim_out, affine=False) if not use_layernorm else nn.LayerNorm(dim_out, elementwise_affine=False)
        self.no_final_ops = no_final_ops
        self.drop1 = nn.Dropout(dropout)
        self.drop2 = nn.Dropout(dropout)

    def forward(self, x):
        identity = x.clone()
        out = self.drop1(self.bn1(F.relu(self.fc1(x))))
        out = self.fc2(out)
        if not self.no_final_ops:
            out = F.relu(out)
            out = self.bn2(out)
        if self.residual:
            if x.size(-1) == self.fc2.weight.size(1):
                out += identity
            else:
                out[:, :identity.size(1)] += identity
        out = self.drop2(out)

        return out


class GLULayer(nn.Module):
    def __init__(self, in_dim, dim, kernel, stride, drop=0., use_layernorm=False):
        super().__init__()
        self.layer = nn.Conv1d(in_dim, dim, kernel_size=kernel, stride=stride)
        self.dropout = Dropout1(drop)
        self.norm = nn.BatchNorm1d(dim // 2, affine=False) if not use_layernorm else lambda x: layer_norm(x)

    def forward(self, x):
        return self.norm(self.dropout(F.glu(self.layer(x), dim=1)))


class ConvLayer(nn.Module):
    def __init__(self, in_dim, dim, kernel, stride, drop=0., use_layernorm=False):
        super().__init__()
        self.layer = nn.Conv1d(in_dim, dim, kernel_size=kernel, stride=stride)
        self.dropout = Dropout1(drop)
        self.norm = nn.BatchNorm1d(dim // 2, affine=False) if not use_layernorm else lambda x: layer_norm(x)

    def forward(self, x):
        return self.dropout(self.norm(F.relu(self.layer(x))))

class ConvReluBasic(nn.Module):
    def __init__(self, dim_in, dim_out, residual=True):
        super().__init__()
        inner_dim = 768
        if dim_out > inner_dim:
            inner_dim = dim_out
        self.residual = residual
        self.cv1 = nn.Conv1d(dim_in, inner_dim, kernel_size=1, stride=1, bias=False)
        self.cv2 = nn.Conv1d(inner_dim, dim_out, kernel_size=1, stride=1, bias=False)
        self.bn1 = nn.BatchNorm1d(inner_dim)
        self.bn2 = nn.BatchNorm1d(dim_out)
        self.dim_in = dim_in
        self.dim_out = dim_out

    def forward(self, x):
        identity = x
        out = F.relu(self.bn1(self.cv1(x)))
        out = self.bn2(self.cv2(out))
        if self.residual:
            if self.dim_in == self.dim_out:
                out += identity
            else:
                out[:, :self.dim_in] += identity
        out = F.relu(out)
        return out


class PositionSymmetricLayer(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.embedding = nn.Parameter(torch.linspace(0., 1., dim//2), requires_grad=False)
        self.dim = dim

    def forward(self, x, durs):
        """ x has shape (N, T, C)
            x is assumed to be sorted by durations (T axis)
        """
        N = x.size(0)
        T = x.size(1)
        pos_emb = torch.zeros((N, T, self.dim), dtype=torch.float32).to(x.device)
        durs, counts = torch.unique_consecutive(durs, return_counts=True)
        idx = 0
        for i, (dur, count) in enumerate(zip(durs, counts)):
            factor = torch.linspace(0, 1, dur).unsqueeze(1).to(x.device)
            pos = (torch.cat((self.embedding - factor, -self.embedding + factor,), dim=-1) >= 0).float()
            pos_emb[idx: idx + count, :dur] = pos.unsqueeze(0).expand(count, -1, -1)
            idx += count
        x = torch.cat((x, pos_emb,), dim=-1)
        return x


def get_angle(pos, i, d):
    return pos / torch.pow(100, (2 * (i//2) / d))


def add_positionsinusoid(x):
    T = x.size(1)
    C = x.size(2)
    table = torch.zeros((T, C,))
    for pos in range(T):
        table[pos] = torch.cat((np.sin(get_angle(pos, torch.arange(C//2), C//2)),
                                np.cos(get_angle(pos, torch.arange(C//2), C//2))), dim=-1)
    x += table.to(x.device)
    return x


class GradNormaliser(torch.autograd.Function):
    @staticmethod
    def forward(ctx, input_word, input_regr):
        return input_word, input_regr

    @staticmethod
    def backward(ctx, grad_output_word, grad_output_regr):
        factor = grad_output_word.norm() / grad_output_regr.norm()
        # logger.info(f'{grad_output_word} {grad_output_regr}')
        # logger.info(f'f {factor} {grad_output_word.norm()} {grad_output_regr.norm()}')
        grad_input_word = grad_output_word.clone()
        grad_input_regr = grad_output_regr * factor
        return grad_input_word, grad_input_regr

grad_normaliser = GradNormaliser.apply

class Dropout1(nn.Module):
    def __init__(self, p):
        super().__init__()
        self.binomial = torch.distributions.binomial.Binomial(probs=1-p)
        self.p = p
    def forward(self, x):
        """ Has shape (N, C, T,) """
        if not self.training:
            return x
        mask = self.binomial.sample((x.size(0), x.size(1),)).to(x.device)
        return x * mask.unsqueeze(2) * (1./(1-self.p))

def sig(x):
    return 2.*torch.sigmoid(x) - 1.
